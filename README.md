# Restaurant REST API


## Overview

This project is an example REST API for a restaurant, it was made as an educational project for a theses concertning the changes in the developmenet methods in moder team with a focus on Web developement. The project is made using Spring Boot following one of the standard Spring Project diviosns and some of the design patters assosiated with it. The project also demonstrates some features of Spring Boot such as the JPA and Mail Starter projects as well a JWT baed authentication. The last addition to the project was a test deployment on Docker containers in Kubernetes. 

## Project Specificaitions

The project has the following specifications during the last update:

- Type: Web Back end Application
- Language: Java
- Java Development Kit Version: 8u191
- Framework: Spring Boot
- Unit Testing Framework: Spring Unit Tests
- Deployment Archive: Jar
- Dependency Manager and Build Automation System: Gradle
- Web Server: Integrated Tomcat
- Version Control Platform: Local GitLab Server
- Containerisation Platform: Docker
- Deployment Platform: Local Kubernetes Cluster


## Project Structure

The general structure of the project is as follows:

```
    .
    ├── build.gradle    # Gradle Build Configuration
    ├── Dockerfile      # Dockerfile for the API
    ├── deployment.yaml # Kubernetes Deployment Configuration
    ├── gradle          # Gradle Wrapper for offline builds
    ├── gradlew         # Gradle Linux Building Script
    ├── gradlew.bat     # Gradle Windows Building Script
    ├── settings.gradle # Global Gradle Settings
    └── src             # Source Code
        ├── main        # Web Server Code
        └── test        # Unit Tests / Integration Tests Code
```    

## Note

the certificates and password used in the projetc should **NOT** be used in productions, those are for educational purpoes only.