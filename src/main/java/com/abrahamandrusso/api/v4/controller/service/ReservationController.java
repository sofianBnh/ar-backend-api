package com.abrahamandrusso.api.v4.controller.service;

import com.abrahamandrusso.api.v4.model.dto.ReservationDTO;
import com.abrahamandrusso.api.v4.model.pojo.Notification;
import com.abrahamandrusso.api.v4.model.pojo.ServiceResponse;
import com.abrahamandrusso.api.v4.model.resource.Reservation;
import com.abrahamandrusso.api.v4.service.error.ServiceException;
import com.abrahamandrusso.api.v4.service.notification.INotificationService;
import com.abrahamandrusso.api.v4.service.reservation.IReservationResourceService;
import com.abrahamandrusso.api.v4.service.reservation.IReservationSimpleUserService;
import com.abrahamandrusso.api.v4.service.reservation.IReservationUserService;
import com.abrahamandrusso.api.v4.utils.request.RequestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.stream.Collectors;

import static com.abrahamandrusso.api.v4.utils.request.RequestUtils.extractTokenValue;
import static com.abrahamandrusso.api.v4.utils.request.RequestUtils.removeReservationRedundancy;
import static com.abrahamandrusso.api.v4.utils.response.ControllerUtils.buildResponse;


/**
 * @author Benhamed Sofiane on 03/03/18 15:17
 * <p>
 * Class Controller
 * Responsible for the handling of reservations
 * resources
 */

@RestController
@RequestMapping("/reservation-service/")
public class ReservationController {


    // ~ Declarations
    //==================================================================================================

    private final IReservationUserService userService;

    private final IReservationResourceService resourceService;

    private final IReservationSimpleUserService simpleUserService;

    private final INotificationService notificationService;


    //~ Application Variables
    //==================================================================================================

    @Value("${jwt.token-header}")
    private String tokenHeader;

    @Value("${jwt.token-bearer}")
    private String bearer;


    // ~ Constructors
    //=================================================================================================

    /**
     * @param userService         Users' Reservations Service : handles operations related to the
     *                            {@link com.abrahamandrusso.api.v4.model.user.User}
     * @param resourceService     Reservation Resource Service : responsible for handling CRUD operation
     *                            on {@link Reservation} object
     * @param simpleUserService   Users' Reservations Service : handles operations related to the
     *                            {@link com.abrahamandrusso.api.v4.model.user.SimpleUser}
     * @param notificationService Service responsible for the user interactions trough emails
     */
    @Autowired
    public ReservationController(
            IReservationUserService userService,
            IReservationResourceService resourceService,
            IReservationSimpleUserService simpleUserService,
            INotificationService notificationService
    ) {
        this.userService = userService;
        this.resourceService = resourceService;
        this.simpleUserService = simpleUserService;
        this.notificationService = notificationService;
    }


    // ~ Routes
    //===============================================================================================

    /**
     * Route for handling the saving of registered users' reservations
     *
     * @param dto : Reservation data transfer object
     * @return Whether the reservation was saved or not
     * @throws ServiceException if there is a problem with the reservation
     */
    @PostMapping("users")
    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    public @ResponseBody
    ResponseEntity<?> saveUserReservation(@RequestBody ReservationDTO dto) throws ServiceException {

        Reservation reservation = resourceService.save(userService, dto);

        removeReservationRedundancy(reservation);

        return new ResponseEntity<>(reservation, HttpStatus.OK);

    }


    /**
     * Route for handling the saving of unregistered users' reservations
     *
     * @param dto : Reservation data transfer object
     * @return Whether the reservation was saved or not
     * @throws ServiceException if there is a problem with the reservation
     */
    @PostMapping("simple-users")
    public @ResponseBody
    ResponseEntity<?> saveSimpleUserReservation(@RequestBody ReservationDTO dto) throws ServiceException {

        Reservation reservation = resourceService.save(simpleUserService, dto);

        removeReservationRedundancy(reservation);

        return new ResponseEntity<>(reservation, HttpStatus.OK);

    }


    /**
     * Route for handling the update of registered users' reservations
     *
     * @param id      : id of the reservation
     * @param dto     : dto build from the previous reservation
     * @param request : http  source request
     * @return Whether the reservation was updated or not
     * @throws ServiceException if there is a problem with the update of
     *                          the reservation such as the dates are passed
     */
    @PutMapping("{id}")
    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    public @ResponseBody
    ResponseEntity<?> update(HttpServletRequest request, @PathVariable("id") Long id,
                             @RequestBody ReservationDTO dto
    ) throws ServiceException {

        // getting the token from the request
        String token = extractTokenValue(request, tokenHeader, bearer);

        if (token == null)
            return buildResponse(new ServiceResponse(false, "Token not found"));


        // building the updated reservation
        Reservation updated = userService.buildReservation(dto, token, id);

        // updating
        resourceService.update(updated);

        removeReservationRedundancy(updated);

        return new ResponseEntity<>(updated, HttpStatus.OK);

    }


    /**
     * Route for handling the deletion of registered users' reservations
     *
     * @param id      : id of the reservation
     * @param request : http  source request
     * @return Whether the reservation was updated or not
     * @throws ServiceException if the reservation does not exist or
     *                          the user deleting it is not the owner
     */
    @DeleteMapping("{id}")
    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    public ResponseEntity<?> delete(HttpServletRequest request, @PathVariable("id") Long id)
            throws ServiceException {

        // getting the token from the request
        String token = extractTokenValue(request, tokenHeader, bearer);

        if (token == null)
            return buildResponse(new ServiceResponse(false, "Token not found"));


        ServiceResponse response = resourceService.delete(token, id);

        return buildResponse(response);

    }


    /**
     * Route for handling the request of registered users' reservations
     *
     * @param request : originating http request
     * @return list of reservations if the token is valid
     * @throws ServiceException if the user does not exist
     */
    @GetMapping("user-reservations")
    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    public @ResponseBody
    ResponseEntity<?> findByUserId(HttpServletRequest request) throws ServiceException {

        // getting the token from the request
        String token = extractTokenValue(request, tokenHeader, bearer);

        if (token == null)
            return buildResponse(new ServiceResponse(false, "Token not found"));

        // getting the reservations
        List<Reservation> reservations = resourceService.findReservationsForToken(token);

        // in case of an error
        if (reservations == null) return ResponseEntity.badRequest().body("An error has occurred");

        reservations = reservations.parallelStream()
                .peek(RequestUtils::removeReservationRedundancy)
                .collect(Collectors.toList());

        // return the list
        return new ResponseEntity<>(reservations, HttpStatus.OK);

    }


    /**
     * Route for handling the confirmation of the reservations
     *
     * @param token : JWT associated with the reservation
     */
    @GetMapping("confirm")
    public void confirm(@Param("token") String token) {
        try {
            // confirm the reservation
            Notification confirm = resourceService.confirm(token);

            // send a notification
//            notificationService.sendSimpleNotification(confirm);
        } catch (ServiceException e) {
            e.printStackTrace();
        }
    }


}
