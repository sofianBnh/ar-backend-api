package com.abrahamandrusso.api.v4.controller.service;

import com.abrahamandrusso.api.v4.model.dto.OrderDTO;
import com.abrahamandrusso.api.v4.model.pojo.ServiceResponse;
import com.abrahamandrusso.api.v4.model.resource.Reservation;
import com.abrahamandrusso.api.v4.service.discount.IDiscountService;
import com.abrahamandrusso.api.v4.service.error.ServiceException;
import com.abrahamandrusso.api.v4.service.order.IOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

import static com.abrahamandrusso.api.v4.utils.request.RequestUtils.extractTokenValue;
import static com.abrahamandrusso.api.v4.utils.response.ControllerUtils.buildResponse;

/**
 * @author Benahmed Sofiane on 03/04/18 16:23
 * <p>
 * Class Controller
 * Responsible for the handling of orders
 * </p>
 */

@RestController
@RequestMapping("/order-service/")
public class OrderController {


    // ~ Declarations
    //==============================================================================================

    private final IOrderService orderService;

    private final IDiscountService discountService;


    //~ Application Variables
    //==============================================================================================

    @Value("${jwt.token-bearer}")
    private String bearer;

    @Value("${jwt.token-header}")
    private String tokenHeader;


    // ~ Constructors
    //==============================================================================================

    /**
     * @param orderService    : service responsible for the orders
     * @param discountService : service responsible for the discount
     */
    @Autowired
    public OrderController(
            IOrderService orderService,
            IDiscountService discountService
    ) {
        this.orderService = orderService;
        this.discountService = discountService;
    }


    // ~ Methods
    //==============================================================================================

    /**
     * Method that allows the user to add an order to a reservation
     *
     * @param order   a data transfer object representing the order
     * @param request the http request
     * @return the final price
     * @throws ServiceException if there is an error while calculating the discount or
     *                          setting the order
     */
    @PostMapping("users")
    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    public ResponseEntity<?> addOrderUser(@RequestBody OrderDTO order, HttpServletRequest request)
            throws ServiceException {

        final String token = extractTokenValue(request, tokenHeader, bearer);

        Reservation reservation = orderService.addOrder(order, token);

        discountService.calculateInitialPrice(reservation);

        discountService.calculateDiscountedPrice(reservation);

        double price = discountService.calculateWithAutomaticDiscount(reservation);

        return buildResponse(new ServiceResponse(true, String.valueOf(price)));

    }


}
