package com.abrahamandrusso.api.v4.controller.resource;

import com.abrahamandrusso.api.v4.model.resource.meal.Meal;
import com.abrahamandrusso.api.v4.service.meal.IMealDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author Benahmed Sofiane on 26/05/18 01:20
 * <p>
 * Class Controller
 * Responsible for the handling of meal
 * resources
 * </p>
 */

@RestController
@RequestMapping("/meal-resources/")
public class MealDataController {

    // ~ Declarations
    //==============================================================================================

    private final IMealDataService mealDataService;

    // ~ Constructors
    //==============================================================================================

    /**
     * @param mealDataService : service responsible for the broadcast of meals data
     */
    @Autowired
    public MealDataController(IMealDataService mealDataService) {
        this.mealDataService = mealDataService;
    }


    // ~ Routes
    //==============================================================================================

    /**
     * Method that provides the front with a list of all the meals
     *
     * @return the list of all meals
     */
    @GetMapping
    public ResponseEntity<List<Meal>> findAllMeals() {
        return new ResponseEntity<>(mealDataService.findAllMeals(), HttpStatus.OK);
    }
}
