package com.abrahamandrusso.api.v4.controller.user;

import com.abrahamandrusso.api.v4.model.dto.SimpleUserDTO;
import com.abrahamandrusso.api.v4.model.dto.UserDTO;
import com.abrahamandrusso.api.v4.model.pojo.Notification;
import com.abrahamandrusso.api.v4.model.pojo.ServiceResponse;
import com.abrahamandrusso.api.v4.service.error.EmailNotSentException;
import com.abrahamandrusso.api.v4.service.error.ServiceException;
import com.abrahamandrusso.api.v4.service.notification.INotificationService;
import com.abrahamandrusso.api.v4.service.registration.ISimpleUserRegistrationService;
import com.abrahamandrusso.api.v4.service.registration.IUserRegistrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static com.abrahamandrusso.api.v4.utils.response.ControllerUtils.buildResponse;

/**
 * @author Benhamed Sofiane on 03/03/18 15:17
 * <p>
 * Class Controller
 * Responsible for the handling of registrations
 * </p>
 */

@RestController
@RequestMapping("/registration-service/")
public class RegistrationController {


    // ~ Declarations
    //==========================================================================================

    private final ISimpleUserRegistrationService simpleUserService;

    private final INotificationService notificationService;

    private final IUserRegistrationService userService;


    // ~ Constructors
    //==========================================================================================

    /**
     * @param userService         : Service that handles users registration
     * @param simpleUserService   : Service that handles simple users ( anonymous ) registration
     * @param notificationService : Service responsible for the email notification of the users
     */
    @Autowired
    public RegistrationController(
            ISimpleUserRegistrationService simpleUserService,
            INotificationService notificationService,
            IUserRegistrationService userService
    ) {
        this.userService = userService;
        this.simpleUserService = simpleUserService;
        this.notificationService = notificationService;
    }


    // ~ Routes
    //==========================================================================================

    /**
     * Route for users' sign up
     *
     * @param user : {@link com.abrahamandrusso.api.v4.model.user.User} data transfer object
     * @return user's id
     */
    @PostMapping("user-sign-up")
    public @ResponseBody
    ResponseEntity<?> signUp(@RequestBody UserDTO user) {

        ServiceResponse response = new ServiceResponse();

        Notification notification = null;

        try {

            // save the user
            notification = userService.signUp(user);

            // notify the user
//            notificationService.sendConfirmationNotification(notification);

            // return the id
            response.setContent(notification.getObjectId());
            response.setOk(true);

        } catch (EmailNotSentException e) {

            // if the confirmation email is not sent
            try {

                assert notification != null;
                // remove the user from the database
                userService.unSignUp(Long.valueOf(notification.getObjectId()));

                // return the error
                response.setContent(e.getMessage());
                response.setOk(false);

            } catch (ServiceException e1) {
                response.setContent(e1.getMessage());
                response.setOk(false);
            }

        } catch (ServiceException e) {
            response.setContent(e.getMessage());
            response.setOk(false);
        }

        return buildResponse(response);

    }


    /**
     * Route for the creation of a simple user
     *
     * @param user : {@link com.abrahamandrusso.api.v4.model.user.SimpleUser} data transfer object
     * @return the id of the user
     */
    @PostMapping("simple-user-sign-up")
    public @ResponseBody
    ResponseEntity<?> createSimpleUser(@RequestBody SimpleUserDTO user) {

        ServiceResponse response = simpleUserService.create(user);

        return buildResponse(response);
    }

    /**
     * Route for the confirmation of a user's account
     *
     * @param token : JWT of the user
     */
    @GetMapping("confirm")
    public void confirm(@RequestParam("token") String token) {
        try {

            // processing the token
            Notification confirm = userService.confirm(token);

            // sending the notification
//            notificationService.sendSimpleNotification(confirm);
        } catch (ServiceException e) {
            e.printStackTrace();
        }
    }


}

