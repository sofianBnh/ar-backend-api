package com.abrahamandrusso.api.v4.controller.resource;

import com.abrahamandrusso.api.v4.service.discount.IDiscountService;
import com.abrahamandrusso.api.v4.service.discount.error.DiscountException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Benahmed Sofiane on 28/05/18 13:23
 * <p>
 * Class Controller
 * Responsible for the handling of  discount
 * resources
 * </p>
 */

@RestController
@RequestMapping("/discount-resources/")
public class DiscountDataController {

    // ~ Declarations
    //==============================================================================================

    private final IDiscountService service;


    // ~ Constructors
    //==============================================================================================

    /**
     * @param service : Service responsible for treatments related to discounts
     */
    @Autowired
    public DiscountDataController(IDiscountService service) {
        this.service = service;
    }


    // ~ Routes
    //==============================================================================================

    /**
     * Method that returns the default discount
     *
     * @return the default discount
     * @throws DiscountException if the discount is not available
     */
    @GetMapping("default")
    public ResponseEntity<?> findDefaultDiscount() throws DiscountException {
        return new ResponseEntity<>(service.getCurrentAutomaticDiscount(), HttpStatus.OK);
    }


}
