package com.abrahamandrusso.api.v4.controller.errors;

import com.abrahamandrusso.api.v4.model.pojo.ServiceResponse;
import com.abrahamandrusso.api.v4.service.authentication.error.AuthenticationException;
import com.abrahamandrusso.api.v4.service.error.ServiceException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import static com.abrahamandrusso.api.v4.utils.response.ControllerUtils.buildResponse;

@ControllerAdvice
public class GlobalExceptionHandler {

    /**
     * Exception handler for the {@link ServiceException}
     *
     * @param exception generated exception
     * @return HTTP code bad request with the error message
     */
    @ExceptionHandler(ServiceException.class)
    public ResponseEntity<?> serviceExceptions(ServiceException exception) {
        return buildResponse(new ServiceResponse(false, exception.getMessage()));
    }


    /**
     * Exception handler for the {@link AuthenticationException}
     *
     * @return HTTP code bad request with the error message "wrong inputs"
     */
    @ExceptionHandler({DataIntegrityViolationException.class, InvalidDataAccessApiUsageException.class})
    public ResponseEntity<?> invalidInputs() {
        return new ResponseEntity<>("Wrong inputs", HttpStatus.BAD_REQUEST);
    }


    /**
     * Exception handler for the {@link AuthenticationException}
     *
     * @param exception generated exception
     * @return HTTP code unauthorized
     */
    @ExceptionHandler({AuthenticationException.class})
    public ResponseEntity<String> handleAuthenticationException(AuthenticationException exception) {
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(exception.getMessage());
    }


}
