package com.abrahamandrusso.api.v4.controller.service;

import com.abrahamandrusso.api.v4.model.dto.DayDTO;
import com.abrahamandrusso.api.v4.model.resource.planning.DefaultPlanning;
import com.abrahamandrusso.api.v4.service.planning.IPlanningService;
import com.abrahamandrusso.api.v4.service.planning.error.PlanningException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Benahmed Sofiane on 03/04/18 16:23
 * <p>
 * Class Controller
 * Responsible for the handling of plannings
 * </p>
 */

@RestController
@RequestMapping("/planning-service/")
public class PlanningController {


    // ~ Declarations
    //==============================================================================================

    private final IPlanningService planningService;


    // ~ Constructors
    //==============================================================================================

    /**
     * @param planningService : service responsible for the plannings
     */
    public PlanningController(IPlanningService planningService) {
        this.planningService = planningService;
    }


    // ~ Methods
    //==============================================================================================

    /**
     * Methods that returns the planning for a specific date
     *
     * @param date : the date wrapped in a dto
     * @return the planning of the equivalent data
     * @throws PlanningException if no planning is available
     */
    @PostMapping("day-planning")
    public ResponseEntity<DefaultPlanning> getDayPlanning(@RequestBody DayDTO date) throws PlanningException {
        DefaultPlanning defaultPlanningForDate = planningService.findPlanningForDate(date);
        return new ResponseEntity<>(defaultPlanningForDate, HttpStatus.OK);
    }


}
