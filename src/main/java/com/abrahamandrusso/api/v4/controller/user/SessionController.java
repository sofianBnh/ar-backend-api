package com.abrahamandrusso.api.v4.controller.user;

import com.abrahamandrusso.api.v4.model.dto.LoginDTO;
import com.abrahamandrusso.api.v4.model.pojo.ServiceResponse;
import com.abrahamandrusso.api.v4.model.response.JWTResponse;
import com.abrahamandrusso.api.v4.service.authentication.error.AuthenticationException;
import com.abrahamandrusso.api.v4.service.session.ISessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

import static com.abrahamandrusso.api.v4.utils.request.RequestUtils.extractTokenValue;
import static com.abrahamandrusso.api.v4.utils.response.ControllerUtils.buildResponse;

/**
 * @author Benhamed Sofiane on 17/03/18 15:25
 * <p>
 * Class Controller
 * Responsible for the handling of seesions
 * </p>
 */

@RestController
public class SessionController {


    // ~ Declarations
    //=============================================================================================

    private final ISessionService service;


    //~ Application Variables
    //==============================================================================================

    @Value("${jwt.token-bearer}")
    private String bearer;

    @Value("${jwt.token-header}")
    private String tokenHeader;


    // ~ Constructors
    //=============================================================================================

    /**
     * @param service : Session Service responsible for the creation and validation of
     *                authentication tokens
     */
    @Autowired
    public SessionController(ISessionService service) {
        this.service = service;
    }


    // ~ Routes
    //=============================================================================================

    /**
     * Route for the connection of a user
     *
     * @param authenticationRequest : Login credentials (username, password)
     * @return a JWT for the session
     * @throws AuthenticationException if the credentials are wrong
     */
    @PostMapping("/auth")
    public ResponseEntity<?> createAuthenticationToken(@RequestBody LoginDTO authenticationRequest)
            throws AuthenticationException {

        final String token = service.createAuthenticationToken(authenticationRequest).getContent();

        return ResponseEntity.ok(new JWTResponse(token));

    }

    /**
     * Route for the recovering of the current user id
     *
     * @param request : originating Http request
     * @return the user id
     */
    @GetMapping("/user-id")
    public ResponseEntity<?> getUserId(HttpServletRequest request) {


        // getting the token from the request
        final String token = extractTokenValue(request, tokenHeader, bearer);

        if (token == null)
            return buildResponse(new ServiceResponse(false, "Token not found"));

        // getting the id
        ServiceResponse response = service.getCurrentUserId(token);

        return buildResponse(response);
    }


    /**
     * Route for the refresh of a token
     *
     * @param request : originating Http request
     * @return the refreshed token
     */
    @GetMapping("/refresh")
    public ResponseEntity<?> refreshAuthenticationToken(HttpServletRequest request) {

        // getting the token from the request
        final String token = extractTokenValue(request, tokenHeader, bearer);

        if (token == null)
            return buildResponse(new ServiceResponse(false, "Token not found"));


        // getting the refreshed token
        ServiceResponse response = service.refreshAndGetAuthenticationToken(token);

        return buildResponse(response);

    }


    /**
     * Method that checks if a user has the role admin
     *
     * @return the http code 200 if he is an admin
     */
    @GetMapping("/admin")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> checkIfAdmin() {
        return ResponseEntity.ok().body("");
    }

}
