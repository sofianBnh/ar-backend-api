package com.abrahamandrusso.api.v4.controller.resource;

import com.abrahamandrusso.api.v4.model.dto.RestaurantDTO;
import com.abrahamandrusso.api.v4.service.restaurant.IRestaurantDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author Benahmed Sofiane on 03/04/18 16:23
 * <p>
 * Class Controller
 * Responsible for the handling of restaurant
 * resources
 * </p>
 */

@RestController
@RequestMapping("/restaurant-resources/")
public class RestaurantDataController {

    // ~ Declarations
    //==========================================================================================

    private final IRestaurantDataService service;

    // ~ Constructors
    //==========================================================================================

    /**
     * @param service : Restaurant Resources Service : provides restaurant related resources
     */
    @Autowired
    public RestaurantDataController(IRestaurantDataService service) {
        this.service = service;
    }

    // ~ Routes
    //==========================================================================================

    /**
     * Route for returning the list of restaurants in the form of {@link RestaurantDTO}
     *
     * @return list of reservations
     */
    @GetMapping("restaurants")
    @PreAuthorize("permitAll()")
    public List<RestaurantDTO> findAll() {
        return service.findAll();
    }

}
