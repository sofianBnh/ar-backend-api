package com.abrahamandrusso.api.v4.service.restaurant;

import com.abrahamandrusso.api.v4.model.dto.RestaurantDTO;

import java.util.List;

/**
 * @author Benahmed Sofiane on 03/04/18 17:45
 * <p>
 * Interface Service
 * Handles the preparation of the data sent to the front end
 */

public interface IRestaurantDataService {

    /**
     * Method that returns all restaurants' data
     *
     * @return list of restaurant dtos
     */
    List<RestaurantDTO> findAll();

}
