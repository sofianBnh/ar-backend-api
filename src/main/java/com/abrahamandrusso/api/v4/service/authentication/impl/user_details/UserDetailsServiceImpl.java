package com.abrahamandrusso.api.v4.service.authentication.impl.user_details;

import com.abrahamandrusso.api.v4.model.user.User;
import com.abrahamandrusso.api.v4.repository.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * @author Benhamed Sofiane on 18/03/18 20:44
 * <p>
 * Class Service
 * Handles the verification of the existance of the
 * username sent during the login
 */

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    // ~ Declarations
    //==========================================================================================


    private final UserRepository userRepository;

    // ~ Constructors
    //==========================================================================================

    /**
     * @param userRepository reppository handling {@link User} entities
     */
    @Autowired
    public UserDetailsServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    /**
     * Method that loads a User based on their username
     *
     * @param username username of the user
     * @return the UserDetails assassinated with username
     * @throws UsernameNotFoundException if the user does not exist
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        // query the user
        User user = userRepository.findByUsername(username);

        // checks the presence of a user
        if (user == null)
            throw new UsernameNotFoundException("Username or/and Password incorrect");

        // return the user
        return new UserDetailsImpl(user);

    }
}