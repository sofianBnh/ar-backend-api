package com.abrahamandrusso.api.v4.service.error;

/**
 * @author Benhamed Sofiane on 23/03/18 15:43
 * <p>
 * Class Exception
 * Thrown when an invalid data is detected
 */

public class InvalidDataException extends ServiceException {

    // ~ Constructors
    //==========================================================================================

    public InvalidDataException() {
        super("Fields Error");
    }
}
