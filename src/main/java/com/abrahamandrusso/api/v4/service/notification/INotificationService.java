package com.abrahamandrusso.api.v4.service.notification;

import com.abrahamandrusso.api.v4.model.pojo.Notification;
import com.abrahamandrusso.api.v4.service.error.EmailNotSentException;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Benahmed Sofiane on 01/04/18 20:40
 * <p>
 * Interface Service
 * Handles the notifications and interactions with the user
 */

public interface INotificationService {

    /**
     * Method that sends a confirmation notification to a user
     *
     * @param notification : Notification object containing an id
     * @throws EmailNotSentException if the email was not sent correctly
     */
    @Transactional
    void sendConfirmationNotification(Notification notification) throws EmailNotSentException;

    /**
     * Method that sends a simple notification to a user
     *
     * @param notification : Notification object containing an id
     * @throws EmailNotSentException if the email was not sent correctly
     */
    @Transactional
    void sendSimpleNotification(Notification notification) throws EmailNotSentException;

}
