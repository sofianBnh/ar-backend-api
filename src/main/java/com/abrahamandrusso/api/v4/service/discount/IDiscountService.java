package com.abrahamandrusso.api.v4.service.discount;

import com.abrahamandrusso.api.v4.model.resource.Reservation;
import com.abrahamandrusso.api.v4.model.resource.discount.Reducible;
import com.abrahamandrusso.api.v4.service.discount.error.DiscountException;

import javax.transaction.Transactional;

/**
 * @author Benahmed Sofiane on 31/04/18 13:40
 * <p>
 * Interface Service
 * Handles the discounts on reservations
 */

public interface IDiscountService {

    /**
     * Method that calculates the initial price of
     * a reservation based on the price of the meals
     *
     * @param reservation reservation with an order
     * @return the total price of the reservation
     * @throws DiscountException if there are no meals
     */
    @Transactional
    Double calculateInitialPrice(Reservation reservation)
            throws DiscountException;

    /**
     * Method that applies the automatic discount on a
     * reservation if it is applicable
     *
     * @param reservation reservation with initial price
     *                    calculated
     * @return the discounted price
     * @throws DiscountException if an error occurred while
     *                           calculating the new price
     */
    @Transactional
    Double calculateWithAutomaticDiscount(Reservation reservation)
            throws DiscountException;

    /**
     * Method that calculates the discounted price of a
     * reservation
     *
     * @param reservation reservation with initial price
     * @return the discounted price
     * @throws DiscountException if an error occurred while
     *                           calculating the new price
     */
    @Transactional
    Double calculateDiscountedPrice(Reservation reservation)
            throws DiscountException;

    /**
     * Method that get data about the automatic discount
     *
     * @return the default discount
     * @throws DiscountException if there is no default
     *                           discount
     */
    @Transactional
    Reducible getCurrentAutomaticDiscount()
            throws DiscountException;

}
