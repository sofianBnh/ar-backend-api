package com.abrahamandrusso.api.v4.service.registration.impl;

import com.abrahamandrusso.api.v4.model.dto.UserDTO;
import com.abrahamandrusso.api.v4.model.pojo.Notification;
import com.abrahamandrusso.api.v4.model.user.User;
import com.abrahamandrusso.api.v4.model.user.role.Role;
import com.abrahamandrusso.api.v4.model.user.role.Roles;
import com.abrahamandrusso.api.v4.repository.user.RoleRepository;
import com.abrahamandrusso.api.v4.repository.user.UserRepository;
import com.abrahamandrusso.api.v4.service.error.ConfirmationException;
import com.abrahamandrusso.api.v4.service.error.InvalidDataException;
import com.abrahamandrusso.api.v4.service.error.InvalidRequestException;
import com.abrahamandrusso.api.v4.service.error.ServiceException;
import com.abrahamandrusso.api.v4.service.registration.IUserRegistrationService;
import com.abrahamandrusso.api.v4.service.registration.error.EmailAlreadyUsedException;
import com.abrahamandrusso.api.v4.service.registration.error.UsernameAlreadyUsedException;
import com.abrahamandrusso.api.v4.utils.jwt.IJWTHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Optional;

import static com.abrahamandrusso.api.v4.model.pojo.Notification.NotificationBuilder.aNotification;

/**
 * @author Benhamed Sofiane on 02/03/18 17:02
 * Class Service
 * Handles the registration of
 * {@link User}
 */
@Service
public class UserRegistrationServiceImpl implements IUserRegistrationService {


    //~ Declarations
    //==============================================================================================

    private final UserRepository userRepository;

    private final RoleRepository roleRepository;

    private final IJWTHolder holder;

    private final PasswordEncoder encoder;


    //~ Application Variables
    //==============================================================================================


    @Value("${api.domain}")
    private String domain;

    @Value("${api.user.confirm}")
    private String confirmationPath;


    // ~ Constructors
    //==========================================================================================


    /**
     * @param userRepository : repository for {@link User} entities
     * @param roleRepository : repository for {@link Role} entities
     * @param holder         : JWT holder
     * @param encoder        : encoder for the passwords
     */
    @Autowired
    public UserRegistrationServiceImpl(
            UserRepository userRepository,
            RoleRepository roleRepository,
            IJWTHolder holder,
            @Qualifier("passwordEncoderBean") PasswordEncoder encoder
    ) {

        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.holder = holder;
        this.encoder = encoder;
    }


    // ~ Methods
    //==========================================================================================

    /**
     * Method that registers a user
     *
     * @param user: dto of a user
     * @return a notification of verification
     * @throws ServiceException in case of a problem while saving the user
     */
    @Override
    public Notification signUp(UserDTO user) throws ServiceException {

        // validating the request
        validateUserRequest(user);

        // saving the user
        User saved = saveUser(user);

        // sending the notification object
        return aNotification()
                .withUserName(saved.getName())
                .withUserEmail(saved.getEmail())
                .withObjectId(saved.getId().toString())
                .withSubject("Verify your account")
                .withOperation("Account verification")
                .withRoute("registration-service")
                .build();
    }


    /**
     * Method that confirms a user
     *
     * @param token : jwt sent to the user
     * @return a simple notification
     * @throws ConfirmationException in case there was a problem while
     *                               confirming the registration
     */
    @Override
    public Notification confirm(String token) throws ConfirmationException {

        // updating the holder
        holder.setToken(token);

        // getting the subject
        String subject = holder.getSubject();

        // throw an exception if no subject is found
        if (subject == null)
            throw new ConfirmationException("Bad Token");

        // getting the user id
        Long id = Long.parseLong(subject);

        // querying the user from the database
        Optional<User> user = userRepository.findById(id);

        // checking if it is present
        if (!user.isPresent())
            throw new ConfirmationException("User Not Found");

        // enabling the user
        user.get().setEnabled(true);

        // sending the notification object
        return aNotification()
                .withUserEmail(user.get().getUsername())
                .withUserEmail(user.get().getEmail())
                .withSubject("Account Verification")
                .withOperation("Verify your account")
                .build();

    }

    /**
     * Method that removes a user i.e. deletes his registration
     *
     * @param id : user id
     * @return a simple notification
     * @throws InvalidRequestException in case there was a problem while deleting
     *                                 the user
     */
    @Override
    public Notification unSignUp(Long id) throws InvalidRequestException {

        // querying the user
        Optional<User> user = userRepository.findById(id);

        // checking its presence
        if (!user.isPresent())
            throw new InvalidRequestException("User not found");

        // removing the user from the database
        userRepository.deleteById(id);

        // sending the notification object
        return aNotification()
                .withUserEmail(user.get().getUsername())
                .withUserEmail(user.get().getEmail())
                .withSubject("Account Deleted")
                .withOperation("Your account has been deleted")
                .build();
    }


    //~ Private Methods
    //==============================================================================================


    /**
     * Method that validates a sign-up request
     *
     * @param user : dto of the request
     * @throws InvalidRequestException if the request is invalid
     */
    private void validateUserRequest(UserDTO user) throws InvalidRequestException {

        // checking if the email is not already used
        if (userRepository.existsByEmail(user.getEmail()))
            throw new EmailAlreadyUsedException();

        // checking if the username is not already used
        if (userRepository.existsByUsername(user.getUsername()))
            throw new UsernameAlreadyUsedException();
    }


    /**
     * Method that saves a user from a dto
     *
     * @param user: dto of the user
     * @return the saved entity
     * @throws InvalidDataException if there was an error while saving
     */
    private User saveUser(UserDTO user) throws InvalidDataException {
        // getting the user role
        Role userRole = roleRepository.findByName(Roles.ROLE_USER);

        // generating the user entity
        User generated = new User(
                user.getName(),
                user.getEmail(),
                user.getPhone(),
                user.getUsername(),
                encoder.encode(user.getPassword()),
                Collections.singletonList(userRole)
        );

        generated.setEnabled(true); // todo remove on production

        // saving the user in the database
        userRepository.save(generated);

        // querying the new user entity
        User saved = userRepository.findByUsername(generated.getUsername());

        // checking if it is present
        if (saved == null)
            throw new InvalidDataException();

        // returning the entity
        return saved;

    }


}
