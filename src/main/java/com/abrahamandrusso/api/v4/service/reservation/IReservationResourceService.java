package com.abrahamandrusso.api.v4.service.reservation;

import com.abrahamandrusso.api.v4.model.dto.ReservationDTO;
import com.abrahamandrusso.api.v4.model.pojo.Notification;
import com.abrahamandrusso.api.v4.model.pojo.ServiceResponse;
import com.abrahamandrusso.api.v4.model.resource.Reservation;
import com.abrahamandrusso.api.v4.service.error.ConfirmationException;
import com.abrahamandrusso.api.v4.service.error.ServiceException;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Benahmed Sofiane on 30/03/18 20:28
 * <p>
 * Interface Service
 * Handles CRUD operations concerning Reservations
 */

public interface IReservationResourceService {

    /**
     * Method for the saving of {@link Reservation} entities
     *
     * @param clientService service that handles the buildReservation of the reservation
     * @param dto           DTO for the reservation
     * @return the final reservation
     * @throws ServiceException if a problem occurred while saving the new reservation
     */
    @Transactional
    Reservation save(IReservationClientService clientService, ReservationDTO dto)
            throws ServiceException;

    /**
     * Method for the update of {@link Reservation} entities
     *
     * @param reservation : reservation object
     * @return a service response
     * @throws ServiceException if a problem occurred while updating a reservation
     */
    @Transactional
    Reservation update(Reservation reservation) throws ServiceException;


    /**
     * Method for the deletion of {@link Reservation} entites
     *
     * @param id    : id of the reservation
     * @param token : token of the user
     * @return a service response
     * @throws ServiceException if a problem occurred while deleting a reservation
     */
    @Transactional
    ServiceResponse delete(String token, Long id) throws ServiceException;

    /**
     * Method for finding a user's reservations
     *
     * @param token : user's token
     * @return list of reservations
     * @throws ServiceException if the user is not valid
     */
    @Transactional
    List<Reservation> findReservationsForToken(String token) throws ServiceException;

    /**
     * Method for the confirmation of a {@link Reservation}
     *
     * @param token : token sent to the user
     * @return a simple notification
     * @throws ConfirmationException if the confirmation is not made
     */
    @Transactional
    Notification confirm(String token) throws ConfirmationException;

}
