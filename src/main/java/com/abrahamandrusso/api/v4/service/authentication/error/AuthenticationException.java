package com.abrahamandrusso.api.v4.service.authentication.error;

/**
 * @author Benhamed Sofiane on 18/03/18 20:42
 * <p>
 * Class Exception
 * Thrown when an Authentication error occurrs
 */
public class AuthenticationException extends RuntimeException {

    private static final long serialVersionUID = -2987381216460248651L;

    // ~ Constructors
    //==========================================================================================

    public AuthenticationException(String message, Throwable cause) {
        super(message, cause);
    }

}
