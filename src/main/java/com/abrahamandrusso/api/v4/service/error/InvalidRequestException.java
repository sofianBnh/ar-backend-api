package com.abrahamandrusso.api.v4.service.error;

/**
 * @author Benhamed Sofiane on 23/03/18 15:41
 * <p>
 * Class Exception
 * Thrown when an invalid request if being validated
 */

public class InvalidRequestException extends ServiceException {

    // ~ Constructors
    //==========================================================================================

    public InvalidRequestException(String message) {
        super(message);
    }
}
