package com.abrahamandrusso.api.v4.service.planning.error;

import com.abrahamandrusso.api.v4.service.error.ServiceException;

/**
 * @author Benhamed Sofiane on 28/05/18 13:20
 * <p>
 * Class Exception
 * Thrown when the default planning is not found or
 * an error occures while checking times with a planning
 */
public class PlanningException extends ServiceException {

    // ~ Constructors
    //==========================================================================================

    public PlanningException(String message) {
        super(message);
    }
}
