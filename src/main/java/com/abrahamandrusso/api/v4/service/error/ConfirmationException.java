package com.abrahamandrusso.api.v4.service.error;

/**
 * @author Benhamed Sofiane on 13/04/18 22:42
 * <p>
 * Class Exception
 * Thrown when an error occurrs when preformiing a Confirmation
 */
public class ConfirmationException extends ServiceException {

    // ~ Constructors
    //==========================================================================================

    public ConfirmationException(String message) {
        super(message);
    }
}
