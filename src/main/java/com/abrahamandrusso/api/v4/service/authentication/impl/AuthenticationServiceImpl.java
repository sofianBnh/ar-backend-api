package com.abrahamandrusso.api.v4.service.authentication.impl;

import com.abrahamandrusso.api.v4.service.authentication.IAuthenticationService;
import com.abrahamandrusso.api.v4.utils.jwt.IJWTHolder;
import io.jsonwebtoken.JwtException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;

/**
 * @author Benhamed Sofiane on 18/03/18 20:43
 * <p>
 * Class Service
 * Handles the verification of the access right
 */


@Component
public class AuthenticationServiceImpl implements IAuthenticationService {


    // ~ Declarations
    //==========================================================================================

    private final IJWTHolder holder;

    private final UserDetailsService userDetailsService;


    // ~ Constructors
    //==========================================================================================

    @Autowired
    public AuthenticationServiceImpl(
            @Qualifier("userDetailsServiceImpl") UserDetailsService userDetailsService,
            IJWTHolder holder
    ) {
        this.userDetailsService = userDetailsService;
        this.holder = holder;
    }

    // ~ Methods
    //==========================================================================================

    /**
     * Method that checks if a token is allowed to pass
     *
     * @param token value of JWT
     * @return the User details of the owner or null
     */
    @Override
    public UserDetails checkIfAllowed(String token) {

        String username;

        // updating the holder
        holder.setToken(token);

        try {
            // extracting the username
            username = holder.getSubject();

        } catch (IllegalArgumentException | JwtException e) {
            return null;
        }

        // if a username is found
        if (username != null) {

            // getting the user details instance of the user
            UserDetails userDetails = userDetailsService.loadUserByUsername(username);

            // if the token is valid and the user is enabled returns a UserDetails
            if (holder.isValidToken(userDetails) && userDetails.isEnabled()) {
                return userDetails;
            }

        }

        return null;

    }

}
