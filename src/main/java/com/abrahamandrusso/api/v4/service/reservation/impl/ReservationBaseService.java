package com.abrahamandrusso.api.v4.service.reservation.impl;

import com.abrahamandrusso.api.v4.model.dto.ReservationDTO;
import com.abrahamandrusso.api.v4.model.resource.Reservation;
import com.abrahamandrusso.api.v4.model.resource.Restaurant;
import com.abrahamandrusso.api.v4.model.resource.RestaurantTable;
import com.abrahamandrusso.api.v4.model.user.SimpleUser;
import com.abrahamandrusso.api.v4.repository.resource.ReservationRepository;
import com.abrahamandrusso.api.v4.repository.resource.RestaurantRepository;
import com.abrahamandrusso.api.v4.service.error.InvalidDataException;
import com.abrahamandrusso.api.v4.service.error.InvalidRequestException;
import com.abrahamandrusso.api.v4.service.error.NotEnoughPlacesException;
import com.abrahamandrusso.api.v4.service.error.ServiceException;
import com.abrahamandrusso.api.v4.service.planning.IPlanningService;
import com.abrahamandrusso.api.v4.service.table.IRestaurantTableService;
import com.abrahamandrusso.api.v4.utils.time.ITimeHandler;
import org.springframework.beans.factory.annotation.Value;

import java.util.Calendar;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


/**
 * @author Benahmed Sofiane on 30/03/18 22:13
 * <p>
 * Super Class Service
 * Handles the shared behaviors for the buildReservation
 * of {@link Reservation} entities
 */

public abstract class ReservationBaseService {

    // ~ Declarations
    //==========================================================================================

    private final IPlanningService planningService;

    private final IRestaurantTableService restaurantTableService;

    private final RestaurantRepository restaurantRepository;

    protected final ReservationRepository reservationRepository;

    private final ITimeHandler timeHandler;


    //~ Application Variables
    //==============================================================================================

    @Value("${api.reservation.max-hours}")
    private int numberOfMaxHoursForReservation;

    @Value("${api.reservation.deadline.day-count}")
    private int numberOfDaysBeforeDeadline;

    @Value("${api.reservation.hours-before-reservation}")
    private int numberOfHoursBeforeReservation;


    // ~ Constructors
    //==========================================================================================

    /**
     * @param planningService        : service for the planning integration
     * @param restaurantTableService : service fot the integration of tables
     * @param reservationRepository  : repository for {@link Reservation} entities
     * @param restaurantRepository   : repository for {@link Restaurant} entities
     * @param timeHandler            : time operations class
     */
    protected ReservationBaseService(
            IRestaurantTableService restaurantTableService,
            ReservationRepository reservationRepository,
            RestaurantRepository restaurantRepository,
            IPlanningService planningService,
            ITimeHandler timeHandler
    ) {
        this.restaurantTableService = restaurantTableService;
        this.reservationRepository = reservationRepository;
        this.restaurantRepository = restaurantRepository;
        this.planningService = planningService;
        this.timeHandler = timeHandler;
    }


    // ~ Methods
    //==========================================================================================

    /**
     * Method that creates a {@link Reservation} from a dto
     *
     * @param dto :  reservation dto
     * @return a reservation object
     * @throws ServiceException if the dto in invalid
     */
    public Reservation make(ReservationDTO dto) throws ServiceException {

        // validate the reservation request
        validateRequest(dto);

        // checking the times with the opening times
        planningService.checkTimesWithCurrentPlanning(dto);

        // get the user from the reservation
        SimpleUser user = getUser(dto);

        // get the restaurant from the reservation
        Restaurant restaurant = getRestaurant(dto);

        // getting the reservations occurring at the same time
        List<Reservation> sameTime = getReservationsOccurringAtTheSameTime(dto);

        // check the availability of the restaurant
        checkAvailability(dto, restaurant, sameTime);

        // getting the tables
        List<RestaurantTable> tables = restaurantTableService
                .getTablesForReservation(dto, restaurant, sameTime);

        // return the buildReservation of the reservation
        return makeReservation(dto, restaurant, user, tables);

    }

    // ~ Abstract Methods
    //==========================================================================================

    /**
     * Method for validating a reservation dto
     *
     * @param reservation : reservation dto
     * @throws InvalidRequestException if the request is invalid
     */
    abstract void validateRequest(ReservationDTO reservation) throws InvalidRequestException;

    /**
     * Method for getting the owner of a reservation dto
     *
     * @param dto : reservation dto
     * @return the owner of the reservation
     * @throws InvalidDataException if the user is not found
     */
    abstract SimpleUser getUser(ReservationDTO dto) throws InvalidDataException;

    // ~ Private Methods
    //==========================================================================================

    /**
     * Method that gets the restaurant from the reservation dto
     *
     * @param dto reservation dto
     * @return the restaurant of the reservation
     * @throws InvalidDataException if the restaurant dose not exist
     */
    private Restaurant getRestaurant(ReservationDTO dto) throws InvalidDataException {

        // query the restaurant
        Optional<Restaurant> restaurant = restaurantRepository
                .findById(dto.getRestaurantId());

        // check its presence
        if (!restaurant.isPresent())
            throw new InvalidDataException();

        return restaurant.get();
    }


    /**
     * Method that checks the restaurant's availability for a reservation dto
     *
     * @param dto        reservation dto
     * @param restaurant restaurant of the reservation
     * @param sameTime   reservations occurring at the same time as the dto
     * @throws NotEnoughPlacesException if there are not enough places
     */
    private void checkAvailability(
            ReservationDTO dto,
            Restaurant restaurant,
            List<Reservation> sameTime
    ) throws NotEnoughPlacesException {

        // get the number of available places in the restaurant
        final int numberOfPlacesInRestaurant = restaurant.getNumberOfPlaces();

        // get the number of places needed in the reservation
        final int numberOfNeededPlaces = dto.getNumberOfPlaces();

        // calculate the number of taken places
        final int numberOfTakenPlaces = sameTime.parallelStream()
                .mapToInt(Reservation::getNumberOfPlaces).sum();

        // calculating the number of places left in the restaurant
        final int numberOfPlacesLeft = numberOfPlacesInRestaurant - numberOfTakenPlaces;

        // check if the free places are enough
        final boolean enough = numberOfNeededPlaces <= numberOfPlacesLeft;

        // if it is not enough throw an exception
        if (!enough)
            throw new NotEnoughPlacesException();

    }


    /**
     * Method that gets all the reservations occurring at the same times
     * as the ones int he dto and in the same restaurant
     *
     * @param dto DTO for reservation
     * @return the list of reservation
     */
    private List<Reservation> getReservationsOccurringAtTheSameTime(ReservationDTO dto) {

        // getting the date of the reservation
        Calendar reservationDate = dto.getDate();

        // query all reservation occurring at in the same day at the same restaurant
        List<Reservation> sameDate = reservationRepository
                .findByDateAndRestaurantId(reservationDate, dto.getRestaurantId());

        // filtering the reservation by time crossing the reservation time
        return sameDate.parallelStream().filter(

                reservation -> {
                    // check if the reservation is confirmed and it crosses with the current one
                    return reservation.isConfirmed() &&
                            timeHandler.crossWith(
                                    reservation.getStartTime(),
                                    reservation.getEndTime(),
                                    dto.getStartTime(),
                                    dto.getEndTime()
                            );
                }
                // collecting the filtered reservations
        ).collect(Collectors.toList());

    }

    // ~ Shared Methods
    //==============================================================================================

    /**
     * Method that validates the times of a reservation dto
     *
     * @param dto : reservation dto
     * @throws InvalidRequestException if the reservation is not valid
     */
    void validateTimes(ReservationDTO dto) throws InvalidRequestException {

        // checking if the times are not null
        if (dto.getStartTime() == null || dto.getEndTime() == null)
            throw new InvalidRequestException("Bad times");


        // check if the reservation has correct times
        if (dto.getStartTime().after(dto.getEndTime()))
            throw new InvalidRequestException("Incorrect Times");


        // check if the reservation is before the current date
        if (dto.getDate().before(timeHandler.getCurrentDate()))
            throw new InvalidRequestException("Reservation Before Today");


        // check the number of hours before the reservation
        if (timeHandler.hoursBetween(dto.getStartTime(), Calendar.getInstance())
                < numberOfHoursBeforeReservation)
            throw new InvalidRequestException("Time Before Reservation is too short");


        // check the number of hours of the reservation
        if (timeHandler.hoursBetween(dto.getEndTime(), dto.getStartTime())
                >= numberOfMaxHoursForReservation)
            throw new InvalidRequestException("Period Exceeding "
                    + numberOfMaxHoursForReservation + " hours.");

    }

    /**
     * Method that validates the restaurant from the reservation dto
     *
     * @param dto : reservation dto
     * @throws InvalidRequestException if the restaurant is not existent
     */
    void validateRestaurant(ReservationDTO dto) throws InvalidRequestException {

        // check if the restaurant exists
        if (!restaurantRepository.existsById(dto.getRestaurantId()))
            throw new InvalidRequestException("Restaurant Does not Exist");

    }

    /**
     * Method that builds a {@link Reservation} from {@link ReservationDTO}
     *
     * @param dto        reservation dto
     * @param restaurant restaurant of the reservation
     * @param user       owner of the reservation
     * @param tables     tables of the reservation
     * @return a Reservation entity
     */
    private Reservation makeReservation(
            ReservationDTO dto,
            Restaurant restaurant,
            SimpleUser user,
            List<RestaurantTable> tables
    ) {

        return new Reservation(
                user,
                restaurant,
                dto.getStartTime(),
                dto.getEndTime(),
                dto.getDate(),
                tables,
                dto.getMeals()
        );

    }

}
