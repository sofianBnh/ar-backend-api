package com.abrahamandrusso.api.v4.service.order;

import com.abrahamandrusso.api.v4.model.dto.OrderDTO;
import com.abrahamandrusso.api.v4.model.resource.Reservation;
import com.abrahamandrusso.api.v4.service.order.error.OrderException;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Benahmed Sofiane on 31/04/18 13:40
 * <p>
 * Interface Service
 * Handles the Orders
 */

public interface IOrderService {

    /**
     * Method that adds an order to a reservation
     *
     * @param dto   the order
     * @param token the token of the user who requested it
     * @return the reservation adter alteration
     * @throws OrderException if there is a problem while adding the order
     */
    @Transactional
    Reservation addOrder(OrderDTO dto, String token)
            throws OrderException;

}
