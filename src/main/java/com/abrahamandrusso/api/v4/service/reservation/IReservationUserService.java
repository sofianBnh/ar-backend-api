package com.abrahamandrusso.api.v4.service.reservation;

import com.abrahamandrusso.api.v4.model.dto.ReservationDTO;
import com.abrahamandrusso.api.v4.model.resource.Reservation;
import com.abrahamandrusso.api.v4.service.error.ServiceException;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Benahmed Sofiane on 30/03/18 20:27
 * <p>
 * Interface Service
 * Handles the creation of simple users' reservations
 */

public interface IReservationUserService extends IReservationClientService {

    @Transactional
    Reservation buildReservation(ReservationDTO dto, String token, Long id) throws ServiceException;

}
