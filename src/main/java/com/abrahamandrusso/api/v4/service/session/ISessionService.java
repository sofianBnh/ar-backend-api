package com.abrahamandrusso.api.v4.service.session;

import com.abrahamandrusso.api.v4.model.dto.LoginDTO;
import com.abrahamandrusso.api.v4.model.pojo.ServiceResponse;
import org.springframework.security.core.AuthenticationException;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Benhamed Sofiane on 18/03/18 20:36
 * <p>
 * Interface Service
 * Handes the sessions tokens creation and refresh
 */

public interface ISessionService {

    /**
     * Method that creates a token using the username and password
     *
     * @param login user credentials
     * @return a token in a {@link ServiceResponse}
     * @throws AuthenticationException if the credentials are incorrect
     */
    @Transactional
    ServiceResponse createAuthenticationToken(LoginDTO login) throws AuthenticationException;

    /**
     * Method that refreshes a token using the old one
     *
     * @param authToken current token
     * @return an updated token
     */
    @Transactional
    ServiceResponse refreshAndGetAuthenticationToken(String authToken);

    /**
     * Method that returns the current user's id
     *
     * @param authToken user's token
     * @return the user id
     */
    @Transactional
    ServiceResponse getCurrentUserId(String authToken);
}