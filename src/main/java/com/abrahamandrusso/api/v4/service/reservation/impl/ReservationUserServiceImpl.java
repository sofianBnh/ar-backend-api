package com.abrahamandrusso.api.v4.service.reservation.impl;

import com.abrahamandrusso.api.v4.model.dto.ReservationDTO;
import com.abrahamandrusso.api.v4.model.resource.Reservation;
import com.abrahamandrusso.api.v4.model.resource.Restaurant;
import com.abrahamandrusso.api.v4.model.user.User;
import com.abrahamandrusso.api.v4.repository.resource.ReservationRepository;
import com.abrahamandrusso.api.v4.repository.resource.RestaurantRepository;
import com.abrahamandrusso.api.v4.repository.user.UserRepository;
import com.abrahamandrusso.api.v4.service.error.InvalidDataException;
import com.abrahamandrusso.api.v4.service.error.InvalidRequestException;
import com.abrahamandrusso.api.v4.service.error.ServiceException;
import com.abrahamandrusso.api.v4.service.planning.IPlanningService;
import com.abrahamandrusso.api.v4.service.reservation.IReservationUserService;
import com.abrahamandrusso.api.v4.service.table.IRestaurantTableService;
import com.abrahamandrusso.api.v4.utils.jwt.IJWTHolder;
import com.abrahamandrusso.api.v4.utils.time.ITimeHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * @author Benahmed Sofiane on 30/03/18 20:42
 * <p>
 * Class Service
 * Handles the buildReservation of {@link User} reservations
 */

@Service
public class ReservationUserServiceImpl extends ReservationBaseService
        implements IReservationUserService {


    // ~ Declarations
    //==========================================================================================

    private final UserRepository userRepository;

    private final IJWTHolder holder;


    // ~ Constructors
    //==========================================================================================

    /**
     * @param reservationRepository : repository for {@link Reservation} entities
     * @param restaurantRepository  : repository for {@link Restaurant} entities
     * @param userRepository        : repository for {@link User} entities
     * @param timeHandler           : time operations class
     * @param holder                : JWT token holder
     */
    @Autowired
    protected ReservationUserServiceImpl(
            IRestaurantTableService restaurantTableService,
            ReservationRepository reservationRepository,
            RestaurantRepository restaurantRepository,
            IPlanningService planningService,
            UserRepository userRepository,
            ITimeHandler timeHandler,
            IJWTHolder holder
    ) {
        super(
                restaurantTableService,
                reservationRepository,
                restaurantRepository,
                planningService,
                timeHandler
        );
        this.userRepository = userRepository;
        this.holder = holder;
    }


    // ~ Methods
    //==========================================================================================

    /**
     * Method for validating a reservation dto
     *
     * @param dto : reservation dto
     * @throws InvalidRequestException if the request is invalid
     */
    @Override
    void validateRequest(ReservationDTO dto) throws InvalidRequestException {

        validateRestaurant(dto);

        if (!userRepository.existsById(dto.getUserId()))
            throw new InvalidRequestException("User not Confirmed");

        validateTimes(dto);

    }

    /**
     * Method for getting the owner of a reservation dto
     *
     * @param dto : reservation dto
     * @return the owner of the reservation
     * @throws InvalidDataException if the user is not found
     */
    @Override
    User getUser(ReservationDTO dto) throws InvalidDataException {

        Optional<User> user = userRepository.findById(dto.getUserId());

        if (!user.isPresent())
            throw new InvalidDataException();

        return user.get();
    }

    @Override
    public Reservation buildReservation(ReservationDTO dto, String token, Long id)
            throws ServiceException {

        // getting the reservation
        Optional<Reservation> reservation = reservationRepository.findById(id);
        if (!reservation.isPresent())
            throw new InvalidRequestException("Reservation not found");


        // check the ownership
        holder.setToken(token);

        final String reservationOwner = ((User) reservation.get().getUser()).getUsername();

        if (!reservationOwner.equals(holder.getSubject()))
            throw new InvalidRequestException("Wrong Owner");

        // make one
        Reservation built = make(dto);

        // map the new attributes to the old reservation
        Reservation updated = reservation.get();

        updated.setRestaurant(built.getRestaurant());
        updated.setStartTime(built.getStartTime());
        updated.setConfirmed(built.isConfirmed());
        updated.setEndTime(built.getEndTime());
        updated.setTables(built.getTables());
        updated.setDate(built.getDate());

        updated.setConfirmed(true); // todo remove on production

        return updated;
    }
}
