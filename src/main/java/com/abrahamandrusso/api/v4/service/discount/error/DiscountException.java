package com.abrahamandrusso.api.v4.service.discount.error;

import com.abrahamandrusso.api.v4.service.error.ServiceException;

/**
 * @author Benhamed Sofiane on 31/04/18 13:40
 * <p>
 * Class Exception
 * Thrown when the default Discount is not found or
 * an error occures while applying discounts
 */
public class DiscountException extends ServiceException {

    // ~ Constructors
    //==========================================================================================

    public DiscountException() {
        super("Error while applying discount");
    }

    public DiscountException(String message) {
        super(message);
    }
}
