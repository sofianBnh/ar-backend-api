package com.abrahamandrusso.api.v4.service.notification.impl;

import com.abrahamandrusso.api.v4.model.pojo.Notification;
import com.abrahamandrusso.api.v4.service.error.EmailNotSentException;
import com.abrahamandrusso.api.v4.service.notification.INotificationService;
import com.abrahamandrusso.api.v4.utils.email.IEmailService;
import com.abrahamandrusso.api.v4.utils.jwt.IJWTBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import static com.abrahamandrusso.api.v4.model.pojo.Notification.NotificationBuilder.aNotification;
import static com.abrahamandrusso.api.v4.utils.email.EmailFormatter.makeConfirmationEmail;
import static com.abrahamandrusso.api.v4.utils.email.EmailFormatter.makeSimpleEmail;

/**
 * @author Benahmed Sofiane on 01/04/18 20:41
 * <p>
 * Class Service
 * Handles the notifications and interactions with the user
 */

@Service
public class NotificationServiceImpl implements INotificationService {

    // ~ Declarations
    //==============================================================================================

    private final IEmailService emailService;

    private final IJWTBuilder builder;


    //~ Application Variables
    //==============================================================================================

    @Value("${api.owner.email}")
    private String ownerEmail;

    @Value("${api.domain}")
    private String domain;

    @Value("${api.reservation.confirm}")
    private String confirmationPath;


    // ~ Constructors
    //==============================================================================================

    /**
     * @param emailService : Service responsible for the sending of emails
     * @param builder      : JWT builder
     */
    @Autowired
    public NotificationServiceImpl(
            IEmailService emailService,
            IJWTBuilder builder
    ) {
        this.emailService = emailService;
        this.builder = builder;
    }


    // ~ Methods
    //==============================================================================================

    /**
     * Method that sends a confirmation notification to a user
     *
     * @param notification : Notification object containing an id
     * @throws EmailNotSentException if the email was not sent correctly
     */

    @Override
    public void sendConfirmationNotification(Notification notification)
            throws EmailNotSentException {

        // generating the token
        String link = generateConfirmationToken(
                notification.getRoute(),
                notification.getObjectId()
        );

        // building the email
        String email = makeConfirmationEmail(
                notification.getUserName(),
                link,
                notification.getOperation()
        );

        // sending the email
        send(notification, email);
    }

    /**
     * Method that sends a simple notification to a user
     *
     * @param notification : Notification object containing an id
     * @throws EmailNotSentException if the email was not sent correctly
     */
    public void sendSimpleNotification(Notification notification)
            throws EmailNotSentException {

        // building the email
        String email = makeSimpleEmail(
                notification.getUserName(),
                notification.getOperation()
        );

        // sending the email
        send(notification, email);

        // sending the notification to the owner
        Notification owner = aNotification()
                .withUserEmail(ownerEmail)
                .withOperation("User Action")
                .build();

        send(owner, email);
    }


    // ~ Private Methods
    //==============================================================================================

    /**
     * Method that generates a confirmation link using a JWT
     *
     * @param route : relative route of the resource to be confirmed
     * @param id    : id of the entity to be confirmed
     * @return a jwt token
     */
    private String generateConfirmationToken(String route, String id) {

        // building the token
        String token = builder.generateToken(id);

        // building the link
        return String.format("%s/%s/%s?token=%s",
                domain,
                route,
                confirmationPath,
                token
        );
    }

    /**
     * Method that send an email to the credentials of a notification object
     *
     * @param notification : Notification object containing an id
     * @param email        : email to be sent
     * @throws EmailNotSentException if the email was not sent correctly
     */
    private void send(Notification notification, String email) throws EmailNotSentException {

        // sending the email
        boolean emailSent = emailService.sendEmail(
                email,
                notification.getUserEmail(),
                notification.getSubject()
        );

        // checking if it was sent
        if (!emailSent)
            throw new EmailNotSentException();

    }

}
