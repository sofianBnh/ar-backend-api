package com.abrahamandrusso.api.v4.service.reservation;

import com.abrahamandrusso.api.v4.model.dto.ReservationDTO;
import com.abrahamandrusso.api.v4.model.resource.Reservation;
import com.abrahamandrusso.api.v4.service.error.ServiceException;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Benahmed Sofiane on 30/03/18 22:20
 * <p>
 * Interface Service
 * Handles the creation of a reservation
 */

public interface IReservationClientService {

    /**
     * Method that creates a {@link Reservation} from a dto
     *
     * @param dto :  reservation dto
     * @return a reservation object
     * @throws ServiceException if the dto in invalid
     */
    @Transactional
    Reservation make(ReservationDTO dto) throws ServiceException;

}
