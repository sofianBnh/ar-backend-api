package com.abrahamandrusso.api.v4.service.order.impl;

import com.abrahamandrusso.api.v4.model.dto.OrderDTO;
import com.abrahamandrusso.api.v4.model.resource.Reservation;
import com.abrahamandrusso.api.v4.model.resource.meal.Meal;
import com.abrahamandrusso.api.v4.model.user.User;
import com.abrahamandrusso.api.v4.repository.resource.MealRepository;
import com.abrahamandrusso.api.v4.repository.resource.ReservationRepository;
import com.abrahamandrusso.api.v4.service.order.IOrderService;
import com.abrahamandrusso.api.v4.service.order.error.OrderException;
import com.abrahamandrusso.api.v4.utils.jwt.IJWTHolder;
import com.abrahamandrusso.api.v4.utils.time.ITimeHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;


/**
 * @author Benahmed Sofiane on 31/04/18 13:40
 * <p>
 * Class Service
 * Handles the orders
 */

@Service
public class OrderServiceImpl implements IOrderService {


    // ~ Declarations
    //==============================================================================================

    private final ReservationRepository reservationRepository;

    private final MealRepository mealRepository;

    private final ITimeHandler timeHandler;

    private final IJWTHolder holder;


    // ~ Constructors
    //==============================================================================================

    /**
     * @param reservationRepository repository for reservations
     * @param mealRepository        repository for meals
     * @param timeHandler           service that handles time
     * @param holder                jwt holder
     */
    @Autowired
    public OrderServiceImpl(
            ReservationRepository reservationRepository,
            MealRepository mealRepository,
            ITimeHandler timeHandler,
            IJWTHolder holder
    ) {
        this.reservationRepository = reservationRepository;
        this.mealRepository = mealRepository;
        this.timeHandler = timeHandler;
        this.holder = holder;
    }


    // ~ Methods
    //==============================================================================================

    /**
     * Method that adds an order to a reservation
     *
     * @param dto   the order
     * @param token the token of the user who requested it
     * @return the reservation adter alteration
     * @throws OrderException if there is a problem while adding the order
     */
    @Override
    public Reservation addOrder(OrderDTO dto, String token) throws OrderException {

        // validate the order
        validateOrder(dto);

        // getting the reservation using the dto
        Reservation reservation = getReservation(dto);

        // checking the ownership of the reservation using the token
        checkReservationOwner(reservation, token);

        // getting the meals from the ids
        final List<Meal> mealsFromIds = getMealsFromId(dto.getMeals());

        // setting the order ( list of meals )
        reservation.setMeals(mealsFromIds);

        // saving the reservation
        return reservationRepository.save(reservation);
    }

    // ~ Private Methods
    //==============================================================================================

    /**
     * Method that validates an order
     *
     * @param order DTO of the order
     * @throws OrderException if the order is not valid
     */
    private void validateOrder(OrderDTO order) throws OrderException {

        // checking if the order contains meals
        if (order.getMeals() == null)
            throw new OrderException("No meals selected");

        // checking if the order has a reservation
        if (order.getReservationId() == null)
            throw new OrderException("Reservation not selected");

        // checking if the reservation exists
        if (!reservationRepository.existsById(order.getReservationId()))
            throw new OrderException("Reservation not found");

    }

    /**
     * Method that get the reservation of an order
     *
     * @param dto DTO of the order
     * @return the reservation
     * @throws OrderException if the reservation does not exist
     */
    private Reservation getReservation(OrderDTO dto) throws OrderException {

        // getting the reservation from the database
        Optional<Reservation> reservation = reservationRepository
                .findById(dto.getReservationId());

        // checking if it exists
        if (!reservation.isPresent())
            throw new OrderException("Reservation not found");

        // checking if there is already an order
        if (reservation.get().getMeals() != null
                && reservation.get().getMeals().size() > 0)
            throw new OrderException("Reservation already has an order");

        // checking the date of the reservation
        if (reservation.get().getDate().before(timeHandler.getCurrentDate()))
            throw new OrderException("Reservation Passed");

        return reservation.get();
    }

    /**
     * Method that checks the ownership of a reservation
     *
     * @param reservation a reservation
     * @param token       token of the supposed owner
     * @throws OrderException if the user is not the owner
     */
    private void checkReservationOwner(Reservation reservation, String token)
            throws OrderException {

        // updating the holder
        holder.setToken(token);

        // checking if the token's owner is the reservation's owner
        if (!holder.getSubject().equals(
                ((User) reservation.getUser()).getUsername()
        ))
            throw new OrderException("You are not the owner of the reservation");

    }

    /**
     * Method that maps ids to meals
     *
     * @param ids the ids of meals
     * @return a list of meals
     * @throws OrderException if one or more ids do not match any meal
     */
    private List<Meal> getMealsFromId(List<Long> ids) throws OrderException {

        // initializing
        List<Meal> meals = new ArrayList<>();
        AtomicBoolean error = new AtomicBoolean(false);

        // getting the meals based on the id
        ids.forEach(
                id -> {
                    Optional<Meal> meal = mealRepository.findById(id);

                    if (!meal.isPresent()) {
                        error.set(true);
                        return;
                    }

                    meals.add(meal.get());
                }
        );

        // checking if there has been an error
        if (error.get())
            throw new OrderException("Meal(s) not found");

        // returning the meals
        return meals;
    }

}
