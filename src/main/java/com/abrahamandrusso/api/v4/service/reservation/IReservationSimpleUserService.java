package com.abrahamandrusso.api.v4.service.reservation;

/**
 * @author Benahmed Sofiane on 30/03/18 20:27
 * <p>
 * Interface Service
 * Handles the creation of simple users' reservations
 */

public interface IReservationSimpleUserService extends IReservationClientService {
}
