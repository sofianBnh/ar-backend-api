package com.abrahamandrusso.api.v4.service.registration;

import com.abrahamandrusso.api.v4.model.dto.UserDTO;
import com.abrahamandrusso.api.v4.model.pojo.Notification;
import com.abrahamandrusso.api.v4.service.error.ServiceException;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Benhamed Sofiane on 02/03/18 17:02
 * <p>
 * Interface Service
 * Handles the registration of
 * {@link com.abrahamandrusso.api.v4.model.user.User}
 */

public interface IUserRegistrationService {

    /**
     * Method that registers a user
     *
     * @param user: dto of a user
     * @return a notification of verification
     * @throws ServiceException in case of a problem while saving the user
     */
    @Transactional
    Notification signUp(UserDTO user) throws ServiceException;

    /**
     * Method that confirms a user
     *
     * @param token : jwt sent to the user
     * @return a simple notification
     * @throws ServiceException in case there was a problem while
     *                          processing the token
     */
    @Transactional
    Notification confirm(String token) throws ServiceException;

    /**
     * Method that removes a user i.e. deletes his registration
     *
     * @param id : user id
     * @return a simple notification
     * @throws ServiceException in case there was a problem while deleting
     *                          the user
     */
    @Transactional
    Notification unSignUp(Long id) throws ServiceException;
}
