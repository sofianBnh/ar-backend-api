package com.abrahamandrusso.api.v4.service.planning;

import com.abrahamandrusso.api.v4.model.dto.DayDTO;
import com.abrahamandrusso.api.v4.model.dto.ReservationDTO;
import com.abrahamandrusso.api.v4.model.resource.planning.DefaultPlanning;
import com.abrahamandrusso.api.v4.service.planning.error.PlanningException;

import javax.transaction.Transactional;

/**
 * @author Benahmed Sofiane on 31/04/18 13:40
 * <p>
 * Interface Service
 * Handles the plannings and days off
 */
public interface IPlanningService {

    /**
     * Method that checks times with the current planning
     *
     * @param dto DTO for a reservation
     * @throws PlanningException if no planning was found
     */
    @Transactional
    void checkTimesWithCurrentPlanning(ReservationDTO dto)
            throws PlanningException;

    /**
     * Method that finds a planning for a date
     *
     * @param date desired date
     * @return the planning
     * @throws PlanningException if no planning was found
     */
    @Transactional
    DefaultPlanning findPlanningForDate(DayDTO date) throws PlanningException;
}
