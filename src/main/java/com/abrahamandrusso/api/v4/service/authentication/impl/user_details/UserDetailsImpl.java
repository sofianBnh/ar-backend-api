package com.abrahamandrusso.api.v4.service.authentication.impl.user_details;

import com.abrahamandrusso.api.v4.model.user.IUser;
import com.abrahamandrusso.api.v4.model.user.role.Role;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * @author Benhamed Sofiane on 18/03/18 20:44
 * Class Entity
 * Serves as the medium between Spring Security and the User entities
 */


@Component
public class UserDetailsImpl implements UserDetails {

    // ~ Declarations
    //==========================================================================================


    private static final long serialVersionUID = 9061621636995959126L;

    private Collection<? extends GrantedAuthority> authorities;

    private IUser user;


    // ~ Constructors
    //==========================================================================================

    public UserDetailsImpl() {
    }

    public UserDetailsImpl(IUser user) {
        this.user = user;
        setAuthorities();
    }

    public IUser getUser() {
        return user;
    }

    public void setUser(IUser user) {
        this.user = user;
        setAuthorities();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    public void setAuthorities() {
        List<Role> roles = user.getRoles();

        List<GrantedAuthority> authorities = new ArrayList<>();

        // extraction and conversion of roles entities into SimpleGrantedAuthority
        roles.forEach(role ->
                authorities.add(new SimpleGrantedAuthority(role.getName().name()))
        );

        this.authorities = authorities;
    }

    public Long getId() {
        return user.getId();
    }

    @Override
    public String getPassword() {
        return user.getPassword();
    }

    @Override
    public String getUsername() {
        return user.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return user.isEnabled();
    }

    public Date getLastPasswordUpdate() {
        return user.getLastPasswordUpdate().getTime();
    }

}
