package com.abrahamandrusso.api.v4.service.error;

/**
 * @author Benahmed Sofiane on 30/03/18 22:09
 * <p>
 * Class Exception
 * Servs a super class for all exceptions thrown in a service class
 */

public abstract class ServiceException extends Exception {

    // ~ Constructors
    //==========================================================================================

    public ServiceException(String message) {
        super(message);
    }
}
