package com.abrahamandrusso.api.v4.service.discount.impl;

import com.abrahamandrusso.api.v4.model.resource.Reservation;
import com.abrahamandrusso.api.v4.model.resource.discount.*;
import com.abrahamandrusso.api.v4.model.resource.meal.Meal;
import com.abrahamandrusso.api.v4.model.user.SimpleUser;
import com.abrahamandrusso.api.v4.repository.resource.ReservationRepository;
import com.abrahamandrusso.api.v4.repository.resource.discount.DefaultDiscountRepository;
import com.abrahamandrusso.api.v4.repository.resource.discount.DynamicDiscountRepository;
import com.abrahamandrusso.api.v4.repository.resource.discount.FixedDiscountRepository;
import com.abrahamandrusso.api.v4.service.discount.IDiscountService;
import com.abrahamandrusso.api.v4.service.discount.error.DiscountException;
import com.abrahamandrusso.api.v4.utils.time.ITimeHandler;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * @author Benahmed Sofiane on 31/04/18 13:40
 * <p>
 * Class Service
 * Handles the discounts of the orders
 */

@Service
public class DiscountServiceImpl implements IDiscountService {

    // ~ Declarations
    //==============================================================================================

    private final DynamicDiscountRepository dynamicDiscountRepository;

    private final DefaultDiscountRepository defaultDiscountRepository;

    private final FixedDiscountRepository fixedDiscountRepository;

    private final ReservationRepository reservationRepository;

    private final ITimeHandler timeHandler;


    // ~ Application Variables
    //==============================================================================================

    @Value("${api.discount.res-per-month}")
    private int numberOfReservationsForDiscount;


    // ~ Constructors
    //==============================================================================================

    /**
     * @param dynamicDiscountRepository repository for Dynamic Discounts
     * @param defaultDiscountRepository repository for Default Discounts
     * @param fixedDiscountRepository   repository for Fixed Discounts
     * @param reservationRepository     repository for Reservations
     * @param timeHandler               service that handles time
     */
    public DiscountServiceImpl(
            DynamicDiscountRepository dynamicDiscountRepository,
            DefaultDiscountRepository defaultDiscountRepository,
            FixedDiscountRepository fixedDiscountRepository,
            ReservationRepository reservationRepository,
            ITimeHandler timeHandler
    ) {
        this.dynamicDiscountRepository = dynamicDiscountRepository;
        this.defaultDiscountRepository = defaultDiscountRepository;
        this.fixedDiscountRepository = fixedDiscountRepository;
        this.reservationRepository = reservationRepository;
        this.timeHandler = timeHandler;
    }


    // ~ Methods
    //==============================================================================================

    /**
     * Method that calculates the initial price of
     * a reservation based on the price of the meals
     *
     * @param reservation reservation with an order
     * @return the total price of the reservation
     * @throws DiscountException if there are no meals
     */
    @Override
    public Double calculateInitialPrice(Reservation reservation) throws DiscountException {

        // validating the reservation
        validateReservationWithMeals(reservation);

        // calculating the total price
        final double initialPrice = reservation
                .getMeals()
                .parallelStream()
                .mapToDouble(Meal::getPrice)
                .sum();

        // updating the reservation
        reservation.setPrice(initialPrice);
        reservationRepository.save(reservation);

        return initialPrice;
    }

    /**
     * Method that applies the automatic discount on a
     * reservation if it is applicable
     *
     * @param reservation reservation with initial price
     *                    calculated
     * @return the discounted price
     * @throws DiscountException if an error occurred while
     *                           calculating the new price
     */
    @Override
    public Double calculateDiscountedPrice(Reservation reservation)
            throws DiscountException {

        // trying to extract the initial price
        final Double initialPrice = extractInitialPrice(reservation);

        // getting the available discounts
        List<Discount> discounts = getDiscounts(reservation.getDate());

        // initializing
        final double[] discountedPrice = {initialPrice};

        // discounting with any applicable discount
        discounts.forEach(discount -> {
            if (discount.isApplicable(discountedPrice[0]))
                discountedPrice[0] = discount.calculateNewPrice(discountedPrice[0]);
        });

        // extracting the final price
        final double finalDiscountedPrice = discountedPrice[0];

        // saving the discounted price
        reservation.setPrice(finalDiscountedPrice);
        reservationRepository.save(reservation);

        return finalDiscountedPrice;

    }

    /**
     * Method that calculates the discounted price of a
     * reservation
     *
     * @param reservation reservation with initial price
     * @return the discounted price
     * @throws DiscountException if an error occurred while
     *                           calculating the new price
     */
    @Override
    public Double calculateWithAutomaticDiscount(Reservation reservation)
            throws DiscountException {

        // trying to extract the initial price
        final Double initialPrice = extractInitialPrice(reservation);

        // getting tje reservation count
        final int reservationCount = getReservationCount(reservation);

        // checking if it is applicable
        if (reservationCount < numberOfReservationsForDiscount)
            return initialPrice;

        // applying the discount
        Reducible defaultDiscount = getCurrentAutomaticDiscount();
        final double discountedPrice = defaultDiscount.calculateNewPrice(initialPrice);

        // saving the new price
        reservation.setPrice(discountedPrice);
        reservationRepository.save(reservation);

        return discountedPrice;

    }

    /**
     * Method that get data about the automatic discount
     *
     * @return the default discount
     * @throws DiscountException if there is no default
     *                           discount
     */
    @Override
    public Reducible getCurrentAutomaticDiscount() throws DiscountException {

        // querying the default discounts
        List<DefaultDiscount> defaultDiscounts = new ArrayList<>();
        defaultDiscountRepository.findAll().forEach(defaultDiscounts::add);

        // checking the presence
        if (defaultDiscounts.isEmpty())
            throw new DiscountException("Discount not found");

        return defaultDiscounts.get(0);
    }


    // ~ Private Methods
    //==============================================================================================

    /**
     * Method that validates the reservation for a price
     * calculation
     *
     * @param reservation a reservation with meals
     * @throws DiscountException if the reservation is not valid
     */
    private void validateReservationWithMeals(Reservation reservation) throws DiscountException {

        // checking if the reservation is not null
        if (reservation == null)
            throw new DiscountException("Reservation not found");

        // checking if it dose not already has meals
        if (reservation.getMeals() == null)
            throw new DiscountException("Reservation without order");

        // getting the current date
        Calendar today = timeHandler.getCurrentDate();

        // checking if it is outdated
        if (reservation.getDate().before(today))
            throw new DiscountException("Reservation prior to today");

    }

    /**
     * Method that extracts the initial price from the reservation
     *
     * @param reservation a reservtation with the initial price calculated
     * @return the initial price
     * @throws DiscountException if it has not been calculated
     */
    private Double extractInitialPrice(Reservation reservation) throws DiscountException {

        // getting the initial price
        final Double initialPrice = reservation.getPrice();

        // checking if it has been calculated
        if (initialPrice == null || initialPrice == 0)
            throw new DiscountException("Problem while saving discount");

        return initialPrice;
    }

    /**
     * Method that gets the reservation count in the last month
     *
     * @param reservation a reservation
     * @return the number of reservations made in the last month
     */
    private int getReservationCount(Reservation reservation) {

        // getting the date from which the discount starts
        Calendar aMonthAgo = timeHandler.getCurrentDate();
        aMonthAgo.set(Calendar.MONTH, aMonthAgo.get(Calendar.MONTH) - 1);

        // getting the owner
        SimpleUser owner = reservation.getUser();

        // counting the reservations made in that period
        return reservationRepository
                .countByConfirmedAndUserAndDateGreaterThan(
                        true, owner, aMonthAgo);

    }

    /**
     * Method that gets all the extra discounts
     *
     * @param date the date of the discounts
     * @return a list of all the discounts applicable at that time
     */
    private List<Discount> getDiscounts(Calendar date) {

        // getting all the fixed discounts
        List<FixedDiscount> fixedDiscounts = fixedDiscountRepository
                .findForDate(date);

        // getting all the dynamic discounts
        List<DynamicDiscount> dynamicDiscounts = dynamicDiscountRepository
                .findForDate(date);

        // collecting all discounts
        List<Discount> discounts = new ArrayList<>();
        discounts.addAll(fixedDiscounts);
        discounts.addAll(dynamicDiscounts);

        return discounts;
    }

}
