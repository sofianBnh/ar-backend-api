package com.abrahamandrusso.api.v4.service.restaurant.impl;

import com.abrahamandrusso.api.v4.model.dto.RestaurantDTO;
import com.abrahamandrusso.api.v4.model.resource.Restaurant;
import com.abrahamandrusso.api.v4.repository.resource.RestaurantRepository;
import com.abrahamandrusso.api.v4.service.restaurant.IRestaurantDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Benahmed Sofiane on 03/04/18 17:46
 * <p>
 * Class Service
 * Handles the preparation of the data sent to the front end
 */

@Component
public class RestaurantDataServiceImpl implements IRestaurantDataService {


    // ~ Declarations
    //==========================================================================================

    private final RestaurantRepository repository;


    // ~ Constructors
    //==========================================================================================

    /**
     * @param repository : repository for the {@link Restaurant} entities
     */
    @Autowired
    public RestaurantDataServiceImpl(RestaurantRepository repository) {
        this.repository = repository;
    }


    // ~ Methods
    //==========================================================================================

    /**
     * Method that returns all restaurants' data
     *
     * @return list of restaurant dtos
     */
    @Override
    public List<RestaurantDTO> findAll() {

        // query all restaurants
        Iterable<Restaurant> all = repository.findAll();

        // initialize list
        List<RestaurantDTO> dtos = new ArrayList<>();

        // fill the list
        all.forEach(restaurant -> dtos.add(buildFromEntity(restaurant)));

        return dtos;

    }


    // ~ Private Methods
    //==========================================================================================

    /**
     * Method that builds {@link RestaurantDTO} from {@link Restaurant}
     *
     * @param restaurant restaurant object
     * @return restaurant dto object
     */
    private RestaurantDTO buildFromEntity(Restaurant restaurant) {

        return new RestaurantDTO(
                restaurant.getId(),
                restaurant.getLatitude(),
                restaurant.getLongitude(),
                restaurant.getAddress()
        );
    }

}
