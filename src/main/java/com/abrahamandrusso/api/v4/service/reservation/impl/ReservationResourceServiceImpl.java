package com.abrahamandrusso.api.v4.service.reservation.impl;

import com.abrahamandrusso.api.v4.model.dto.ReservationDTO;
import com.abrahamandrusso.api.v4.model.pojo.Notification;
import com.abrahamandrusso.api.v4.model.pojo.ServiceResponse;
import com.abrahamandrusso.api.v4.model.resource.Reservation;
import com.abrahamandrusso.api.v4.model.user.SimpleUser;
import com.abrahamandrusso.api.v4.model.user.User;
import com.abrahamandrusso.api.v4.repository.resource.ReservationRepository;
import com.abrahamandrusso.api.v4.repository.user.UserRepository;
import com.abrahamandrusso.api.v4.service.error.ConfirmationException;
import com.abrahamandrusso.api.v4.service.error.InvalidRequestException;
import com.abrahamandrusso.api.v4.service.error.ServiceException;
import com.abrahamandrusso.api.v4.service.notification.INotificationService;
import com.abrahamandrusso.api.v4.service.reservation.IReservationClientService;
import com.abrahamandrusso.api.v4.service.reservation.IReservationResourceService;
import com.abrahamandrusso.api.v4.utils.jwt.IJWTHolder;
import com.abrahamandrusso.api.v4.utils.time.ITimeHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static com.abrahamandrusso.api.v4.model.pojo.Notification.NotificationBuilder.aNotification;


/**
 * @author Benahmed Sofiane on 30/03/18 20:31
 * <p>
 * Class Service
 * Handles CRUD operations concerning Reservations
 */

@Service
public class ReservationResourceServiceImpl implements IReservationResourceService {


    // ~ Declarations
    //==============================================================================================

    private final ReservationRepository reservationRepository;

    private final INotificationService notificationService;

    private final UserRepository userRepository;

    private final ITimeHandler timeHandler;

    private final IJWTHolder holder;


    //~ Application Variables
    //==============================================================================================

    @Value("${api.reservation.max-hours}")
    private int numberOfMaxHoursForReservation;

    @Value("${api.reservation.deadline.day-count}")
    private int numberOfDaysBeforeDeadline;


    // ~ Constructors
    //==============================================================================================

    /**
     * @param reservationRepository : repository for the {@link Reservation} entities
     * @param notificationService   : service that handles notifications
     * @param userRepository        : repository for the {@link User} entities
     * @param timeHandler           : time operations class
     * @param holder                : jwt holder
     */
    @Autowired
    public ReservationResourceServiceImpl(
            ReservationRepository reservationRepository,
            INotificationService notificationService,
            UserRepository userRepository,
            ITimeHandler timeHandler,
            IJWTHolder holder
    ) {
        this.reservationRepository = reservationRepository;
        this.notificationService = notificationService;
        this.userRepository = userRepository;
        this.timeHandler = timeHandler;
        this.holder = holder;
    }


    // ~ Methods
    //==============================================================================================

    /**
     * Method for the saving of {@link Reservation} entites
     *
     * @param clientService service that handles the buildReservation of the reservation
     * @param dto           DTO for the reservation
     * @return the final reservation
     * @throws ServiceException if a problem occurred while saving the new reservation
     */
    @Override
    public Reservation save(IReservationClientService clientService,
                            ReservationDTO dto) throws ServiceException {

        Reservation reservation = clientService.make(dto);

        reservation.setConfirmed(true); // todo remove on production

        // save the reservation
        reservation = reservationRepository.save(reservation);

        SimpleUser user = reservation.getUser();

        // building the notification
        Notification confirm = aNotification()
                .withUserName(user.getName())
                .withUserEmail(user.getEmail())
                .withObjectId(reservation.getId().toString())
                .withSubject("Confirm Reservation")
                .withRoute("reservation-service")
                .withOperation("Reservation Confirmation")
                .build();

        // sending the confirmation notification
//        notificationService.sendConfirmationNotification(confirm);
        return reservation;
    }

    /**
     * Method for the update of {@link Reservation} entities
     *
     * @param reservation : reservation object
     * @return a service response
     * @throws ServiceException if a problem occurred while updating a reservation
     */
    @Override
    public Reservation update(Reservation reservation)
            throws ServiceException {

        // check if the reservation exists
        if (!reservationRepository.existsById(reservation.getId()))
            throw new InvalidRequestException("Reservation Not Found");

        // check the time between the reservation and the current day
        if (timeHandler.daysBetween(reservation.getDate(),
                timeHandler.getCurrentDate()) < numberOfDaysBeforeDeadline)
            throw new InvalidRequestException("Out of time");

        // update reservation
        return reservationRepository.save(reservation);
    }

    /**
     * Method for the deletion of {@link Reservation} entites
     *
     * @param id : id of the reservation
     * @return a service response
     * @throws ServiceException if a problem occurred while deleting a reservation
     */
    @Override
    public ServiceResponse delete(String token, Long id)
            throws ServiceException {

        // get the reservation
        Optional<Reservation> reservation = reservationRepository.findById(id);

        // check its presence
        if (!reservation.isPresent())
            throw new InvalidRequestException("A Problem Occurred");

        // checking the ownership
        final String ownerUsername = getUser(token).getUsername();

        final String reservationOwner = ((User) reservation.get().getUser()).getUsername();

        if (!ownerUsername.equals(reservationOwner))
            throw new InvalidRequestException("Reservation Not found");

        // check the time between the reservation and the current day
        if (timeHandler.daysBetween(reservation.get().getDate(), timeHandler.getCurrentDate())
                < numberOfDaysBeforeDeadline)
            throw new InvalidRequestException("Out of time");


        // delete reservation
        reservationRepository.deleteById(id);
        return new ServiceResponse(true, "Deleted");

    }

    /**
     * Method for finding a user's reservations
     *
     * @param token : user's token
     * @return list of reservations
     * @throws InvalidRequestException if the user is not valid
     */
    @Override
    public List<Reservation> findReservationsForToken(String token)
            throws InvalidRequestException {

        User user = getUser(token);

        // get the reservations by user id
        return reservationRepository
                .findByUserId(user.getId());
    }


    /**
     * Method for the confirmation of a {@link Reservation}
     *
     * @param token : token sent to the user
     * @return a simple notification
     * @throws ConfirmationException if the confirmation is not made
     */
    @Override
    public Notification confirm(String token) throws ConfirmationException {

        // update the holder
        holder.setToken(token);

        // get the subject
        String subject = holder.getSubject();

        // check the subject presence
        if (subject == null)
            throw new ConfirmationException("Bad Token");

        // get the id
        Long id = Long.parseLong(subject);

        // query the reservation
        Optional<Reservation> reservation = reservationRepository.findById(id);

        // check its presence
        if (!reservation.isPresent())
            throw new ConfirmationException("Reservation Not Found");

        // check the reservation date
        if (reservation.get().getDate().before(timeHandler.getCurrentDate()))
            throw new ConfirmationException("Confirmation Timeout");

        // confirm the reservation
        reservation.get().setConfirmed(true);

        // get the owner of the reservation
        SimpleUser user = reservation.get().getUser();

        // return the notification object
        return aNotification()
                .withUserName(user.getName())
                .withUserEmail(user.getEmail())
                .withOperation("Reservation Verification")
                .withSubject("Verify Reservation")
                .build();
    }


    // ~ Private Methods
    //==============================================================================================

    /**
     * Method that gets the user from the token
     *
     * @param token token of the user
     * @return the User owner
     * @throws InvalidRequestException if the user does not exist or
     *                                 the token is bad
     */
    private User getUser(String token) throws InvalidRequestException {

        // updating the holder
        holder.setToken(token);

        // returning the subject
        String username = holder.getSubject();

        // if the token if defective
        if (username == null)
            throw new InvalidRequestException("Invalid Token");

        // get the user
        User user = userRepository.findByUsername(username);

        if (user == null)
            throw new InvalidRequestException("User not found");

        return user;
    }

}

