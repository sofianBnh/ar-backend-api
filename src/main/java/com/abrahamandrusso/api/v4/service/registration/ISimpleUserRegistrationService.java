package com.abrahamandrusso.api.v4.service.registration;

import com.abrahamandrusso.api.v4.model.dto.SimpleUserDTO;
import com.abrahamandrusso.api.v4.model.pojo.ServiceResponse;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Benahmed Sofiane on 24/03/18 21:59
 * <p>
 * Interface Service
 * Handles the registration of
 * {@link com.abrahamandrusso.api.v4.model.user.SimpleUser}
 */

public interface ISimpleUserRegistrationService {

    /**
     * Method that creates a {@link com.abrahamandrusso.api.v4.model.user.SimpleUser}
     * from a {@link SimpleUserDTO} and saves it
     *
     * @param user : simple user data transfer object
     * @return the id of the created user
     */
    @Transactional
    ServiceResponse create(SimpleUserDTO user);

}
