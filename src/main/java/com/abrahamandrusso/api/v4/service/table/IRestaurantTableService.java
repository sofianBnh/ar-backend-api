package com.abrahamandrusso.api.v4.service.table;

import com.abrahamandrusso.api.v4.model.dto.ReservationDTO;
import com.abrahamandrusso.api.v4.model.resource.Reservation;
import com.abrahamandrusso.api.v4.model.resource.Restaurant;
import com.abrahamandrusso.api.v4.model.resource.RestaurantTable;
import com.abrahamandrusso.api.v4.service.error.NotEnoughPlacesException;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Benahmed Sofiane on 31/04/18 13:40
 * <p>
 * Interface Service
 * Handles the tables of the restaurants
 */
public interface IRestaurantTableService {


    /**
     * Method that returns the tables of a restaurant
     * for a reservation according to some parameters
     *
     * @param dto        reservation dto for times
     * @param restaurant restaurant of the reservation
     * @param sameTime   reservations occurring at the same time
     * @return a list of available tables at the time of
     * the reservation
     * @throws NotEnoughPlacesException if there are not any available
     *                                  places
     */
    @Transactional
    List<RestaurantTable> getTablesForReservation(ReservationDTO dto, Restaurant restaurant, List<Reservation> sameTime) throws NotEnoughPlacesException;

}
