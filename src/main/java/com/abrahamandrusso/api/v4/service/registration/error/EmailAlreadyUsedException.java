package com.abrahamandrusso.api.v4.service.registration.error;

import com.abrahamandrusso.api.v4.service.error.InvalidRequestException;

/**
 * @author Benahmed Sofiane on 28/03/18 17:49
 * <p>
 * Class Exception
 * Thrown when an email already registred is sent in a new request
 */

public class EmailAlreadyUsedException extends InvalidRequestException {

    // ~ Constructors
    //==========================================================================================

    public EmailAlreadyUsedException() {
        super("Email Already Used");
    }
}
