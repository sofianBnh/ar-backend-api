package com.abrahamandrusso.api.v4.service.authentication;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Benhamed Sofiane on 18/03/18 20:41
 * <p>
 * Interface Service
 * Handles the access authorisation based on tokens
 */

public interface IAuthenticationService {

    /**
     * Method that checks if a token is allowed to pass
     *
     * @param token value of JWT
     * @return the User details of the owner or null
     */
    @Transactional
    UserDetails checkIfAllowed(String token);

}
