package com.abrahamandrusso.api.v4.service.registration.impl;

import com.abrahamandrusso.api.v4.model.dto.SimpleUserDTO;
import com.abrahamandrusso.api.v4.model.pojo.ServiceResponse;
import com.abrahamandrusso.api.v4.model.user.SimpleUser;
import com.abrahamandrusso.api.v4.model.user.role.Role;
import com.abrahamandrusso.api.v4.model.user.role.Roles;
import com.abrahamandrusso.api.v4.repository.user.RoleRepository;
import com.abrahamandrusso.api.v4.repository.user.SimpleUserRepository;
import com.abrahamandrusso.api.v4.service.error.InvalidDataException;
import com.abrahamandrusso.api.v4.service.error.InvalidRequestException;
import com.abrahamandrusso.api.v4.service.registration.ISimpleUserRegistrationService;
import com.abrahamandrusso.api.v4.service.registration.error.EmailAlreadyUsedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;

/**
 * @author Benahmed Sofiane on 24/03/18 21:59
 * <p>
 * Class Service
 * Handles the registration of
 * {@link com.abrahamandrusso.api.v4.model.user.SimpleUser}
 */

@Service
public class SimpleUserRegistrationServiceImpl implements ISimpleUserRegistrationService {


    //~ Declarations
    //==============================================================================================


    private final SimpleUserRepository simpleUserRepository;

    private final RoleRepository roleRepository;


    // ~ Constructors
    //==============================================================================================


    /**
     * @param simpleUserRepository : repository for {@link SimpleUser} entities
     * @param roleRepository       : repository for {@link Role} entities
     */
    @Autowired
    public SimpleUserRegistrationServiceImpl(
            SimpleUserRepository simpleUserRepository,
            RoleRepository roleRepository
    ) {
        this.simpleUserRepository = simpleUserRepository;
        this.roleRepository = roleRepository;
    }


    // ~ Methods
    //==============================================================================================

    /**
     * Method that creates a {@link com.abrahamandrusso.api.v4.model.user.SimpleUser}
     * from a {@link SimpleUserDTO} and saves it
     *
     * @param user : simple user data transfer object
     * @return the id of the created user
     */
    @Override
    public ServiceResponse create(SimpleUserDTO user) {

        Long id;

        try {

            // validate the request
            validateSimpleUserRequest(user);

            // save the simple user
            id = saveSimpleUser(user);

        } catch (InvalidRequestException | InvalidDataException e) {
            return new ServiceResponse(false, e.getMessage());
        }

        // if no if was returned an error occurred
        if (id == null)
            return new ServiceResponse(false, "Something went wrong");

        return new ServiceResponse(true, id.toString());

    }


    // ~ Private Methods
    //==============================================================================================


    /**
     * @param user : data transfer object for {@link SimpleUser}
     * @throws InvalidRequestException if the dto is invalid
     */
    private void validateSimpleUserRequest(SimpleUserDTO user)
            throws InvalidRequestException {

        // check if the email has already been used
        if (simpleUserRepository.existsByEmail(user.getEmail()))
            throw new EmailAlreadyUsedException();

    }

    /**
     * @param user : data transfer object for {@link SimpleUser}
     * @return the id of the saved user
     * @throws InvalidDataException if there was a problem while saving
     */
    private Long saveSimpleUser(SimpleUserDTO user) throws InvalidDataException {
        // getting the user
        Role userRole = roleRepository.findByName(Roles.ROLE_SIMPLE_USER);

        // generating the entity
        SimpleUser generated = new SimpleUser(
                user.getName(),
                user.getEmail(),
                user.getPhone(),
                Collections.singletonList(userRole)
        );

        // saving the entity into the database
        simpleUserRepository.save(generated);

        // querying the new entity
        SimpleUser saved = simpleUserRepository.findByEmail(user.getEmail());

        // checking if it was saved
        if (saved == null)
            throw new InvalidDataException();

        // sending the id
        return saved.getId();

    }

}
