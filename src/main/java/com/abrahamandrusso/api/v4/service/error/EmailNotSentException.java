package com.abrahamandrusso.api.v4.service.error;

/**
 * @author Benhamed Sofiane on 23/03/18 15:43
 * <p>
 * Class Exception
 * Thrown when an email is not sent
 */

public class EmailNotSentException extends ServiceException {

    // ~ Constructors
    //==========================================================================================

    public EmailNotSentException() {
        super("Could Not Send Confirmation Email, Try Again Later");
    }
}
