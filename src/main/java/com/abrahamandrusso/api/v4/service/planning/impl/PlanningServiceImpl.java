package com.abrahamandrusso.api.v4.service.planning.impl;

import com.abrahamandrusso.api.v4.model.dto.DayDTO;
import com.abrahamandrusso.api.v4.model.dto.ReservationDTO;
import com.abrahamandrusso.api.v4.model.resource.planning.DefaultPlanning;
import com.abrahamandrusso.api.v4.model.resource.planning.SpecialDay;
import com.abrahamandrusso.api.v4.repository.resource.planning.DefaultPlanningRepository;
import com.abrahamandrusso.api.v4.repository.resource.planning.SpecialDayRepository;
import com.abrahamandrusso.api.v4.service.planning.IPlanningService;
import com.abrahamandrusso.api.v4.service.planning.error.PlanningException;
import com.abrahamandrusso.api.v4.utils.time.ITimeHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;

/**
 * @author Benahmed Sofiane on 26/05/18 13:40
 * <p>
 * Class Service
 * Handles the plannings
 */

@Service
public class PlanningServiceImpl implements IPlanningService {


    // ~ Declarations
    //==============================================================================================

    private final SpecialDayRepository specialDayRepository;

    private final DefaultPlanningRepository defaultPlanningRepository;

    private final ITimeHandler timeHandler;


    // ~ Constructors
    //==============================================================================================

    /**
     * @param specialDayRepository      repository for special days
     * @param defaultPlanningRepository repository for default plannings
     * @param timeHandler               handler for the time
     */
    @Autowired
    public PlanningServiceImpl(
            SpecialDayRepository specialDayRepository,
            DefaultPlanningRepository defaultPlanningRepository,
            ITimeHandler timeHandler
    ) {
        this.specialDayRepository = specialDayRepository;
        this.defaultPlanningRepository = defaultPlanningRepository;
        this.timeHandler = timeHandler;
    }


    // ~ Methods
    //==============================================================================================

    /**
     * Method that checks times with the current planning
     *
     * @param dto DTO for a reservation
     * @throws PlanningException if no planning was found
     */
    @Override
    public void checkTimesWithCurrentPlanning(ReservationDTO dto)
            throws PlanningException {

        // finding the planning of the date
        DefaultPlanning hours = findPlanningForDate(new DayDTO(dto.getDate()));

        // checking the closing time
        if (timeHandler.isBeforeIgnoreDate(hours.getClosingTime(), dto.getEndTime()))
            throw new PlanningException("Ends after closing time, Closing time is at "
                    + formatTime(hours.getClosingTime()));

        // checking the opening time
        if (timeHandler.isBeforeIgnoreDate(dto.getStartTime(), hours.getOpeningTime()))
            throw new PlanningException("Starts before opening time, Opening Time is at "
                    + formatTime(hours.getOpeningTime()));

    }


    /**
     * Method that finds a planning for a date
     *
     * @param dto desired date wrapped in a dto
     * @return the planning
     * @throws PlanningException if no planning was found
     */
    @Override
    public DefaultPlanning findPlanningForDate(DayDTO dto) throws PlanningException {

        // initializing the date
        Calendar date = timeHandler.getCurrentDate();

        // setting the times
        date.set(Calendar.DAY_OF_MONTH, dto.getDate().get(Calendar.DAY_OF_MONTH));
        date.set(Calendar.MONTH, dto.getDate().get(Calendar.MONTH));
        date.set(Calendar.YEAR, dto.getDate().get(Calendar.YEAR));

        // checking the existence of a special day
        if (specialDayRepository.existsByDate(date))
            return checkForSpecialDay(date);
        else
            return checkForDefaultPlanning();

    }


    // ~ Private Methods
    //==============================================================================================

    /**
     * Method that returns a default planning
     *
     * @return default planning
     * @throws PlanningException if the default planning is not found
     */
    private DefaultPlanning checkForDefaultPlanning() throws PlanningException {

        // initializing
        List<DefaultPlanning> plannings = new ArrayList<>();
        defaultPlanningRepository.findAll().forEach(plannings::add);

        // checking the existence
        if (plannings.isEmpty())
            throw new PlanningException("DefaultPlanning Not found");

        // returning the first
        return plannings.get(0);

    }

    /**
     * Method that returns the special day planning
     *
     * @param today date
     * @return a special planning
     * @throws PlanningException if the default planning is not found
     */
    private DefaultPlanning checkForSpecialDay(Calendar today) throws PlanningException {
        // initializing
        Optional<SpecialDay> planning = specialDayRepository.findByDate(today);

        // checking the existence
        if (!planning.isPresent())
            throw new PlanningException("DefaultPlanning Not found");

        // checking if it is a working day
        if (!planning.get().isWorkingDay())
            throw new PlanningException("Day off !");

        return planning.get();

    }

    /**
     * Method that formats a calendar into a time
     * @param calendar {@link Calendar} object
     * @return a formatted version
     */
    private String formatTime(Calendar calendar) {
        return " " +
                calendar.get(Calendar.HOUR_OF_DAY) +
                ":" +
                (calendar.get(Calendar.MINUTE) < 10 ? "0" : "") +
                calendar.get(Calendar.MINUTE)
                + " ";
    }

}
