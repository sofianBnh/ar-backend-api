package com.abrahamandrusso.api.v4.service.table.impl;

import com.abrahamandrusso.api.v4.model.dto.ReservationDTO;
import com.abrahamandrusso.api.v4.model.resource.Reservation;
import com.abrahamandrusso.api.v4.model.resource.Restaurant;
import com.abrahamandrusso.api.v4.model.resource.RestaurantTable;
import com.abrahamandrusso.api.v4.repository.resource.RestaurantTableRepository;
import com.abrahamandrusso.api.v4.service.error.NotEnoughPlacesException;
import com.abrahamandrusso.api.v4.service.table.IRestaurantTableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * @author Benahmed Sofiane on 26/05/18 13:40
 * <p>
 * Class Service
 * Handles the tables of the restaurant
 */

@Service
public class RestaurantTableServiceImpl implements IRestaurantTableService {

    // ~ Declarations
    //==============================================================================================

    private final RestaurantTableRepository restaurantTableRepository;


    // ~ Constructors
    //==============================================================================================

    /**
     * @param restaurantTableRepository repository for the tables
     */
    @Autowired
    public RestaurantTableServiceImpl(
            RestaurantTableRepository restaurantTableRepository
    ) {
        this.restaurantTableRepository = restaurantTableRepository;
    }


    // ~ Methods
    //==============================================================================================

    /**
     * Method that returns the tables of a restaurant
     * for a reservation according to some parameters
     *
     * @param dto        reservation dto for times
     * @param restaurant restaurant of the reservation
     * @param sameTime   reservations occurring at the
     *                   same time
     * @return a list of available tables at the time of
     * the reservation
     * @throws NotEnoughPlacesException if there are not
     *                                  any available places
     */
    @Override
    public List<RestaurantTable> getTablesForReservation(
            ReservationDTO dto,
            Restaurant restaurant,
            List<Reservation> sameTime
    ) throws NotEnoughPlacesException {

        // getting all the available tables tables at that time
        List<RestaurantTable> availableTables = getAvailableTables(
                restaurant, sameTime
        );

        // initializing
        AtomicInteger placeCount = new AtomicInteger(dto.getNumberOfPlaces());
        List<RestaurantTable> reservationTables = new ArrayList<>();

        // Adding tables by max capacity
        addTablesByMaxCapacity(reservationTables, availableTables, placeCount);

        if (placeCount.get() <= 0)
            return reservationTables;

        availableTables.removeAll(reservationTables);

        // Adding tables by min capacity
        addTablesByMinCapacity(reservationTables, availableTables, placeCount);
        if (placeCount.get() <= 0)
            return reservationTables;

        availableTables.removeAll(reservationTables);

        // Adding remaining tables
        addRemainingTables(reservationTables, availableTables, placeCount);

        if (placeCount.get() <= 0)
            return reservationTables;

        throw new NotEnoughPlacesException();

    }


    // ~ Private Methods
    //==============================================================================================

    /**
     * Method that gets the remaining available places
     * of a restaurant
     *
     * @param restaurant restaurant of the tables
     * @param sameTime   reservations occurring at the
     *                   same time
     * @return the list of remaining non taken tables
     */
    private List<RestaurantTable> getAvailableTables(
            Restaurant restaurant,
            List<Reservation> sameTime
    ) {

        // getting the tables of the reservations
        List<RestaurantTable> usedTables = new ArrayList<>();
        sameTime.forEach(reservation -> usedTables
                .addAll(reservation.getTables()));

        // extracting their ids
        final List<Long> ids = usedTables
                .parallelStream()
                .mapToLong(RestaurantTable::getId)
                .boxed()
                .collect(Collectors.toList());

        // checking if any tables are taken
        List<RestaurantTable> availableTables;
        if (ids != null && ids.size() > 0)
            availableTables = restaurantTableRepository
                    .findByRestaurantAndIdNotIn(restaurant, ids);
        else
            availableTables = restaurantTableRepository
                    .findByRestaurant(restaurant);

        // returning the available tables sorted by max capacity desc
        return availableTables
                .stream()
                .sorted(Comparator
                        .comparing(RestaurantTable::getMaxCapacity)
                        .reversed()
                ).collect(Collectors.toList());
    }


    /**
     * Method that adds a table to the reservation tables
     *
     * @param reservationTables list of tables for the reservation
     * @param placeCount        number of places not filled
     * @param table             table to be added
     */
    private void addTableToReservationTables(
            List<RestaurantTable> reservationTables,
            AtomicInteger placeCount,
            RestaurantTable table
    ) {
        placeCount.addAndGet(-table.getMaxCapacity());
        reservationTables.add(table);
    }


    /**
     * Method that adds tables based on their max capacity
     * by looking for the reservations with a close place
     * count as the max capacity
     *
     * @param reservationTables list of tables for the reservation
     * @param availableTables   list of all the available tables
     * @param placeCount        number of places not filled
     */
    private void addTablesByMaxCapacity(
            List<RestaurantTable> reservationTables,
            List<RestaurantTable> availableTables,
            AtomicInteger placeCount
    ) {
        availableTables.forEach(table -> {
            if (table.getMaxCapacity() <= placeCount.get())
                addTableToReservationTables(
                        reservationTables,
                        placeCount,
                        table
                );
        });
    }

    /**
     * Method that adds tables based on their min capacity
     * by looking for the reservations with a close place
     * count as the min capacity
     *
     * @param reservationTables list of tables for the reservation
     * @param availableTables   list of all the available tables
     * @param placeCount        number of places not filled
     */
    private void addTablesByMinCapacity(
            List<RestaurantTable> reservationTables,
            List<RestaurantTable> availableTables,
            AtomicInteger placeCount
    ) {
        availableTables.forEach(table -> {
            if (table.getMinCapacity() <= placeCount.get())
                addTableToReservationTables(
                        reservationTables,
                        placeCount,
                        table
                );
        });
    }

    /**
     * Method that adds tables that are available and remaining
     *
     * @param reservationTables list of tables for the reservation
     * @param availableTables   list of all the available tables
     * @param placeCount        number of places not filled
     */
    private void addRemainingTables(
            List<RestaurantTable> reservationTables,
            List<RestaurantTable> availableTables,
            AtomicInteger placeCount
    ) {

        availableTables.forEach(table -> {
            if (placeCount.get() > 0)
                addTableToReservationTables(
                        reservationTables,
                        placeCount,
                        table
                );
        });
    }


}
