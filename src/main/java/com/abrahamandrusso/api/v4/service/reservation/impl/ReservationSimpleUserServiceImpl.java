package com.abrahamandrusso.api.v4.service.reservation.impl;

import com.abrahamandrusso.api.v4.model.dto.ReservationDTO;
import com.abrahamandrusso.api.v4.model.resource.Reservation;
import com.abrahamandrusso.api.v4.model.resource.Restaurant;
import com.abrahamandrusso.api.v4.model.user.SimpleUser;
import com.abrahamandrusso.api.v4.repository.resource.ReservationRepository;
import com.abrahamandrusso.api.v4.repository.resource.RestaurantRepository;
import com.abrahamandrusso.api.v4.repository.user.SimpleUserRepository;
import com.abrahamandrusso.api.v4.service.error.InvalidDataException;
import com.abrahamandrusso.api.v4.service.error.InvalidRequestException;
import com.abrahamandrusso.api.v4.service.planning.IPlanningService;
import com.abrahamandrusso.api.v4.service.reservation.IReservationSimpleUserService;
import com.abrahamandrusso.api.v4.service.table.IRestaurantTableService;
import com.abrahamandrusso.api.v4.utils.time.ITimeHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * @author Benahmed Sofiane on 30/03/18 20:42
 * <p>
 * Class Service
 * Handles the buildReservation of {@link SimpleUser} reservations
 */

@Service
public class ReservationSimpleUserServiceImpl extends ReservationBaseService
        implements IReservationSimpleUserService {


    // ~ Declarations
    //==========================================================================================

    private final SimpleUserRepository simpleUserRepository;


    // ~ Constructors
    //==========================================================================================

    /**
     * @param reservationRepository : repository for {@link Reservation} entities
     * @param restaurantRepository  : repository for {@link Restaurant} entities
     * @param simpleUserRepository  : repository for {@link SimpleUser} entities
     * @param timeHandler           : time operations class
     */
    @Autowired
    protected ReservationSimpleUserServiceImpl(
            IRestaurantTableService restaurantTableService,
            ReservationRepository reservationRepository,
            RestaurantRepository restaurantRepository,
            SimpleUserRepository simpleUserRepository,
            IPlanningService planningService,
            ITimeHandler timeHandler
    ) {
        super(
                restaurantTableService,
                reservationRepository,
                restaurantRepository,
                planningService,
                timeHandler
        );

        this.simpleUserRepository = simpleUserRepository;
    }


    // ~ Methods
    //==========================================================================================

    /**
     * Method for validating a reservation dto
     *
     * @param dto : reservation dto
     * @throws InvalidRequestException if the request is invalid
     */
    @Override
    void validateRequest(ReservationDTO dto) throws InvalidRequestException {

        validateRestaurant(dto);

        if (!simpleUserRepository.existsById(dto.getUserId()))
            throw new InvalidRequestException("User not Confirmed");

        validateTimes(dto);

    }


    /**
     * Method for getting the owner of a reservation dto
     *
     * @param dto : reservation dto
     * @return the owner of the reservation
     * @throws InvalidDataException if the user is not found
     */
    @Override
    SimpleUser getUser(ReservationDTO dto) throws InvalidDataException {

        Optional<SimpleUser> user = simpleUserRepository.findById(dto.getUserId());

        if (!user.isPresent())
            throw new InvalidDataException();

        return user.get();
    }
}
