package com.abrahamandrusso.api.v4.service.order.error;

import com.abrahamandrusso.api.v4.service.error.ServiceException;

/**
 * @author Benhamed Sofiane on 31/04/18 13:40
 * <p>
 * Class Exception
 * Thrown when there an error occures while adding the
 * order to the reservation
 */
public class OrderException extends ServiceException {

    // ~ Constructors
    //==============================================================================================

    public OrderException(String message) {
        super(message);
    }
}
