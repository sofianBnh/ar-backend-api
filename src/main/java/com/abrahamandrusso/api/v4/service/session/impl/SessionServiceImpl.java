package com.abrahamandrusso.api.v4.service.session.impl;

import com.abrahamandrusso.api.v4.model.dto.LoginDTO;
import com.abrahamandrusso.api.v4.model.pojo.ServiceResponse;
import com.abrahamandrusso.api.v4.service.authentication.error.AuthenticationException;
import com.abrahamandrusso.api.v4.service.authentication.impl.user_details.UserDetailsImpl;
import com.abrahamandrusso.api.v4.service.session.ISessionService;
import com.abrahamandrusso.api.v4.utils.jwt.IJWTBuilder;
import com.abrahamandrusso.api.v4.utils.jwt.IJWTHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;

import java.util.Objects;

/**
 * @author Benhamed Sofiane on 18/03/18 23:36
 * <p>
 * Class Service
 * Handes the sessions tokens creation and refresh
 */

@Component
public class SessionServiceImpl implements ISessionService {


    // ~ Declarations
    //==========================================================================================

    private final AuthenticationManager authenticationManager;

    private final UserDetailsService userDetailsService;

    private final IJWTBuilder builder;

    private final IJWTHolder holder;


    // ~ Constructors
    //==========================================================================================

    /**
     * @param authenticationManager handles the authentication with the current user service
     * @param userDetailsService    service handling the verification of credentials
     * @param builder               holder builder
     * @param holder                holder for the token
     */
    @Autowired
    public SessionServiceImpl(
            AuthenticationManager authenticationManager,
            @Qualifier("userDetailsServiceImpl") UserDetailsService userDetailsService,
            IJWTBuilder builder,
            IJWTHolder holder
    ) {
        this.authenticationManager = authenticationManager;
        this.userDetailsService = userDetailsService;
        this.builder = builder;
        this.holder = holder;
    }


    // ~ Methods
    //==========================================================================================

    /**
     * Method that creates a holder using the username and password
     *
     * @param login user credentials
     * @return a holder in a {@link ServiceResponse}
     * @throws AuthenticationException if the credentials are incorrect
     */
    @Override
    public ServiceResponse createAuthenticationToken(LoginDTO login) throws AuthenticationException {

        // authenticate the user
        authenticate(login.getUsername(), login.getPassword());

        // retrieve the user details
        final UserDetails userDetails = userDetailsService.loadUserByUsername(login.getUsername());

        // buildReservation the token
        String token = builder.generateToken(userDetails.getUsername());

        // return the token
        return new ServiceResponse(true, token);

    }

    /**
     * Method that refreshes a holder using the old one
     *
     * @param authToken current holder
     * @return an updated holder
     */
    @Override
    public ServiceResponse refreshAndGetAuthenticationToken(String authToken) {

        // updating the holder
        holder.setToken(authToken);

        // extracting the current username
        final String username = holder.getSubject();

        // checking it is present
        if (username != null) {

            // getting the user details
            UserDetailsImpl user = (UserDetailsImpl) userDetailsService.loadUserByUsername(username);

            // checking the presence
            if (user == null)
                return new ServiceResponse(false, "User Not Found");

            // checking if the token is refreshable
            if (holder.isRefreshable(user.getLastPasswordUpdate())) {

                // building the new token
                String refreshToken = builder.refreshToken(holder);

                // returning the token
                return new ServiceResponse(true, refreshToken);

            } else
                return new ServiceResponse(false, "Token not refreshable");

        } else
            return new ServiceResponse(false, "Invalid holder");

    }

    /**
     * Method that returns the current user's id
     *
     * @param authToken user's holder
     * @return the user id
     */
    @Override
    public ServiceResponse getCurrentUserId(String authToken) {

        // updating the holder
        holder.setToken(authToken);

        // getting the username
        final String username = holder.getSubject();

        // checking the presence
        if (username != null) {

            // getting the user details
            final UserDetailsImpl userDetails = (UserDetailsImpl) userDetailsService.loadUserByUsername(username);

            // getting the user id
            Long id = userDetails.getId();

            // returning the id
            return new ServiceResponse(true, id.toString());
        }
        return new ServiceResponse(false, "Bad holder");
    }


    // ~ Private Methods
    //==========================================================================================

    /**
     * Method that authenticates a user's credentials
     *
     * @param username user's username
     * @param password user's password
     */
    private void authenticate(String username, String password) {

        try {
            // handling nulls
            Objects.requireNonNull(username);
            Objects.requireNonNull(password);
        } catch (Exception e) {
            throw new AuthenticationException("No credentials sent", e);
        }

        try {
            // authenticating the credentials
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            username,
                            password
                    )
            );

        } catch (DisabledException e) {
            throw new AuthenticationException("User is disabled!", e);

        } catch (BadCredentialsException e) {
            throw new AuthenticationException("Bad credentials!", e);
        }
    }

}
