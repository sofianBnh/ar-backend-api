package com.abrahamandrusso.api.v4.service.registration.error;

import com.abrahamandrusso.api.v4.service.error.InvalidRequestException;

/**
 * @author Benahmed Sofiane on 28/03/18 17:50
 * <p>
 * Class Exception
 * Thrown when a username already registred is sent in a new request
 */

public class UsernameAlreadyUsedException extends InvalidRequestException {

    // ~ Constructors
    //==========================================================================================

    public UsernameAlreadyUsedException() {
        super("Username Already Used");
    }
}
