package com.abrahamandrusso.api.v4.service.error;

/**
 * @author Benhamed Sofiane on 23/03/18 17:00
 * <p>
 * Class Exception
 * Thrown when there are no more available places
 */

public class NotEnoughPlacesException extends ServiceException {


    // ~ Constructors
    //==========================================================================================

    public NotEnoughPlacesException() {
        super("Not Enough Places");
    }
}
