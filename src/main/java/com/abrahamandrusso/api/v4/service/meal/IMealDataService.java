package com.abrahamandrusso.api.v4.service.meal;

import com.abrahamandrusso.api.v4.model.resource.meal.Meal;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * @author Benahmed Sofiane on 31/04/18 13:40
 * <p>
 * Interface Service
 * Handles the meals
 */
public interface IMealDataService {

    /**
     * Method that get all the available meals
     *
     * @return all the meals
     */
    @Transactional
    List<Meal> findAllMeals();

}
