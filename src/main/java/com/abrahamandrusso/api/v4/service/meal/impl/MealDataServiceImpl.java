package com.abrahamandrusso.api.v4.service.meal.impl;

import com.abrahamandrusso.api.v4.model.resource.meal.Meal;
import com.abrahamandrusso.api.v4.repository.resource.MealRepository;
import com.abrahamandrusso.api.v4.service.meal.IMealDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MealDataServiceImpl implements IMealDataService {


    // ~ Declarations
    //==============================================================================================

    private final MealRepository mealRepository;


    // ~ Constructors
    //==============================================================================================

    /**
     * @param mealRepository repository for meals
     */
    @Autowired
    public MealDataServiceImpl(MealRepository mealRepository) {
        this.mealRepository = mealRepository;
    }


    // ~ Methods
    //==============================================================================================


    /**
     * Method that get all the available meals
     *
     * @return all the meals
     */
    @Override
    public List<Meal> findAllMeals() {
        List<Meal> meals = new ArrayList<>();
        mealRepository.findAll().forEach(meals::add);
        return meals;
    }


}
