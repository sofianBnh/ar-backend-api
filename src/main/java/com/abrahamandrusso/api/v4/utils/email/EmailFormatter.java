package com.abrahamandrusso.api.v4.utils.email;

/**
 * @author Benhamed Sofiane on 03/03/18 18:06
 * <p>
 * Class Utils
 * Handles the formating of the emails
 */

public final class EmailFormatter {

    private static String greeting = "Dear %s,\n\n";

    private static String footer = "\nHave a nice day!\nAbraham & Russo";


    /**
     * Method for the buildReservation of confirmation emails
     *
     * @param username  designator name
     * @param link      confirmation link
     * @param operation operation to be performed
     * @return the formatted email
     */
    public static String makeConfirmationEmail(String username, String link, String operation) {

        // static parts definition
        String guideConfirmation = "\nPlease Click on the link below to confirm your %s .";
        String confirmation = "\nConfirmation link : %s";

        // formatting the email
        return String.format(greeting, username) +
                String.format(guideConfirmation, operation) +
                String.format(confirmation, link) +
                footer;
    }


    /**
     * Method for the buildReservation of simple emails
     *
     * @param username  designator name
     * @param operation operation to be performed
     * @return the formatted email
     */
    public static String makeSimpleEmail(String username, String operation) {

        // static parts
        String notification = "We wanted to notify you of your %s";

        // formatting the email
        return String.format(greeting, username) +
                String.format(notification, operation) +
                footer;
    }


}
