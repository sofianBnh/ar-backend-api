package com.abrahamandrusso.api.v4.utils.jwt;

import java.util.Map;

/**
 * @author Benahmed Sofiane on 29/03/18 17:40
 * <p>
 * Interface Service
 * Responsible for building JWTs
 */

public interface IJWTBuilder {

    /**
     * Method for generating tokens
     *
     * @param subject      subject of the token
     * @param customClaims custom claims of the jzt
     * @return a token
     */
    String generateToken(String subject, Map<String, Object> customClaims);

    /**
     * Method for generating tokens
     *
     * @param subject subject of the token
     * @return a token
     */
    String generateToken(String subject);


    /**
     * Method that refreshes a token
     *
     * @param token old token
     * @return refreshed token
     */
    String refreshToken(IJWTHolder token);

}
