package com.abrahamandrusso.api.v4.utils.jwt.impl;

import com.abrahamandrusso.api.v4.utils.jwt.IJWTBuilder;
import com.abrahamandrusso.api.v4.utils.jwt.IJWTHolder;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Clock;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.DefaultClock;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Benhamed Sofiane on 18/03/18 20:34
 * <p>
 * Interface Service
 * Responsible for building JWTs
 */

@Component
public class JWTBuilderImpl implements IJWTBuilder {

    // ~ Declarations
    //==========================================================================================

    private static Clock clock;


    //~ Application Variables
    //==========================================================================================

    @Value("${jwt.expiration}")
    private long expiration;

    @Value("${jwt.secret}")
    private String secret;


    // ~ Constructors
    //==========================================================================================

    public JWTBuilderImpl() {
        clock = DefaultClock.INSTANCE;
    }


    // ~ Methods
    //==========================================================================================

    /**
     * Method for generating tokens
     *
     * @param subject      subject of the token
     * @param customClaims custom claims of the jzt
     * @return a token
     */
    @Override
    public String generateToken(String subject, Map<String, Object> customClaims) {

        // getting the dates
        final Date createdDate = clock.now();
        final Date expirationDate = calculateExpirationDate(createdDate);

        // building the jwt
        return Jwts.builder()
                .setClaims(customClaims)
                .setSubject(subject)
                .setIssuedAt(createdDate)
                .setExpiration(expirationDate)
                .signWith(SignatureAlgorithm.HS512, secret)
                .compact();
    }

    /**
     * Method for generating tokens
     *
     * @param subject subject of the token
     * @return a token
     */
    @Override
    public String generateToken(String subject) {
        // setting the claims to an empty hash map
        Map<String, Object> claims = new HashMap<>();
        return generateToken(subject, claims);
    }


    /**
     * Method that refreshes a token
     *
     * @param token old token
     * @return refreshed token
     */
    @Override
    public String refreshToken(IJWTHolder token) {

        // previous the dates
        final Date createdDate = clock.now();
        final Date expirationDate = calculateExpirationDate(createdDate);

        // getting the default claims
        final Claims claims = token.getClaims();

        // setting the dates
        claims.setIssuedAt(createdDate);
        claims.setExpiration(expirationDate);

        // building the token
        return Jwts.builder()
                .setClaims(claims)
                .signWith(SignatureAlgorithm.HS512, secret)
                .compact();
    }


    // ~ Private Methods
    //==========================================================================================

    /**
     * Method that calculates the expiration date of a token
     *
     * @param createdDate date of creation
     * @return the expiration date
     */
    private Date calculateExpirationDate(Date createdDate) {
        return new Date(createdDate.getTime() + expiration * 1000);
    }

}
