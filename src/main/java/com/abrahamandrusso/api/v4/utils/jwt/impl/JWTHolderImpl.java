package com.abrahamandrusso.api.v4.utils.jwt.impl;

import com.abrahamandrusso.api.v4.service.authentication.impl.user_details.UserDetailsImpl;
import com.abrahamandrusso.api.v4.utils.jwt.IJWTHolder;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Clock;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.impl.DefaultClock;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.function.Function;

/**
 * @author Benhamed Sofiane on 18/03/18 20:34
 * <p>
 * Class Service
 * Serves as a wraper for the jwt
 */

@Component
public class JWTHolderImpl implements IJWTHolder {

    // ~ Declarations
    //==========================================================================================

    private static Clock clock = DefaultClock.INSTANCE;

    private String token;


    // ~ Application Variables
    //==========================================================================================

    @Value("${jwt.secret}")
    private String secret;

    @Value("${jwt.expiration}")
    private Long expiration;


    // ~ Constructors
    //==========================================================================================

    public JWTHolderImpl() {
    }

    // ~ Methods
    //==========================================================================================

    /**
     * Method that returns the String value of the token
     *
     * @return original token
     */
    @Override
    public String getToken() {
        return token;
    }

    /**
     * Method that sets the String value of the token
     *
     * @param token String value of the token
     */
    @Override
    public void setToken(String token) {
        this.token = token;
    }

    /**
     * Method that gets the subject of the token
     *
     * @return the subject of a token
     */
    @Override
    public String getSubject() {
        return getClaim(Claims::getSubject);
    }

    /**
     * Method that returns the claims of a token
     *
     * @return the claims of a token
     */
    @Override
    public Claims getClaims() {

        return Jwts.parser()
                .setSigningKey(secret)
                .parseClaimsJws(token)
                .getBody();

    }

    // validators

    /**
     * Method that check the validity of a token
     *
     * @param userDetails user of the token
     * @return is the token is valid
     */
    @Override
    public Boolean isValidToken(UserDetails userDetails) {

        // if the token is loaded
        if (token == null)
            return false;

        // getting the user
        UserDetailsImpl user = (UserDetailsImpl) userDetails;

        try {
            // extracting the username
            final String username = getSubject();
            // extracting the creation date
            final Date created = getIssuedAtDate();

            // returning whether the username is correct and the token has not expired
            // and is still usable
            return (
                    username.equals(user.getUsername())
                            && !isExpired()
                            && !isIssuedBeforeLastPasswordUpdate(created, user.getLastPasswordUpdate())
            );
        } catch (JwtException e) {
            return false;
        }
    }

    /**
     * Method that check if a token is refreshable
     *
     * @param lastPasswordReset date of last update of the password
     * @return is the token is refreshable
     */
    @Override
    public Boolean isRefreshable(Date lastPasswordReset) {

        // checks if the token is loaded
        if (token == null)
            return false;

        try {
            // extracting the creation date
            final Date created = getIssuedAtDate();
            // returning if the token has expired and the user hasn't changed his password
            return (!isIssuedBeforeLastPasswordUpdate(created, lastPasswordReset)
                    && !isExpired()
            );

        } catch (JwtException e) {
            return false;
        }
    }


    // ~ Private Methods
    //==========================================================================================

    /**
     * Method for verifying the ist in compqrison with the last password update
     *
     * @param created            date of the creation of the token
     * @param lastPasswordUpdate date of the last update of the user's password
     * @return if the creation date is before the last password update
     */
    private static Boolean isIssuedBeforeLastPasswordUpdate(Date created, Date lastPasswordUpdate) {
        return (lastPasswordUpdate != null && created.before(lastPasswordUpdate));
    }

    /**
     * Method that returns one of the claims of the token
     *
     * @param claimsResolver claim getter
     * @param <T>            generic type of return
     * @return retunrs q specific claim from the tokem
     */
    private <T> T getClaim(Function<Claims, T> claimsResolver) {
        final Claims claims = getClaims();
        return claimsResolver.apply(claims);
    }

    /**
     * Method that gets the creation date of the token
     *
     * @return the token's creation date
     */
    private Date getIssuedAtDate() {
        return getClaim(Claims::getIssuedAt);
    }

    /**
     * Method that gets the expiration date of the token
     *
     * @return the token's creation date
     */
    private Date getExpirationDate() {
        return getClaim(Claims::getExpiration);
    }

    /**
     * Method that checks if the token has expired
     *
     * @return if the token has expired
     */
    private Boolean isExpired() {
        final Date expiration = getExpirationDate();
        return expiration.before(clock.now());
    }

}
