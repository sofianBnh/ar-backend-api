package com.abrahamandrusso.api.v4.utils.time.impl;

import com.abrahamandrusso.api.v4.utils.time.ITimeHandler;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.concurrent.TimeUnit;

/**
 * @author Benhamed Sofiane on 23/03/18 17:51
 * <p>
 * Interface Utils
 * Handles the comparisons of times
 */

@Component
public class TimeHandlerImpl implements ITimeHandler {

    /**
     * Method that returns the current date without the timestamp
     *
     * @return the current date
     */
    @Override
    public Calendar getCurrentDate() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal;
    }

    /**
     * Method that returns if a time is before another while
     * ignoring the date
     *
     * @param start starting time
     * @param end   ending time
     * @return if start is before end
     */
    @Override
    public boolean isBeforeIgnoreDate(Calendar start, Calendar end) {

        Calendar startHolder = Calendar.getInstance();
        Calendar endHolder = Calendar.getInstance();
        Calendar date = Calendar.getInstance();

        startHolder.setTime(date.getTime());
        startHolder.set(Calendar.HOUR_OF_DAY, start.get(Calendar.HOUR_OF_DAY));
        startHolder.set(Calendar.MINUTE, start.get(Calendar.MINUTE));

        endHolder.setTime(date.getTime());
        endHolder.set(Calendar.HOUR_OF_DAY, end.get(Calendar.HOUR_OF_DAY));
        endHolder.set(Calendar.MINUTE, end.get(Calendar.MINUTE));

        long endMillis = startHolder.getTimeInMillis();
        long startMillis = endHolder.getTimeInMillis();

        return endMillis < startMillis;

    }

    /**
     * Method that checks if a period of times crosses with another
     *
     * @param start     start of a period
     * @param end       end of a period
     * @param mainStart start of another period
     * @param mainEnd   end of another period
     * @return is the two period cross
     */
    @Override
    public boolean crossWith(Calendar start, Calendar end, Calendar mainStart, Calendar mainEnd) {
        return isBetween(start, mainStart, mainEnd)
                || isBetween(end, mainStart, mainEnd)
                || isBetween(mainEnd, start, end)
                || isBetween(mainStart, start, end)
                || isTimeEqual(start, mainStart)
                || isTimeEqual(end, mainEnd);
    }

    /**
     * Method that counts the days between two times
     *
     * @param start starting time
     * @param end   ending time
     * @return the number of days
     */
    @Override
    public double daysBetween(Calendar start, Calendar end) {
        long endMillis = start.getTimeInMillis();
        long startMillis = end.getTimeInMillis();
        return TimeUnit.MILLISECONDS.toDays(Math.abs(endMillis - startMillis));
    }

    /**
     * Method that counts the hours between two times
     *
     * @param start starting time
     * @param end   ending time
     * @return the number of hours
     */
    @Override
    public double hoursBetween(Calendar start, Calendar end) {
        long endMillis = start.getTimeInMillis();
        long startMillis = end.getTimeInMillis();
        return TimeUnit.MILLISECONDS.toHours(Math.abs(endMillis - startMillis));
    }

    /**
     * Method that checks if a time is between the start and the end
     *
     * @param time  time to be compared
     * @param start starting time
     * @param end   ending time
     * @return if time is between start and end
     */
    @Override
    public boolean isBetween(Calendar time, Calendar start, Calendar end) {
        return time.after(start) && time.before(end);
    }


    private boolean isTimeEqual(Calendar time1, Calendar time2) {
        return time1.get(Calendar.HOUR_OF_DAY) == time2.get(Calendar.HOUR_OF_DAY)
                && time1.get(Calendar.MINUTE) == time2.get(Calendar.MINUTE);
    }


}
