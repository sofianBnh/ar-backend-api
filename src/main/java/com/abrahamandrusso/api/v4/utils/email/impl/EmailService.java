package com.abrahamandrusso.api.v4.utils.email.impl;

import com.abrahamandrusso.api.v4.utils.email.IEmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

/**
 * @author Benhamed Sofiane on 03/03/18 17:55
 * <p>
 * Interface Service
 * Handles the sending of the emails
 */

@Component
public class EmailService implements IEmailService {

    // ~ Declarations
    //==========================================================================================

    private final JavaMailSender mailSender;


    // ~ Constructors
    //==========================================================================================

    @Autowired
    public EmailService(JavaMailSender mailSender) {
        this.mailSender = mailSender;
    }


    // ~ Methods
    //==========================================================================================


    /**
     * Sends an emails using parameters
     *
     * @param to      email address destination
     * @param subject subject of the email
     * @return if the email was sent
     */
    @Override
    public boolean sendEmail(String content, String to, String subject) {

        // initialise SimpleMailMessage
        SimpleMailMessage message = new SimpleMailMessage();

        // defining attributes
        message.setTo(to);
        message.setSubject(subject);
        message.setText(content);

        // trying to send the email
        try {
            mailSender.send(message);
            return true;

        } catch (MailException e) {
            return false;
        }

    }

}
