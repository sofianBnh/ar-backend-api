package com.abrahamandrusso.api.v4.utils.email;

/**
 * @author Benhamed Sofiane on 03/03/18 17:54
 * <p>
 * Interface Service
 * Handles the sending of the emails
 */

public interface IEmailService {

    /**
     * Sends an emails using parameters
     *
     * @param email content of the email
     * @param to email address destination
     * @param subject subject of the email
     * @return if the email was sent
     */
    boolean sendEmail(String email, String to, String subject);

}
