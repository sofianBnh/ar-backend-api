package com.abrahamandrusso.api.v4.utils.request;

import com.abrahamandrusso.api.v4.model.resource.Reservation;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Benhamed Sofiane on 03/03/18 15:17
 * <p>
 * Class Utils
 * Responsible for the handling of {@link HttpServletRequest}
 * </p>
 */

public final class RequestUtils {

    /**
     * Method that extracts a value from an {@link HttpServletRequest}
     *
     * @param request Http request received
     * @param key     key attribute in the request header
     * @param bearer  token bearer
     * @return token
     */
    public static String extractTokenValue(HttpServletRequest request, String key, String bearer) {

        String header = request.getHeader(key);

        // the header must not be null and it must start with the bearer
        if (header != null && header.startsWith(bearer)) {
            // extracting the jwt
            String tokenString = header.substring(bearer.length());

            // checking the validity of the token
            if (StringUtils.countOccurrencesOf(tokenString, ".") == 2)
                return tokenString;
        }

        // if there is no header or token
        return null;
    }


    public static void removeReservationRedundancy(Reservation reservation) {
        reservation.getRestaurant().setTables(null);
        reservation.getTables().parallelStream().forEach(
                table -> table.setRestaurant(null)
        );
    }

}
