package com.abrahamandrusso.api.v4.utils.jwt;

import io.jsonwebtoken.Claims;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Date;

/**
 * @author Benahmed Sofiane on 29/03/18 17:40
 * <p>
 * Interface Service
 * Serves as a wraper for the jwt
 */

public interface IJWTHolder {

    /**
     * Method that returns the String value of the token
     *
     * @return original token
     */
    String getToken();

    /**
     * Method that sets the String value of the token
     *
     * @param token String value of the token
     */
    void setToken(String token);


    /**
     * Method that returns the claims of a token
     *
     * @return the claims of a token
     */
    Claims getClaims();

    /**
     * Method that gets the subject of the token
     *
     * @return the subject of a token
     */
    String getSubject();

    /**
     * Method that check the validity of a token
     *
     * @param userDetails user of the token
     * @return is the token is valid
     */
    Boolean isValidToken(UserDetails userDetails);

    /**
     * Method that check if a token is refreshable
     *
     * @param lastPasswordReset date of last update of the password
     * @return is the token is refreshable
     */
    Boolean isRefreshable(Date lastPasswordReset);
}
