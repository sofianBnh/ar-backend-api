package com.abrahamandrusso.api.v4.utils.time;

import java.util.Calendar;

/**
 * @author Benahmed Sofiane on 02/04/18 01:10
 * <p>
 * Interface Utils
 * Handles the comparisons of times
 */

public interface ITimeHandler {

    /**
     * Method that checks if a time is between the start and the end
     *
     * @param time  time to be compared
     * @param start starting time
     * @param end   ending time
     * @return if time is between start and end
     */
    boolean isBetween(Calendar time, Calendar start, Calendar end);

    /**
     * Method that checks if a period of times crosses with another
     *
     * @param start     start of a period
     * @param end       end of a period
     * @param mainStart start of another period
     * @param mainEnd   end of another period
     * @return is the two period cross
     */
    boolean crossWith(Calendar start, Calendar end, Calendar mainStart, Calendar mainEnd);

    /**
     * Method that counts the hours between two times
     *
     * @param start starting time
     * @param end   ending time
     * @return the number of hours
     */
    double hoursBetween(Calendar start, Calendar end);

    /**
     * Method that counts the days between two times
     *
     * @param start starting time
     * @param end   ending time
     * @return the number of days
     */
    double daysBetween(Calendar start, Calendar end);


    /**
     * Method that returns the current date without the timestamp
     *
     * @return the current date
     */
    Calendar getCurrentDate();


    /**
     * Method that returns if a time is before another while
     * ignoring the date
     *
     * @param start starting time
     * @param end   ending time
     * @return if start is before end
     */
    boolean isBeforeIgnoreDate(Calendar start, Calendar end);

}
