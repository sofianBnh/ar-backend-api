package com.abrahamandrusso.api.v4.utils.response;

import com.abrahamandrusso.api.v4.model.pojo.ServiceResponse;
import org.springframework.http.ResponseEntity;

/**
 * @author Benhamed Sofiane on 13/04/18 13:57
 * <p>
 * Class Utility
 * Responsible for the handling common functions
 * in controllers
 * </p>
 */

public final class ControllerUtils {

    /**
     * Method for warping {@link ServiceResponse} by {@link ResponseEntity}
     * according to the <em>ok</em> attribute
     *
     * @param response : {@link ServiceResponse}
     * @return {@link ResponseEntity} based on the serviceResponse
     */
    public static ResponseEntity<?> buildResponse(ServiceResponse response) {

        if (response.isOk())
            return ResponseEntity.ok().body(response.getContent());
        else
            return ResponseEntity.badRequest().body(response.getContent());
    }

}
