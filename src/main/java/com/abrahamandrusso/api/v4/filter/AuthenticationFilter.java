package com.abrahamandrusso.api.v4.filter;

import com.abrahamandrusso.api.v4.service.authentication.IAuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Service;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.abrahamandrusso.api.v4.utils.request.RequestUtils.extractTokenValue;

/**
 * @author Benhamed Sofiane on 18/03/18 20:40
 * <p>
 * Class Filter
 * Handles the filtering of the http requests
 */

@Service
public class AuthenticationFilter extends OncePerRequestFilter {


    // ~ Declarations
    //==========================================================================================

    private final IAuthenticationService service;


    //~ Application Variables
    //==============================================================================================

    @Value("${jwt.token-header}")
    private String tokenHeader;

    @Value("${jwt.token-bearer}")
    private String bearer;


    // ~ Constructors
    //==========================================================================================

    /**
     * @param service : Service responsible for the verification of the authentication
     */
    @Autowired
    public AuthenticationFilter(IAuthenticationService service) {
        this.service = service;
    }


    // ~ Methods
    //==========================================================================================

    @Override
    protected void doFilterInternal(
            HttpServletRequest request,
            HttpServletResponse response,
            FilterChain filterChain
    ) {

        // getting the token from the request
        final String token = extractTokenValue(request, tokenHeader, bearer);

        if (token != null) {

            // checking if the request is allowed to pass
            UserDetails userDetails = service.checkIfAllowed(token);

            // if a user is detected and no session is open
            if (userDetails != null && SecurityContextHolder.getContext().getAuthentication() == null) {
                // set the session of the user
                setUserAuthentication(request, userDetails);
            }

        }


        try {
            // allow the request to access the rest of the server
            filterChain.doFilter(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    // ~ Private Methods
    //==========================================================================================

    /**
     * Method that sets the user authentication token using the credentials
     * retrieved from the user details
     *
     * @param request     : Http request
     * @param userDetails : User Details extracted
     */
    private void setUserAuthentication(HttpServletRequest request, UserDetails userDetails) {

        // extracting the UsernamePasswordAuthenticationToken
        UsernamePasswordAuthenticationToken authentication =

                new UsernamePasswordAuthenticationToken(
                        userDetails,
                        null,
                        userDetails.getAuthorities()
                );

        // setting the user
        authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

        // updating the Security Context
        SecurityContextHolder.getContext().setAuthentication(authentication);

    }

}
