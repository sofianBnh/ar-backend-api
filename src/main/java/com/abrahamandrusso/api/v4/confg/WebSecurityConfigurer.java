package com.abrahamandrusso.api.v4.confg;

import com.abrahamandrusso.api.v4.filter.AuthenticationFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author Benhamed Sofiane on 15/03/18 19:52
 * <p>
 * Class Configuartion
 * Responsible for the Security Configuration
 * and definig the authentication filter
 * </p>
 */

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfigurer extends WebSecurityConfigurerAdapter {


    //~ Declarations
    //==============================================================================================

    private final UserDetailsService userDetailsService;

    private final AuthenticationFilter authenticationFilter;


    //~ Application Variables
    //==============================================================================================


    @Value("${jwt.token-header}")
    private String tokenHeader;

    @Value("${jwt.route.authentication.path}")
    private String authenticationPath;


    // ~ Constructors
    //==============================================================================================


    @Autowired
    public WebSecurityConfigurer(
            @Qualifier("userDetailsServiceImpl") UserDetailsService userDetailsService,
            AuthenticationFilter authenticationFilter
    ) {

        this.userDetailsService = userDetailsService;
        this.authenticationFilter = authenticationFilter;

    }


    // ~ Bean Declaration
    //==============================================================================================


    /**
     * Method providing the authentication bean for the application context
     *
     * @return AuthenticationManager object
     * @throws Exception in case of an error occurring during the creation of such bean
     */
    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    /**
     * Method providing the authentication bean for the application context
     *
     * @return PasswordEncoder object responsible for the encryption of the passwords sent
     */
    @Bean
    public PasswordEncoder passwordEncoderBean() {
        return new BCryptPasswordEncoder();
    }

    /**
     * Method providing the authentication bean for the application context
     *
     * @return PasswordEncoder object
     */
    @Bean
    public PasswordEncoder getPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    /**
     * Method for the configuration of the access to the application from other application
     * used during tests with the React Front end
     *
     * @return WebMvcConfigurer object
     */
    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                // allow requests made to all routes from port 3000
                registry.addMapping("/**")
                        .allowedOrigins("*");
            }
        };
    }


    // ~ Configuration Methods
    //==============================================================================================


    /**
     * @param auth Builder for the {@link AuthenticationManager}
     * @throws Exception in case of an error occurring during the configuration of the userDetailsService
     *                   or the loading of the password encoder bean
     */
    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {

        auth
                // using the custom userDetailsService to load the users during authentication
                .userDetailsService(userDetailsService)
                // configuring the password encoder to crypt the received passwords
                .passwordEncoder(passwordEncoderBean());
    }

    /**
     * Method for the configuration of the filter for http requests
     *
     * @param httpSecurity {@link HttpSecurity} Object
     * @throws Exception in case of a configuration problem
     */
    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
//                .requestMatchers(EndpointRequest.to("health"))

        httpSecurity

                // disabling csrf due to the custom use of the JWT authentication
                .csrf().disable()

                .cors().and()

                // setting the session to be stateless
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()

                // authorizing requests to the web service
                .authorizeRequests()

                // allowing access to the embedded database for development purpose
                .antMatchers("/h2/**/**").permitAll()


                // adding the authorization requirement for admin
                .antMatchers("/restaurants/**").hasRole("ADMIN")
                .antMatchers("/reservations/**").hasRole("ADMIN")
                .antMatchers("/meals/**").hasRole("ADMIN")

                .antMatchers("/roles/**").hasRole("ADMIN")
                .antMatchers("/simpleUsers/**").hasRole("ADMIN")
                .antMatchers("/users/**").hasRole("ADMIN")

                .antMatchers("/specialDays/**").hasRole("ADMIN")
                .antMatchers("/defaultPlannings/**").hasRole("ADMIN")

                .antMatchers("/fixedDiscounts/**").hasRole("ADMIN")
                .antMatchers("/dynamicDiscounts/**").hasRole("ADMIN")
                .antMatchers("/defaultDiscounts/**").hasRole("ADMIN")

                .antMatchers("/tables/**").hasRole("ADMIN")


                // allowing access to the session controller
                .antMatchers("/auth/**").permitAll()


                // allowing access to the restaurant resources routes
                .antMatchers("/restaurant-resources/**").permitAll()

                // allowing access to the discount resources routes
                .antMatchers("/discount-resources/**").permitAll()

                // allowing access to the meal resources routes
                .antMatchers("/meal-resources/**").permitAll()

                // allowing access to the registration controller
                .antMatchers("/registration-service/**").permitAll()

                // allowing access to the non-registered client reservation route
                .antMatchers(HttpMethod.POST, "/reservation-service/simple-users").permitAll()

                // allowing access to the non-registered client planning route
                .antMatchers(HttpMethod.POST, "/planning-service/day-planning").permitAll()

                // allowing access to the confirmation route
                .antMatchers(HttpMethod.GET, "/reservation-service/confirm").permitAll()


                // all other requests must be authenticated
                .anyRequest().authenticated();


        httpSecurity
                // configuring the custom filter to authenticate requests
                .addFilterBefore(authenticationFilter, UsernamePasswordAuthenticationFilter.class);


        httpSecurity
                .headers()
                .frameOptions().sameOrigin()  // required to set for H2 else H2 Console will be blank.
                .cacheControl();

    }


    /**
     * Method for configuring exceptions to the HttpSecurity configuration
     *
     * @param web {@link WebSecurity} object
     */
    @Override
    public void configure(WebSecurity web) {

        // allowing GET requests to static resources using
        web
                .ignoring()
                .antMatchers(
                        HttpMethod.GET,
                        "/",
                        "/*.html",
                        "/favicon.ico",
                        "/**/*.html",
                        "/**/*.css",
                        "/**/*.js"
                );

    }

}
