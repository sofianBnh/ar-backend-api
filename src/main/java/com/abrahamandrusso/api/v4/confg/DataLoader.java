package com.abrahamandrusso.api.v4.confg;

import com.abrahamandrusso.api.v4.model.resource.Restaurant;
import com.abrahamandrusso.api.v4.model.resource.RestaurantTable;
import com.abrahamandrusso.api.v4.model.resource.discount.DefaultDiscount;
import com.abrahamandrusso.api.v4.model.resource.discount.FixedDiscount;
import com.abrahamandrusso.api.v4.model.resource.meal.Meal;
import com.abrahamandrusso.api.v4.model.resource.meal.MealType;
import com.abrahamandrusso.api.v4.model.resource.planning.DefaultPlanning;
import com.abrahamandrusso.api.v4.model.resource.planning.SpecialDay;
import com.abrahamandrusso.api.v4.model.user.User;
import com.abrahamandrusso.api.v4.model.user.role.Role;
import com.abrahamandrusso.api.v4.model.user.role.Roles;
import com.abrahamandrusso.api.v4.repository.resource.MealRepository;
import com.abrahamandrusso.api.v4.repository.resource.RestaurantRepository;
import com.abrahamandrusso.api.v4.repository.resource.RestaurantTableRepository;
import com.abrahamandrusso.api.v4.repository.resource.discount.DefaultDiscountRepository;
import com.abrahamandrusso.api.v4.repository.resource.discount.FixedDiscountRepository;
import com.abrahamandrusso.api.v4.repository.resource.planning.DefaultPlanningRepository;
import com.abrahamandrusso.api.v4.repository.resource.planning.SpecialDayRepository;
import com.abrahamandrusso.api.v4.repository.user.RoleRepository;
import com.abrahamandrusso.api.v4.repository.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import static java.util.Arrays.asList;

/**
 * @author Benhamed Sofiane on 26/02/18 18:01
 * <p>
 * Class Configuartion
 * Responsable for the initial loading of the data in
 * the embeded database
 * </p>
 */
@Configuration
public class DataLoader implements ApplicationRunner {


    // ~ Declarations
    //==========================================================================================

    private final DefaultDiscountRepository defaultDiscountRepository;

    private final RestaurantTableRepository restaurantTableRepository;

    private final DefaultPlanningRepository defaultPlanningRepository;

    private final FixedDiscountRepository fixedDiscountRepository;

    private final RestaurantRepository restaurantRepository;

    private final SpecialDayRepository specialDayRepository;

    private final UserRepository userRepository;

    private final RoleRepository roleRepository;

    private final MealRepository mealRepository;


    // ~ Constructors
    //==========================================================================================

    /**
     * @param defaultPlanningRepository Planing Objects repository
     * @param defaultDiscountRepository Default Discount Objects repository
     * @param restaurantTableRepository Restaurant Tables Objects repository
     * @param fixedDiscountRepository   Fixed Discounts Repository
     * @param restaurantRepository      Restaurant Objects repository
     * @param specialDayRepository      Special Day Objects repository
     * @param userRepository            User Objects repository
     * @param roleRepository            Role objects repository
     * @param mealRepository            Meals Objects repository
     */
    @Autowired

    public DataLoader(
            DefaultPlanningRepository defaultPlanningRepository,
            DefaultDiscountRepository defaultDiscountRepository,
            RestaurantTableRepository restaurantTableRepository,
            FixedDiscountRepository fixedDiscountRepository,
            RestaurantRepository restaurantRepository,
            SpecialDayRepository specialDayRepository,
            UserRepository userRepository,
            RoleRepository roleRepository,
            MealRepository mealRepository
    ) {
        this.defaultDiscountRepository = defaultDiscountRepository;
        this.defaultPlanningRepository = defaultPlanningRepository;
        this.restaurantTableRepository = restaurantTableRepository;
        this.fixedDiscountRepository = fixedDiscountRepository;
        this.restaurantRepository = restaurantRepository;
        this.specialDayRepository = specialDayRepository;
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.mealRepository = mealRepository;
    }


    // ~ Methods
    //==========================================================================================

    /**
     * Method that executes code when the application is started
     *
     * @param applicationArguments Arguments passed in the command line
     */

    @Override
    public void run(ApplicationArguments applicationArguments) {

        // creating basic roles
        Role adminRole = new Role(Roles.ROLE_ADMIN);
        Role simpleUserRole = new Role(Roles.ROLE_SIMPLE_USER);
        Role userRole = new Role(Roles.ROLE_USER);

        PasswordEncoder encoder = new BCryptPasswordEncoder();

        // creating user
        User user = new User(
                "John Doe",
                "jd@example.com",
                "55555555",
                "John",
                encoder.encode("password"),
                Collections.singletonList(userRole)
        );

        User admin = new User(
                "Admin Man",
                "ad@admin.com",
                "6565656",
                "Admin",
                encoder.encode("admin"),
                Collections.singletonList(adminRole)
        );

        // creating a restaurant
        Restaurant restaurant = new Restaurant(
                "21, Baker Street, London",
                55.555555d,
                -54.22222d
        );

        restaurantRepository.save(restaurant);

//        Restaurant restaurant2 = new Restaurant(
//                "No Tables",
//                22.33d,
//                -44.3d
//        );


        List<RestaurantTable> tables = asList(
                new RestaurantTable(2, 4, restaurant),
                new RestaurantTable(1, 6, restaurant),
                new RestaurantTable(1, 2, restaurant),
                new RestaurantTable(4, 7, restaurant)
        );


        // setting the planning
        Calendar opening = Calendar.getInstance();
        opening.set(Calendar.HOUR_OF_DAY, 7);
        opening.set(Calendar.MINUTE, 0);

        Calendar closing = Calendar.getInstance();
        closing.set(Calendar.HOUR_OF_DAY, 23);
        closing.set(Calendar.MINUTE, 0);

        DefaultPlanning defaultPlanning = new DefaultPlanning(
                opening, closing
        );

        Calendar dayOff = Calendar.getInstance();
        dayOff.set(Calendar.DAY_OF_MONTH, dayOff.get(Calendar.DAY_OF_MONTH) + 3);
        SpecialDay specialDay = new SpecialDay(dayOff, dayOff, dayOff, false);

        Calendar start = Calendar.getInstance();
        Calendar end = Calendar.getInstance();
        end.set(Calendar.DAY_OF_MONTH, end.get(Calendar.DAY_OF_MONTH) + 2);
        FixedDiscount fixedDiscount = new FixedDiscount(start, end, 50d);


        DefaultDiscount defaultDiscount = new DefaultDiscount();
        defaultDiscount.setRatio(0.2);

        Meal meal = new Meal("food", 10d, "Some very good food", MealType.MAIN);
        Meal anotherMeal = new Meal("super food", 100d, "Some super and very good food", MealType.DESERT);
        Meal yetAnotherMeal = new Meal("a food", 50d, "Some intro to a food", MealType.STARTER);

        // saving the objects in the database
        user.setEnabled(true);
        admin.setEnabled(true);
        roleRepository.save(userRole);
        roleRepository.save(simpleUserRole);
        roleRepository.save(adminRole);
        userRepository.save(user);
        userRepository.save(admin);


        restaurantTableRepository.saveAll(tables);

        defaultPlanningRepository.save(defaultPlanning);
        specialDayRepository.save(specialDay);

        mealRepository.save(meal);
        mealRepository.save(anotherMeal);
        mealRepository.save(yetAnotherMeal);

        defaultDiscountRepository.save(defaultDiscount);
        fixedDiscountRepository.save(fixedDiscount);
    }
}
