package com.abrahamandrusso.api.v4.model.pojo;

/**
 * @author Benhamed Sofiane on 13/04/18 18:11
 * <p>
 * Class Pojo
 * Used to send data from a service to
 * the {@link com.abrahamandrusso.api.v4.service.notification.INotificationService}
 */

public class Notification {


    // ~ Attributes
    //================================================================================================

    private String userName;

    private String userEmail;

    private String operation;

    private String objectId;

    private String subject;

    private String route;


    // ~ Constructors
    //================================================================================================

    public Notification(
            String userName,
            String userEmail,
            String operation,
            String objectId,
            String subject,
            String route
    ) {
        this.userName = userName;
        this.userEmail = userEmail;
        this.operation = operation;
        this.objectId = objectId;
        this.subject = subject;
        this.route = route;
    }


    // ~ Getters & Setters
    //================================================================================================


    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }


    // ~ Builder
    //================================================================================================


    public static final class NotificationBuilder {
        private String userName;
        private String userEmail;
        private String operation;
        private String objectId;
        private String subject;
        private String route;

        private NotificationBuilder() {
        }

        public static NotificationBuilder aNotification() {
            return new NotificationBuilder();
        }

        public NotificationBuilder withUserName(String userName) {
            this.userName = userName;
            return this;
        }

        public NotificationBuilder withUserEmail(String userEmail) {
            this.userEmail = userEmail;
            return this;
        }

        public NotificationBuilder withOperation(String operation) {
            this.operation = operation;
            return this;
        }

        public NotificationBuilder withObjectId(String objectId) {
            this.objectId = objectId;
            return this;
        }

        public NotificationBuilder withSubject(String subject) {
            this.subject = subject;
            return this;
        }

        public NotificationBuilder withRoute(String route) {
            this.route = route;
            return this;
        }

        public Notification build() {
            return new Notification(userName, userEmail, operation, objectId, subject, route);
        }
    }
}
