package com.abrahamandrusso.api.v4.model.resource.meal;

import com.abrahamandrusso.api.v4.model.BaseEntity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * @author Benhamed Sofiane on 29/04/18 21:15
 * Class Entity
 * <p>
 * Stores the data about the restaurants' meals
 */

@Entity
@Table(name = "Meals")
public class Meal extends BaseEntity {

    // ~ Attributes
    //================================================================================================

    @NotNull
    @Column(unique = true)
    private String name;

    @NotNull
    private Double price;

    @NotNull
    @Enumerated(value = EnumType.STRING)
    private MealType type;

    @NotNull
    private String description;


    // ~ Constructors
    //================================================================================================

    public Meal() {
        super();
    }

    public Meal(
            @NotNull String name,
            @NotNull double price,
            @NotNull String description,
            @NotNull MealType type
    ) {
        this();
        this.name = name;
        this.price = price;
        this.description = description;
        this.type = type;
    }


    // ~ Getters & Setters
    //================================================================================================

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public MealType getType() {
        return type;
    }

    public void setType(MealType type) {
        this.type = type;
    }
}
