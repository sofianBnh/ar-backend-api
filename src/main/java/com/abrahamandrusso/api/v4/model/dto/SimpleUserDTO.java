package com.abrahamandrusso.api.v4.model.dto;

import javax.validation.constraints.NotNull;

/**
 * @author Benhamed Sofiane on 03/03/18 15:52
 * <p>
 * Class Data Transfer for {@link com.abrahamandrusso.api.v4.model.user.SimpleUser}U
 * Used while creating a simple user
 * </p>
 */

public class SimpleUserDTO {


    // ~ Attributes
    //================================================================================================

    @NotNull
    private String email;

    @NotNull
    private String name;

    @NotNull
    private String phone;

    // ~ Constructors
    //================================================================================================

    public SimpleUserDTO(
            @NotNull String email,
            @NotNull String name,
            @NotNull String phone
    ) {
        this.email = email;
        this.name = name;
        this.phone = phone;
    }

    public SimpleUserDTO() {
    }


    // ~ Getters & Setters
    //================================================================================================


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

}
