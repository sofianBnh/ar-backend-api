package com.abrahamandrusso.api.v4.model.user;

import com.abrahamandrusso.api.v4.model.user.role.Role;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Calendar;
import java.util.List;

/**
 * @author Benhamed Sofiane on 15/03/18 18:26
 * Class Entity
 * <p>
 * Stores the data about Users
 */


@Entity
@Component
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class User extends SimpleUser implements IUser {

    // ~ Attributes
    //================================================================================================

    @NotNull
    @Column(name = "enabled")
    private boolean enabled;

    @NonNull
    @Size(min = 3, max = 24)
    @Column(name = "username", unique = true)
    private String username;

    @NonNull
    @JsonIgnore
    @Column(name = "password")
    @Size(min = 60)
    private String password;

    @Temporal(value = TemporalType.TIMESTAMP)
    @Column(name = "last_password_update")
    private Calendar lastPasswordUpdate;


    // ~ Constructors
    //================================================================================================


    public User(
            @NotNull String name,
            @NotNull String email,
            @NotNull String phone,
            @NonNull @Size(min = 3, max = 24) String username,
            @NonNull String password,
            @NotNull List<Role> roles
    ) {

        super(name, email, phone, roles);
        this.enabled = false;
        this.username = username;
        this.password = password;
        this.lastPasswordUpdate = Calendar.getInstance();

    }

    public User() {
        super();
    }


    // ~ Getters & Setters
    //================================================================================================


    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
        setLastPasswordUpdate(Calendar.getInstance());
    }

    public Calendar getLastPasswordUpdate() {
        return lastPasswordUpdate;
    }

    public void setLastPasswordUpdate(Calendar lastPasswordUpdate) {
        this.lastPasswordUpdate = lastPasswordUpdate;
    }

}
