package com.abrahamandrusso.api.v4.model.resource.discount;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Calendar;

/**
 * @author Benhamed Sofiane on 29/04/18 21:15
 * Class Entity
 * <p>
 * Stores the data about the discounts set to
 * retduce a pourcentage amount from the price
 */

@Entity
@Table(name = "Dynamic_Discounts")
public class DynamicDiscount extends Discount {

    // ~ Attributes
    //================================================================================================

    @NotNull
    @Max(1)
    @Min(0)
    private Double ratio;


    // ~ Constructors
    //================================================================================================

    public DynamicDiscount() {
        super();
    }

    public DynamicDiscount(
            @NotNull Calendar start,
            @NotNull Calendar end,
            @NotNull Double ratio
    ) {
        super(start, end);
        this.ratio = ratio;
    }


    // ~ Getters & Setters
    //================================================================================================

    public Double getRatio() {
        return ratio;
    }

    public void setRatio(Double ratio) {
        this.ratio = ratio;
    }


    // ~ Entity Methods
    //================================================================================================

    @Override
    public double calculateNewPrice(double price) {
        return price - price * ratio;
    }

    @Override
    public boolean isApplicable(double price) {
        return true;
    }
}
