package com.abrahamandrusso.api.v4.model.pojo;

/**
 * @author Benhamed Sofiane on 23/03/18 16:26
 * Class Pojo
 * <p>
 * Used to send data from the service layer to the controller layer
 */

public class ServiceResponse {


    // ~ Attributes
    //================================================================================================

    private boolean ok;

    private String content;

    // ~ Constructors
    //================================================================================================

    public ServiceResponse() {
    }

    public ServiceResponse(boolean ok, String content) {
        this.ok = ok;
        this.content = content;
    }

    // ~ Getters & Setters
    //================================================================================================


    public boolean isOk() {
        return ok;
    }

    public void setOk(boolean ok) {
        this.ok = ok;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
