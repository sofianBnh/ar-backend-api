package com.abrahamandrusso.api.v4.model.resource.planning;

import com.abrahamandrusso.api.v4.model.BaseEntity;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import java.util.Calendar;

/**
 * @author Benhamed Sofiane on 29/04/18 21:15
 * Class Entity
 * <p>
 * Stores the data about the restaurants'
 * default planning
 */

@Entity
@Table(name = "Plannings")
public class DefaultPlanning extends BaseEntity {

    // ~ Attributes
    //================================================================================================

    @NotNull
    @DateTimeFormat(pattern = "HH:mm")
//    @JsonFormat(pattern = "HH:mm")
    @Temporal(value = TemporalType.TIMESTAMP)
    private Calendar openingTime;

    @NotNull
    @DateTimeFormat(pattern = "HH:mm")
//    @JsonFormat(pattern = "HH:mm")
    @Temporal(value = TemporalType.TIMESTAMP)
    private Calendar closingTime;


    // ~ Constructors
    //================================================================================================

    public DefaultPlanning() {
        super();
    }

    public DefaultPlanning(
            @NotNull Calendar openingTime,
            @NotNull Calendar closingTime
    ) {
        this();
        this.openingTime = openingTime;
        this.closingTime = closingTime;
    }


    // ~ Getters & Setters
    //================================================================================================

    public Calendar getOpeningTime() {
        return openingTime;
    }

    public void setOpeningTime(Calendar openingTime) {
        this.openingTime = openingTime;
    }

    public Calendar getClosingTime() {
        return closingTime;
    }

    public void setClosingTime(Calendar closingTime) {
        this.closingTime = closingTime;
    }
}
