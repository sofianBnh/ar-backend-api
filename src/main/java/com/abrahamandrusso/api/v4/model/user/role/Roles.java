package com.abrahamandrusso.api.v4.model.user.role;

/**
 * @author Benhamed Sofiane on 15/03/18 18:34
 * Default Roles of the application
 */

public enum Roles {
    ROLE_SIMPLE_USER,
    ROLE_USER,
    ROLE_ADMIN
}
