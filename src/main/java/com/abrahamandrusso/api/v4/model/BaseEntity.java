package com.abrahamandrusso.api.v4.model;

import javax.persistence.*;

/**
 * @author Benhamed Sofiane on 26/02/18 17:51
 * Class Entity
 * <p>
 * Defiens the default attributes available in all entites
 */

@MappedSuperclass
public abstract class BaseEntity {

    // ~ Attributes
    //================================================================================================

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private final Long id;

    @Version
    private Long version;


    // ~ Constructors
    //================================================================================================

    protected BaseEntity() {
        id = null;
    }


    // ~ Getters & Setters
    //================================================================================================

    public Long getId() {
        return id;
    }

}
