package com.abrahamandrusso.api.v4.model.resource.discount;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.Calendar;

/**
 * @author Benhamed Sofiane on 29/04/18 21:15
 * Class Entity
 * <p>
 * Stores the data about the discounts set to
 * retduce a specific amount from the price
 */

@Entity
@Table(name = "Fixed_Discounts")
public class FixedDiscount extends Discount {

    // ~ Attributes
    //================================================================================================

    @NotNull
    private Double reduction;


    // ~ Constructors
    //================================================================================================

    public FixedDiscount() {
        super();
    }

    public FixedDiscount(
            @NotNull Calendar start,
            @NotNull Calendar end,
            @NotNull Double reduction
    ) {
        super(start, end);
        this.reduction = reduction;
    }


    // ~ Getters & Setters
    //================================================================================================

    public Double getReduction() {
        return reduction;
    }

    public void setReduction(Double reduction) {
        this.reduction = reduction;
    }


    // ~ Entity Methods
    //================================================================================================

    @Override
    public double calculateNewPrice(double price) {
        return price - reduction;
    }

    @Override
    public boolean isApplicable(double price) {
        return price > reduction;
    }

}
