package com.abrahamandrusso.api.v4.model.dto;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author Benhamed Sofiane on 15/03/18 19:36
 * <p>
 * Class Data Transfer
 * Used while making a new order
 * </p>
 */

public class OrderDTO {

    // ~ Attributes
    //================================================================================================

    @NotNull
    private List<Long> meals;

    @NotNull
    private Long reservationId;


    // ~ Constructors
    //================================================================================================

    public OrderDTO() {
    }

    public OrderDTO(@NotNull List<Long> meals, @NotNull Long reservationId) {
        this.meals = meals;
        this.reservationId = reservationId;
    }


    // ~ Getters & Setters
    //================================================================================================

    public List<Long> getMeals() {
        return meals;
    }

    public void setMeals(List<Long> meals) {
        this.meals = meals;
    }

    public Long getReservationId() {
        return reservationId;
    }

    public void setReservationId(Long reservationId) {
        this.reservationId = reservationId;
    }
}
