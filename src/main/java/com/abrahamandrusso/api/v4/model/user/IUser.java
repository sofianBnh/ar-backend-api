package com.abrahamandrusso.api.v4.model.user;

import com.abrahamandrusso.api.v4.model.user.role.Role;

import java.util.Calendar;
import java.util.List;

/**
 * @author Benhamed Sofiane on 15/03/18 19:33
 * Interface reprensting a fully registred user
 */

public interface IUser {

    Long getId();

    List<Role> getRoles();

    String getPassword();

    String getUsername();

    boolean isEnabled();

    Calendar getLastPasswordUpdate();

}
