package com.abrahamandrusso.api.v4.model.resource.discount;

import com.abrahamandrusso.api.v4.model.BaseEntity;

import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import java.util.Calendar;

/**
 * @author Benhamed Sofiane on 29/04/18 21:15
 * Class Base
 * <p>
 * Stores the data about the Discounts
 */

@MappedSuperclass
public abstract class Discount extends BaseEntity implements Reducible {

    // ~ Attributes
    //================================================================================================

    @Temporal(value = TemporalType.DATE)
    private Calendar start;

    @Temporal(value = TemporalType.DATE)
    private Calendar end;


    // ~ Constructors
    //================================================================================================

    public Discount() {
        super();
    }

    public Discount(
            @NotNull Calendar start,
            @NotNull Calendar end
    ) {
        this();
        this.start = start;
        this.end = end;
    }


    // ~ Getters & Setters
    //================================================================================================

    public Calendar getStart() {
        return start;
    }

    public void setStart(Calendar start) {
        this.start = start;
    }

    public Calendar getEnd() {
        return end;
    }

    public void setEnd(Calendar end) {
        this.end = end;
    }

}
