package com.abrahamandrusso.api.v4.model.resource;

import com.abrahamandrusso.api.v4.model.BaseEntity;
import com.abrahamandrusso.api.v4.model.resource.meal.Meal;
import com.abrahamandrusso.api.v4.model.user.SimpleUser;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Calendar;
import java.util.List;

/**
 * @author Benhamed Sofiane on 22/02/18 21:09
 * Class Entity
 * <p>
 * Stores the data about Reservations
 */


@Entity
@Table(name = "Reservations")
public class Reservation extends BaseEntity {


    // ~ Attributes
    //================================================================================================

    @NotNull
    @Temporal(value = TemporalType.TIMESTAMP)
//    @JsonFormat(pattern = "HH:mm")
    @DateTimeFormat(pattern = "HH:mm")
    @Column(name = "start_time")
    private Calendar startTime;

    @NotNull
    @Temporal(value = TemporalType.TIMESTAMP)
//    @JsonFormat(pattern = "HH:mm")
    @DateTimeFormat(pattern = "HH:mm")
    @Column(name = "end_time")
    private Calendar endTime;

    @NotNull
    @Temporal(value = TemporalType.DATE)
    @Column(name = "date")
    private Calendar date;

    @NotNull
    @Column(name = "confirmed")
    private boolean confirmed;

    @Column(name = "price")
    private Double price;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private SimpleUser user;

    @ManyToOne
    @JoinColumn(name = "restaurant_id", nullable = false)
    private Restaurant restaurant;

    @ManyToMany
    private List<Meal> meals;

    @ManyToMany
    private List<RestaurantTable> tables;


    // ~ Constructors
    //================================================================================================

    public Reservation(
            SimpleUser user,
            Restaurant restaurant,
            Calendar startTime,
            Calendar endTime,
            Calendar date
    ) {
        this();
        this.user = user;
        this.restaurant = restaurant;
        this.startTime = startTime;
        this.endTime = endTime;
        this.date = date;
        this.confirmed = false;
    }


    public Reservation(
            SimpleUser user,
            Restaurant restaurant,
            Calendar startTime,
            Calendar endTime,
            Calendar date,
            List<RestaurantTable> tables,
            List<Meal> meals
    ) {
        this(user, restaurant, startTime, endTime, date);
        this.tables = tables;
        this.meals = meals;
    }


    // ~ Getters & Setters
    //================================================================================================


    public Reservation() {
        super();
    }

    public SimpleUser getUser() {
        return user;
    }

    public void setUser(SimpleUser user) {
        this.user = user;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    public Calendar getStartTime() {
        return startTime;
    }

    public void setStartTime(Calendar startTime) {
        this.startTime = startTime;
    }

    public Calendar getEndTime() {
        return endTime;
    }

    public void setEndTime(Calendar endTime) {
        this.endTime = endTime;
    }

    public Calendar getDate() {
        return date;
    }

    public void setDate(Calendar date) {
        this.date = date;
    }

    public List<RestaurantTable> getTables() {
        return tables;
    }

    public void setTables(List<RestaurantTable> tables) {
        this.tables = tables;
    }

    public boolean isConfirmed() {
        return confirmed;
    }

    public void setConfirmed(boolean confirmed) {
        this.confirmed = confirmed;
    }

    public List<Meal> getMeals() {
        return meals;
    }

    public void setMeals(List<Meal> meals) {
        this.meals = meals;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    @JsonIgnore
    public int getNumberOfPlaces() {
        return tables.parallelStream().mapToInt(RestaurantTable::getMaxCapacity).sum();
    }
}
