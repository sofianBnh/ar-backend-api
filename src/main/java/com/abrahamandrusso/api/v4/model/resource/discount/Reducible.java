package com.abrahamandrusso.api.v4.model.resource.discount;

/**
 * @author Benhamed Sofiane on  29/04/18 21:15
 * Interface that reprensts an entity capable
 * of reducing the price of a reservation
 */

public interface Reducible {

    double calculateNewPrice(double oldPrice);

    boolean isApplicable(double price);
}
