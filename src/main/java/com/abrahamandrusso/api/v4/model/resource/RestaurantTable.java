package com.abrahamandrusso.api.v4.model.resource;

import com.abrahamandrusso.api.v4.model.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * @author Benhamed Sofiane on 29/04/18 21:15
 * Class Entity
 * <p>
 * Stores the data about the restaurants' tables
 */

@Entity
@Table(name = "Tables")
public class RestaurantTable extends BaseEntity {

    // ~ Attributes
    //================================================================================================

    @NotNull
    private int minCapacity;

    @NotNull
    private int maxCapacity;

    @ManyToOne(optional = false)
    @JoinTable(name = "fk_restaurant")
    private Restaurant restaurant;


    // ~ Constructors
    //================================================================================================

    public RestaurantTable() {
        super();
    }

    public RestaurantTable(@NotNull int minCapacity, @NotNull int maxCapacity) {
        this();
        this.minCapacity = minCapacity;
        this.maxCapacity = maxCapacity;
    }

    public RestaurantTable(
            @NotNull int minCapacity,
            @NotNull int maxCapacity,
            Restaurant restaurant
    ) {
        this(minCapacity, maxCapacity);
        this.restaurant = restaurant;
        restaurant.addTable(this);
    }

    // ~ Getters & Setters
    //================================================================================================

    public int getMinCapacity() {
        return minCapacity;
    }

    public void setMinCapacity(int minCapacity) {
        this.minCapacity = minCapacity;
    }

    public int getMaxCapacity() {
        return maxCapacity;
    }

    public void setMaxCapacity(int maxCapacity) {
        this.maxCapacity = maxCapacity;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

}
