package com.abrahamandrusso.api.v4.model.resource.planning;

import javax.persistence.Entity;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import java.util.Calendar;

/**
 * @author Benhamed Sofiane on 29/04/18 21:15
 * Class Entity
 * <p>
 * Stores the data about the restaurants'
 * special days' planning
 */

@Entity
public class SpecialDay extends DefaultPlanning {

    // ~ Attributes
    //================================================================================================

    @NotNull
    @Temporal(value = TemporalType.DATE)
    private Calendar date;

    @NotNull
    private boolean workingDay;


    // ~ Constructors
    //================================================================================================

    public SpecialDay() {
        super();
    }

    public SpecialDay(
            @NotNull Calendar openingTime,
            @NotNull Calendar closingTime,
            @NotNull Calendar date,
            @NotNull boolean workingDay
    ) {
        super(openingTime, closingTime);
        this.date = date;
        this.workingDay = workingDay;
    }


    // ~ Getters & Setters
    //================================================================================================

    public Calendar getDate() {
        return date;
    }

    public void setDate(Calendar date) {
        this.date = date;
    }

    public boolean isWorkingDay() {
        return workingDay;
    }

    public void setWorkingDay(boolean workingDay) {
        this.workingDay = workingDay;
    }
}
