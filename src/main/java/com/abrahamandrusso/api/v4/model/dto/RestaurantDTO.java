package com.abrahamandrusso.api.v4.model.dto;

/**
 * @author Benahmed Sofiane on 03/04/18 17:42
 * <p>
 * Class Data Transfer
 * Used while sending available restaurants to the front end
 * </p>
 */

public class RestaurantDTO {

    // ~ Attributes
    //================================================================================================

    private Long id;

    private double latitude;

    private double longitude;

    private String address;


    // ~ Constructors
    //================================================================================================

    public RestaurantDTO() {
    }

    public RestaurantDTO(
            Long id,
            double latitude,
            double longitude,
            String address
    ) {
        this.id = id;
        this.latitude = latitude;
        this.longitude = longitude;
        this.address = address;
    }


    // ~ Getters & Setters
    //================================================================================================

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
