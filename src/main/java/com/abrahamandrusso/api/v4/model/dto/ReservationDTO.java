package com.abrahamandrusso.api.v4.model.dto;

import com.abrahamandrusso.api.v4.model.resource.meal.Meal;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.util.Calendar;
import java.util.List;

/**
 * @author Benhamed Sofiane on 03/03/18 20:59
 * <p>
 * Class Data Transfer for {@link com.abrahamandrusso.api.v4.model.resource.Reservation}
 * Used while sending reservation data from the form
 * </p>
 */

public class ReservationDTO {

    // ~ Attributes
    //================================================================================================


    @NotNull
    private Long userId;

    private Long restaurantId;

    @NotNull
    private Calendar date;

    @NotNull
    @JsonFormat(pattern = "HH:mm")
    @DateTimeFormat(pattern = "HH:mm")
    private Calendar startTime;

    @NotNull
    @JsonFormat(pattern = "HH:mm")
    @DateTimeFormat(pattern = "HH:mm")
    private Calendar endTime;

    @NotNull
    private int numberOfPlaces;

    private List<Meal> meals;


    // ~ Constructors
    //================================================================================================

    public ReservationDTO(
            @NotNull Long userId,
            @NotNull Long restaurantId,
            @NotNull Calendar date,
            @NotNull Calendar startTime,
            @NotNull Calendar endTime,
            @NotNull int numberOfPlaces
    ) {
        this.userId = userId;
        this.restaurantId = restaurantId;
        this.date = date;
        this.startTime = startTime;
        this.endTime = endTime;
        this.numberOfPlaces = numberOfPlaces;
    }


    public ReservationDTO(
            @NotNull Long userId,
            @NotNull Long restaurantId,
            @NotNull Calendar date,
            @NotNull Calendar startTime,
            @NotNull Calendar endTime,
            @NotNull int numberOfPlaces,
            List<Meal> meals
    ) {
        this.userId = userId;
        this.restaurantId = restaurantId;
        this.date = date;
        this.startTime = startTime;
        this.endTime = endTime;
        this.numberOfPlaces = numberOfPlaces;
        this.meals = meals;
    }

    public ReservationDTO() {
    }


    // ~ Getters & Setters
    //================================================================================================


    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(Long restaurantId) {
        this.restaurantId = restaurantId;
    }

    public Calendar getDate() {
        return date;
    }

    public void setDate(Calendar date) {
        this.date = date;
    }

    public Calendar getStartTime() {
        return startTime;
    }

    public void setStartTime(Calendar startTime) {
        this.startTime = startTime;
    }

    public Calendar getEndTime() {
        return endTime;
    }

    public void setEndTime(Calendar endTime) {
        this.endTime = endTime;
    }

    public int getNumberOfPlaces() {
        return numberOfPlaces;
    }

    public void setNumberOfPlaces(int numberOfPlaces) {
        this.numberOfPlaces = numberOfPlaces;
    }

    public List<Meal> getMeals() {
        return meals;
    }

    public void setMeals(List<Meal> meals) {
        this.meals = meals;
    }

}
