package com.abrahamandrusso.api.v4.model.resource.meal;

/**
 * @author Benhamed Sofiane on 29/04/18 21:15
 * Enumeration of the possible meal types
 */

public enum MealType {
    MAIN,
    DESERT,
    STARTER,
}
