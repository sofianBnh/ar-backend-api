package com.abrahamandrusso.api.v4.model.user;

import com.abrahamandrusso.api.v4.model.BaseEntity;
import com.abrahamandrusso.api.v4.model.user.role.Role;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Benhamed Sofiane on 16/03/18 20:07
 * Class Entity
 * <p>
 * Stores the data about Simple User ( Anonymous Users )
 */
@Entity
@Component
@Table(name = "Simple_Users")
public class SimpleUser extends BaseEntity {


    // ~ Attributes
    //================================================================================================

    @NotNull
    @Column(name = "name")
    private String name;

    @NotNull
    @Column(name = "email", unique = true)
    private String email;

    @NotNull
    @Column(name = "phone", unique = true)
    private String phone;

    @NotNull
    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    private List<Role> roles;


    // ~ Constructors
    //================================================================================================

    public SimpleUser() {
        super();
        this.roles = new ArrayList<>();
    }


    public SimpleUser(
            @NotNull String name,
            @NotNull String email,
            @NotNull String phone,
            @NotNull List<Role> roles
    ) {
        this();
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.roles = roles;

    }

    // ~ Getters & Setters
    //================================================================================================


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    public void addItem(Role role) {
        roles.add(role);
    }

}
