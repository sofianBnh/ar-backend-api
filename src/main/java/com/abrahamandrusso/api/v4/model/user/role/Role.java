package com.abrahamandrusso.api.v4.model.user.role;

import com.abrahamandrusso.api.v4.model.BaseEntity;
import com.abrahamandrusso.api.v4.model.user.SimpleUser;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Benhamed Sofiane on 15/03/18 18:26
 * Class Entity
 * <p>
 * Stores the data about {@link com.abrahamandrusso.api.v4.model.user.User} roles
 */

@Entity
@Table(name = "Roles")
public class Role extends BaseEntity {


    // ~ Attributes
    //================================================================================================

    @NotNull
    @Enumerated(value = EnumType.STRING)
    @Column(name = "name", unique = true)
    private Roles name;

    @ManyToMany(mappedBy = "roles")
    private List<SimpleUser> users;


    // ~ Constructors
    //================================================================================================

    public Role(Roles name) {
        this();
        this.name = name;
    }

    public Role() {
        super();
        this.users = new ArrayList<>();
    }


    // ~ Getters & Setters
    //================================================================================================


    public Roles getName() {
        return name;
    }

    public void setName(Roles name) {
        this.name = name;
    }

    public List<SimpleUser> getUsers() {
        return users;
    }

    public void setUsers(List<SimpleUser> users) {
        this.users = users;
    }

    public void addItem(SimpleUser simpleUser) {
        users.add(simpleUser);
    }
}