package com.abrahamandrusso.api.v4.model.dto;

import javax.validation.constraints.NotNull;

/**
 * @author Benhamed Sofiane on 15/03/18 19:36
 * <p>
 * Class Data Transfer
 * Used while connecting into the web
 * service (log-in)
 * </p>
 */

public class LoginDTO {

    // ~ Attributes
    //================================================================================================

    @NotNull
    private String username;

    @NotNull
    private String password;


    // ~ Constructors
    //================================================================================================


    public LoginDTO() {
    }

    public LoginDTO(
            @NotNull String username,
            @NotNull String password
    ) {
        this.username = username;
        this.password = password;
    }


    // ~ Getters & Setters
    //================================================================================================


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
