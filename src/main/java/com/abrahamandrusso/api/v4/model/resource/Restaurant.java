package com.abrahamandrusso.api.v4.model.resource;

import com.abrahamandrusso.api.v4.model.BaseEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Benhamed Sofiane on 22/02/18 21:08
 * Class Entity
 * <p>
 * Stores the data about Restaurants
 */

@Entity
@Table(name = "Restaurants")
public class Restaurant extends BaseEntity {


    // ~ Attributes
    //================================================================================================

    @NotNull
    @Column(name = "address")
    private String address;

    @NotNull
    @Column(name = "longitude")
    private double longitude;

    @NotNull
    @Column(name = "latitude")
    private double latitude;


    @OneToMany(mappedBy = "restaurant", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<RestaurantTable> tables = new HashSet<RestaurantTable>();

    // ~ Constructors
    //================================================================================================

    public Restaurant(
            String address,
            double longitude,
            double latitude
    ) {
        this();
        this.address = address;
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public Restaurant(
            @NotNull String address,
            @NotNull double longitude,
            @NotNull double latitude,
            Set<RestaurantTable> tables) {
        this(address, longitude, latitude);
        this.tables = tables;
    }

    public Restaurant() {
        super();
    }


    // ~ Getters & Setters
    //================================================================================================


    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public Set<RestaurantTable> getTables() {
        return tables;
    }

    public void setTables(Set<RestaurantTable> tables) {
        this.tables = tables;
    }

    public void addTable(RestaurantTable table) {
        this.tables.add(table);
        table.setRestaurant(this);
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @JsonIgnore
    public int getNumberOfPlaces() {
        return tables.parallelStream().mapToInt(RestaurantTable::getMaxCapacity).sum();
    }

}