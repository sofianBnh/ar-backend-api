package com.abrahamandrusso.api.v4.model.dto;

import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.util.Calendar;

/**
 * @author Benhamed Sofiane on 15/03/18 19:36
 * <p>
 * Class Data Transfer
 * Used while querying the planning
 * </p>
 */

public class DayDTO {

    // ~ Attributes
    //================================================================================================

    @NotNull
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Calendar date;


    // ~ Constructors
    //================================================================================================

    public DayDTO(@NotNull Calendar date) {
        this.date = date;
    }

    public DayDTO() {
    }


    // ~ Getters & Setters
    //================================================================================================

    public Calendar getDate() {
        return date;
    }

    public void setDate(Calendar date) {
        this.date = date;
    }
}
