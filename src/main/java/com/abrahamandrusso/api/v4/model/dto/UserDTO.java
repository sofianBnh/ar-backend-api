package com.abrahamandrusso.api.v4.model.dto;

import javax.validation.constraints.NotNull;

/**
 * @author Benhamed Sofiane on 18/03/18 20:54
 * <p>
 * Class Data Transfer for {@link com.abrahamandrusso.api.v4.model.user.User}
 * Used while creating a user
 * </p>
 */

public class UserDTO extends SimpleUserDTO {


    // ~ Attributes
    //================================================================================================

    @NotNull
    private String username;

    @NotNull
    private String password;

    // ~ Constructors
    //================================================================================================


    public UserDTO(
            @NotNull String email,
            @NotNull String name,
            @NotNull String phone,
            @NotNull String username,
            @NotNull String password
    ) {
        super(email, name, phone);
        this.username = username;
        this.password = password;
    }

    public UserDTO() {
        super();
    }


    // ~ Getters & Setters
    //================================================================================================


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
