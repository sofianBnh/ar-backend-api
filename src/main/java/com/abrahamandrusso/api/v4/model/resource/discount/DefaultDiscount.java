package com.abrahamandrusso.api.v4.model.resource.discount;


import com.abrahamandrusso.api.v4.model.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

/**
 * @author Benhamed Sofiane on 22/05/18 21:09
 * Class Entity
 * <p>
 * Stores the data about the Default
 * Discount
 */


@Entity
@Table(name = "Default_Discounts")
public class DefaultDiscount extends BaseEntity implements Reducible {


    // ~ Declarations
    //==============================================================================================

    @Max(1)
    @Min(0)
    private double ratio;

    private double reduction;


    // ~ Constructors
    //==============================================================================================

    public DefaultDiscount(double ratio, double reduction) {
        super();
        this.ratio = ratio;
        this.reduction = reduction;
    }


    public DefaultDiscount() {
        super();
        reduction = 0;
        ratio = 0;
    }


    // ~ Methods
    //==============================================================================================

    public double getRatio() {
        return ratio;
    }

    public void setRatio(double ratio) {
        this.ratio = ratio;
    }

    public double getReduction() {
        return reduction;
    }

    public void setReduction(double reduction) {
        this.reduction = reduction;
    }

    @Override
    public double calculateNewPrice(double oldPrice) {

        if (ratio != 0)
            return oldPrice - oldPrice * ratio;

        if (reduction != 0)
            return oldPrice - reduction;

        return oldPrice;
    }

    @Override
    public boolean isApplicable(double price) {

        if (ratio != 0)
            return true;

        if (reduction != 0)
            return price > reduction;

        return true;
    }
}
