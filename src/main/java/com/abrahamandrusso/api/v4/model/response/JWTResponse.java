package com.abrahamandrusso.api.v4.model.response;

/**
 * @author Benhamed Sofiane on 15/03/18 19:40
 * Class Wraper
 * <p>
 * Wrapes a JWT to send it to the front end
 */

public class JWTResponse {


    // ~ Attributes
    //================================================================================================

    private String token;

    // ~ Constructors
    //================================================================================================

    public JWTResponse() {
    }

    public JWTResponse(String token) {
        this.token = token;
    }


    // ~ Getters & Setters
    //================================================================================================

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

}
