package com.abrahamandrusso.api.v4.repository.resource.planning;

import com.abrahamandrusso.api.v4.model.resource.planning.SpecialDay;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

import java.util.Calendar;
import java.util.Optional;

/**
 * @author Benhamed Sofiane on 29/04/18 22:17
 * Interface Repository
 * <p>
 * Handles Data access for the {@link SpecialDay} entities
 */

@Repository
public interface SpecialDayRepository extends CrudRepository<SpecialDay, Long> {

    /**
     * Method that finds a Special Day Planning based on
     * its date
     *
     * @param specialDayDate day of the required planning
     * @return the Planning
     */
    @RestResource(exported = false)
    Optional<SpecialDay> findByDate(Calendar specialDayDate);

    /**
     * Method that checks if there is a Special Day Plannin
     * for a date
     *
     * @param date day of the required planning
     * @return if it exists
     */
    @RestResource(exported = false)
    boolean existsByDate(Calendar date);

}
