package com.abrahamandrusso.api.v4.repository.resource;

import com.abrahamandrusso.api.v4.model.resource.Restaurant;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Benhamed Sofiane on 26/02/18 16:47
 * Interface Repository
 * <p>
 * Handles Data access for the {@link Restaurant} entities
 */

@Repository
public interface RestaurantRepository extends CrudRepository<Restaurant, Long> {
}
