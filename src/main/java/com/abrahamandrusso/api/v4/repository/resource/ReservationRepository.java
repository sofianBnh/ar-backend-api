package com.abrahamandrusso.api.v4.repository.resource;

import com.abrahamandrusso.api.v4.model.resource.Reservation;
import com.abrahamandrusso.api.v4.model.user.SimpleUser;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

import java.util.Calendar;
import java.util.List;

/**
 * @author Benhamed Sofiane on 26/02/18 16:50
 * Interface Repository
 * <p>
 * Handles Data access for the {@link Reservation} entities
 */

@Repository
public interface ReservationRepository extends CrudRepository<Reservation, Long> {

    /**
     * Method that return a list of reservation having the same data and Restaurant id
     *
     * @param date : date of the reservation
     * @param id   : id of the restaurant
     * @return list of reservations
     */
    @RestResource(exported = false)
    List<Reservation> findByDateAndRestaurantId(Calendar date, Long id);

    /**
     * Method for finding all reservations made by a user
     *
     * @param id : id of the user
     * @return list of reservations
     */
    @RestResource(exported = false)
    List<Reservation> findByUserId(Long id);


    /**
     * Method that counts the number of confirmed reservations of a user
     * until a beginning date
     *
     * @param confirmed if the reservation is confirmed
     * @param user      the owner of the reservations
     * @param begging   beginning date of the count
     * @return the number of reservations
     */
    @RestResource(exported = false)
    int countByConfirmedAndUserAndDateGreaterThan(
            boolean confirmed, SimpleUser user, Calendar begging);

}