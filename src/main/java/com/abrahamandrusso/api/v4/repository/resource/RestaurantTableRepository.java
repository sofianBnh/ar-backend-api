package com.abrahamandrusso.api.v4.repository.resource;

import com.abrahamandrusso.api.v4.model.resource.Restaurant;
import com.abrahamandrusso.api.v4.model.resource.RestaurantTable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Benhamed Sofiane on 29/04/18 22:17
 * Interface Repository
 * <p>
 * Handles Data access for the {@link RestaurantTable} entities
 */

@Repository
public interface RestaurantTableRepository extends CrudRepository<RestaurantTable, Long> {

    /**
     * Method that finds tables that are not in a list of tables
     * but in a specific restaurant
     *
     * @param restaurant restaurant of the tables
     * @param ids        ids of the excluded tables
     * @return a list of tables in the restaurant minus the ones
     * sent through their ids
     */
    @RestResource(exported = false)
    List<RestaurantTable> findByRestaurantAndIdNotIn(Restaurant restaurant, List<Long> ids);

    /**
     * Method that finds all the tables of a restaurant
     *
     * @param restaurant the restaurant of the tables
     * @return the list of tables associated with the restaurant
     */
    @RestResource(exported = false)
    List<RestaurantTable> findByRestaurant(Restaurant restaurant);

}
