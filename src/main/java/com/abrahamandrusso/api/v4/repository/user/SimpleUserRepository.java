package com.abrahamandrusso.api.v4.repository.user;

import com.abrahamandrusso.api.v4.model.user.SimpleUser;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Benhamed Sofiane on 18/03/18 20:48
 * Interface Repository
 * <p>
 * Handles Data access for the {@link SimpleUser} entities
 */
@Repository

public interface SimpleUserRepository extends CrudRepository<SimpleUser, Long> {
    /**
     * Method that returns a {@link SimpleUser} by its email
     *
     * @param email : email of the user
     * @return the Simple User
     */
    SimpleUser findByEmail(String email);

    /**
     * Method that checks if a Simple User exists by its email
     *
     * @param email : email of the user
     * @return whether it exists or not
     */
    boolean existsByEmail(String email);
}
