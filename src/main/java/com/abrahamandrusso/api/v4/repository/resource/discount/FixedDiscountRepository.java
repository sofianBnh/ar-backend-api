package com.abrahamandrusso.api.v4.repository.resource.discount;

import com.abrahamandrusso.api.v4.model.resource.discount.FixedDiscount;
import org.springframework.stereotype.Repository;

/**
 * @author Benhamed Sofiane on 29/04/18 22:17
 * Interface Repository
 * <p>
 * Handles Data access for the {@link FixedDiscount} entities
 */

@Repository
public interface FixedDiscountRepository extends DiscountRepository<FixedDiscount> {
}
