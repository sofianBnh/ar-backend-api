package com.abrahamandrusso.api.v4.repository.resource;

import com.abrahamandrusso.api.v4.model.resource.meal.Meal;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

/**
 * @author Benhamed Sofiane on 29/04/18 22:17
 * Interface Repository
 * <p>
 * Handles Data access for the {@link Meal} entities
 */
@Repository
public interface MealRepository extends CrudRepository<Meal, Long> {

    /**
     * Method that checks if all ids sent exist in the db
     *
     * @param ids meal ids
     * @return if they are all in the
     */
    @RestResource(exported = false)
    boolean existsById(Iterable<Long> ids);

}
