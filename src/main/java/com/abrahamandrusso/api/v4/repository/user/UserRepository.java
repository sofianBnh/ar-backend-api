package com.abrahamandrusso.api.v4.repository.user;

import com.abrahamandrusso.api.v4.model.user.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


/**
 * @author Benhamed Sofiane on 26/02/18 16:40
 * Interface Repository
 * <p>
 * Handles Data access for the {@link User} entities
 */

@Repository
public interface UserRepository extends CrudRepository<User, Long> {

    /**
     * Method that returns a User by its username
     *
     * @param username username of the user
     * @return a User
     */
    User findByUsername(String username);


    /**
     * Method that checks if a User exists by its email
     *
     * @param email email of the user
     * @return whether it exists or not
     */
    boolean existsByEmail(String email);


    /**
     * Method that checks if a User by its username
     *
     * @param username username of the user
     * @return whether it exists or not
     */
    boolean existsByUsername(String username);

}
