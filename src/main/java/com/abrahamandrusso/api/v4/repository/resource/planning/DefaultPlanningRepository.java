package com.abrahamandrusso.api.v4.repository.resource.planning;

import com.abrahamandrusso.api.v4.model.resource.planning.DefaultPlanning;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Benhamed Sofiane on 29/04/18 22:17
 * Interface Repository
 * <p>
 * Handles Data access for the {@link DefaultPlanning} entities
 */

@Repository
public interface DefaultPlanningRepository extends CrudRepository<DefaultPlanning, Long> {
}
