package com.abrahamandrusso.api.v4.repository.user;

import com.abrahamandrusso.api.v4.model.user.role.Role;
import com.abrahamandrusso.api.v4.model.user.role.Roles;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Benhamed Sofiane on 26/02/18 16:48
 * Interface Repository
 * <p>
 * Handles Data access for the {@link Role} entities
 */

@Repository

public interface RoleRepository extends CrudRepository<Role, Long> {
    /**
     * Method that returns the role by its name
     * @param roleName : name of the role
     * @return : the Role entity
     */
    Role findByName(Roles roleName);
}