package com.abrahamandrusso.api.v4.repository.resource.discount;

import com.abrahamandrusso.api.v4.model.resource.discount.Discount;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.Calendar;
import java.util.List;

/**
 * @author Benhamed Sofiane on 29/04/18 22:17
 * Interface for Repositories
 * <p>
 * Maps a common behviour for Discount Repositories
 */

@NoRepositoryBean
public interface DiscountRepository<T extends Discount> extends CrudRepository<T, Long> {

    /**
     * Method that finds a discount that has a date between its
     * start and end data
     *
     * @param date the date when the discount should be available
     * @return a list of discounts compatible
     */
    @RestResource(exported = false)
    @Query("FROM #{#entityName} AS discount WHERE :date BETWEEN discount.start AND discount.end")
    List<T> findForDate(@Param("date") Calendar date);

}
