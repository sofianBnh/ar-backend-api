package com.abrahamandrusso.api.v4;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class V3ApplicationTests {

    @Test
    public void contextLoads() {
        // automatic test of context loading
    }

}
