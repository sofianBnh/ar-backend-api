package com.abrahamandrusso.api.v4.utils.time;

import com.abrahamandrusso.api.v4.utils.time.impl.TimeHandlerImpl;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * @author Benahmed Sofiane on 02/04/18 00:05
 */

public class TimeHandlerTest {

    private TimeHandlerImpl handler = new TimeHandlerImpl();

    //~
    //==============================================================================================


    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);

    //~
    //==============================================================================================


    @Test
    public void isBetweenSuccessful() {

        Calendar start = Calendar.getInstance();
        Calendar end = Calendar.getInstance();
        Calendar between = Calendar.getInstance();

        try {
            start.setTime(sdf.parse("2018-04-31 15:00:00"));
            between.setTime(sdf.parse("2018-04-31 16:00:00"));
            end.setTime(sdf.parse("2018-04-31 17:00:00"));

        } catch (ParseException e) {
            e.printStackTrace();
        }

        assertThat(handler.isBetween(between, start, end), is(true));

    }

    @Test
    public void isBetweenOnEdge() {

        Calendar start = Calendar.getInstance();
        Calendar end = Calendar.getInstance();
        Calendar between = Calendar.getInstance();

        try {
            start.setTime(sdf.parse("2018-04-31 15:00:00"));
            between.setTime(sdf.parse("2018-04-31 17:00:00"));
            end.setTime(sdf.parse("2018-04-31 17:00:00"));

        } catch (ParseException e) {
            e.printStackTrace();
        }

        assertThat(handler.isBetween(between, start, end), is(false));

    }

    @Test
    public void isBetweenNotBetween() {

        Calendar start = Calendar.getInstance();
        Calendar end = Calendar.getInstance();
        Calendar between = Calendar.getInstance();

        try {
            start.setTime(sdf.parse("2018-04-31 15:00:00"));
            between.setTime(sdf.parse("2018-04-31 18:00:00"));
            end.setTime(sdf.parse("2018-04-31 17:00:00"));

        } catch (ParseException e) {
            e.printStackTrace();
        }

        assertThat(handler.isBetween(between, start, end), is(false));

    }


    //~
    //==============================================================================================


    @Test
    public void daysBetweenNormal() {

        Calendar start = Calendar.getInstance();
        Calendar end = Calendar.getInstance();

        try {
            start.setTime(sdf.parse("2018-04-29 15:00:00"));
            end.setTime(sdf.parse("2018-04-31 15:00:00"));

        } catch (ParseException e) {
            e.printStackTrace();
        }

        assertThat(handler.daysBetween(start, end), is(2.0));

    }


    @Test
    public void daysBetweenHoursDelay() {

        Calendar start = Calendar.getInstance();
        Calendar end = Calendar.getInstance();

        try {
            start.setTime(sdf.parse("2018-04-29 17:00:00"));
            end.setTime(sdf.parse("2018-04-31 15:00:00"));

        } catch (ParseException e) {
            e.printStackTrace();
        }

        assertThat(handler.daysBetween(start, end), is(1.0));

    }


    //~
    //==============================================================================================


    @Test
    public void hoursBetweenNormal() {

        Calendar start = Calendar.getInstance();
        Calendar end = Calendar.getInstance();

        try {
            start.setTime(sdf.parse("2018-04-31 15:00:00"));
            end.setTime(sdf.parse("2018-04-31 17:00:00"));

        } catch (ParseException e) {
            e.printStackTrace();
        }

        assertThat(handler.hoursBetween(start, end), is(2.0));

    }


    @Test
    public void hoursBetweenHoursDelay() {

        Calendar start = Calendar.getInstance();
        Calendar end = Calendar.getInstance();

        try {
            start.setTime(sdf.parse("2018-04-30 15:00:00"));
            end.setTime(sdf.parse("2018-04-31 15:00:00"));

        } catch (ParseException e) {
            e.printStackTrace();
        }

        assertThat(handler.hoursBetween(start, end), is(24.0));

    }


    //~
    //==============================================================================================


    @Test
    public void crossWithSuccessful() {


        Calendar start = Calendar.getInstance();
        Calendar end = Calendar.getInstance();

        Calendar start2 = Calendar.getInstance();
        Calendar end2 = Calendar.getInstance();

        try {
            start.setTime(sdf.parse("2018-04-31 15:00:00"));
            end.setTime(sdf.parse("2018-04-31 17:00:00"));

            start2.setTime(sdf.parse("2018-04-31 15:30:00"));
            end2.setTime(sdf.parse("2018-04-31 16:00:00"));

        } catch (ParseException e) {
            e.printStackTrace();
        }

        assertThat(handler.crossWith(start, end, start2, end2), is(true));
        assertThat(handler.crossWith(start2, end2, start2, end2), is(true));
        assertThat(handler.crossWith(start2, end2, start, end), is(true));

    }


    @Test
    public void crossWithOut() {

        Calendar start = Calendar.getInstance();
        Calendar end = Calendar.getInstance();

        Calendar start2 = Calendar.getInstance();
        Calendar end2 = Calendar.getInstance();

        try {
            start.setTime(sdf.parse("2018-04-31 15:00:00"));
            end.setTime(sdf.parse("2018-04-31 16:00:00"));

            start2.setTime(sdf.parse("2018-04-31 17:30:00"));
            end2.setTime(sdf.parse("2018-04-31 18:00:00"));

        } catch (ParseException e) {
            e.printStackTrace();
        }

        assertThat(handler.crossWith(start, end, start2, end2), is(false));
        assertThat(handler.crossWith(start2, end2, start, end), is(false));

    }

    // ~
    //==============================================================================================


    @Test
    public void isBeforeIgnoreDateSuccessful() {


        Calendar end = Calendar.getInstance();
        Calendar start = Calendar.getInstance();

        try {

            start.setTime(sdf.parse("2018-04-31 15:00:00"));
            end.setTime(sdf.parse("2018-04-21 16:30:00"));

        } catch (ParseException e) {
            e.printStackTrace();
        }

        assertThat(handler.isBeforeIgnoreDate(start, end), is(true));
        assertThat(handler.isBeforeIgnoreDate(end, start), is(false));

    }

}