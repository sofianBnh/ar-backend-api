package com.abrahamandrusso.api.v4.utils.jwt;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Benahmed Sofiane on 25/03/18 19:39
 */

public class FakeJWTBuilder {

    public static String generateToken(
            String subject,
            Date createdDate,
            String secret,
            Long expiration
    ) {

        final Date expirationDate = calculateExpirationDate(createdDate, expiration);
        Map<String, Object> customClaims = new HashMap<>();

        return Jwts.builder()
                .setClaims(customClaims)
                .setSubject(subject)
                .setIssuedAt(createdDate)
                .setExpiration(expirationDate)
                .signWith(SignatureAlgorithm.HS512, secret)
                .compact();
    }

    private static Date calculateExpirationDate(Date createdDate, Long expiration) {
        return new Date(createdDate.getTime() + expiration * 1000);
    }


    public static String generateToken(String subject) {

        return generateToken(
                subject,
                Calendar.getInstance().getTime(),
                "secret",
                6000L
        );
    }

}
