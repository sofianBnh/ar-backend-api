package com.abrahamandrusso.api.v4.utils.jwt;

import com.abrahamandrusso.api.v4.model.user.User;
import com.abrahamandrusso.api.v4.model.user.role.Role;
import com.abrahamandrusso.api.v4.model.user.role.Roles;
import com.abrahamandrusso.api.v4.service.authentication.impl.user_details.UserDetailsImpl;
import com.abrahamandrusso.api.v4.utils.jwt.impl.JWTBuilderImpl;
import com.abrahamandrusso.api.v4.utils.jwt.impl.JWTHolderImpl;
import org.assertj.core.util.DateUtil;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.Calendar;
import java.util.Collections;
import java.util.Date;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * @author Benahmed Sofiane on 24/03/18 19:24
 */

public class JWTTest {


    @InjectMocks
    private IJWTHolder IJWTHolder = new JWTHolderImpl();

    @InjectMocks
    private IJWTBuilder builder = new JWTBuilderImpl();

    //~
    //==============================================================================================

    private UserDetailsImpl userDetails;

    private UserDetailsImpl userDetailsNotRegistered;

    private String testSecret = "testSecret";

    private long expiration = 6000L;

    //~
    //==============================================================================================

    @Before
    public void setUp() {

        MockitoAnnotations.initMocks(this);

        ReflectionTestUtils.setField(IJWTHolder, "expiration", expiration);
        ReflectionTestUtils.setField(IJWTHolder, "secret", testSecret);

        ReflectionTestUtils.setField(builder, "expiration", expiration);
        ReflectionTestUtils.setField(builder, "secret", testSecret);


        Role userRole = new Role(Roles.ROLE_USER);

        PasswordEncoder encoder = new BCryptPasswordEncoder();

        User user = new User(
                "John Doe",
                "jd@example.com",
                "55555555",
                "john",
                encoder.encode("password"),
                Collections.singletonList(
                        userRole
                )
        );

        Calendar earlier = DateUtil.toCalendar(new Date(System.currentTimeMillis()));
        earlier.add(Calendar.MINUTE, -1);

        user.setLastPasswordUpdate(earlier);

        User userNotRegistered = new User(

                "Jack Doe",
                "jk@example.com",
                "55585555",
                "jack",
                encoder.encode("password"),
                Collections.singletonList(
                        userRole
                )
        );

        userNotRegistered.setLastPasswordUpdate(earlier);

        userDetails = new UserDetailsImpl(user);
        userDetailsNotRegistered = new UserDetailsImpl(userNotRegistered);


        String token = builder.generateToken(userDetails.getUsername());

        IJWTHolder.setToken(token);

    }

    //~
    //==============================================================================================

    @Test
    public void isValidTokenSuccessful() {
        assertThat(IJWTHolder.isValidToken(userDetails), is(true));
    }

    @Test
    public void isValidTokenDifferentUser() {
        assertThat(IJWTHolder.isValidToken(userDetailsNotRegistered), is(false));
    }

    @Test
    public void isValidTokenPasswordChanged() {
        ((User) userDetails.getUser()).setPassword("new very secure password");

        assertThat(IJWTHolder.isValidToken(userDetails), is(false));
    }

    @Test
    public void isValidTokenExpired() {
        String token = FakeJWTBuilder.generateToken(
                userDetails.getUsername(),
                DateUtil.yesterday(),
                testSecret,
                expiration
        );

        IJWTHolder.setToken(token);
        assertThat(IJWTHolder.isValidToken(userDetails), is(false));

    }

    //~
    //==============================================================================================

    @Test
    public void isRefreshableSuccessful() {
        assertThat(IJWTHolder.isRefreshable(userDetails.getLastPasswordUpdate()), is(true));
    }

    @Test
    public void isRefreshablePasswordChanged() {
        Date tomorrow = DateUtil.tomorrow();
        assertThat(IJWTHolder.isRefreshable(tomorrow), is(false));
    }

    @Test
    public void isRefreshableExpired() {
        String token = FakeJWTBuilder.generateToken(
                userDetails.getUsername(),
                DateUtil.yesterday(),
                testSecret,
                expiration
        );

        IJWTHolder.setToken(token);
        assertThat(IJWTHolder.isRefreshable(userDetails.getLastPasswordUpdate()), is(false));

    }


}