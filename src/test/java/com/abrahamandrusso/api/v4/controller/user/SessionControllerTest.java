package com.abrahamandrusso.api.v4.controller.user;

import com.abrahamandrusso.api.v4.model.pojo.ServiceResponse;
import com.abrahamandrusso.api.v4.service.session.ISessionService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author Benahmed Sofiane on 03/04/18 14:30
 */
@RunWith(MockitoJUnitRunner.class)
public class SessionControllerTest {

    @InjectMocks
    private SessionController controller;

    @Mock
    private ISessionService service;


    private MockMvc mockMvc;


    private String tokenHeader = "Authorization";
    private String bearer = "Bearer ";


    @Before
    public void setUp() {

        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();

        ReflectionTestUtils.setField(controller, "tokenHeader", tokenHeader);

        ReflectionTestUtils.setField(controller, "bearer", bearer);
    }


    @Test
    public void refreshAndGetAuthenticationToken() throws Exception {

        when(service.refreshAndGetAuthenticationToken("john.fdfd.fdfdf"))
                .thenReturn(new ServiceResponse(true, ""));

        mockMvc.perform(
                get("/refresh")
                        .header("Authorization", "Bearer john.fdfd.fdfdf")
        )
                .andExpect(status().isOk());
    }


    @Test
    public void refreshAndGetAuthenticationTokenBadRequest() throws Exception {

        mockMvc.perform(
                get("/refresh")
                        .header("Authorization", "Not john")
        ).andExpect(status().isBadRequest());
    }

}