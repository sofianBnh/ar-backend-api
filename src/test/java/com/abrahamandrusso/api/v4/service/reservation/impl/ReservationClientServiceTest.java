package com.abrahamandrusso.api.v4.service.reservation.impl;

import com.abrahamandrusso.api.v4.model.dto.ReservationDTO;
import com.abrahamandrusso.api.v4.model.user.User;
import com.abrahamandrusso.api.v4.repository.resource.RestaurantRepository;
import com.abrahamandrusso.api.v4.repository.user.SimpleUserRepository;
import com.abrahamandrusso.api.v4.service.error.InvalidDataException;
import com.abrahamandrusso.api.v4.service.error.InvalidRequestException;
import com.abrahamandrusso.api.v4.utils.time.impl.TimeHandlerImpl;
import org.assertj.core.util.DateUtil;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.Calendar;
import java.util.Date;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Benahmed Sofiane on 30/03/18 23:02
 */

@RunWith(MockitoJUnitRunner.class)
public class ReservationClientServiceTest {


//    @InjectMocks
//    private ReservationUserServiceImpl userService;

    @InjectMocks
    private ReservationSimpleUserServiceImpl userService;

    //~
    //==============================================================================================


//    @Mock
//    private UserRepository userRepository;


    @Mock
    private SimpleUserRepository userRepository;

    @Mock
    private RestaurantRepository restaurantRepository;

    @Mock
    private ReservationDTO dto;

    @Mock
    private User user;

    @Mock
    private TimeHandlerImpl timeHandler;


    //~
    //==============================================================================================


    private int numberOfMaxHoursForReservation = 3;

    private int numberOfDaysBeforeDeadline = 1;

    private int numberOfHoursBeforeReservation = 2;

    private long testId = 1L;


    //~
    //==============================================================================================


    @Before
    public void setUp() {

        ReflectionTestUtils.setField(userService, "numberOfDaysBeforeDeadline",
                numberOfDaysBeforeDeadline);

        ReflectionTestUtils.setField(userService, "numberOfMaxHoursForReservation",
                numberOfMaxHoursForReservation);

        ReflectionTestUtils.setField(userService, "numberOfHoursBeforeReservation",
                numberOfHoursBeforeReservation);

    }


    //~
    //==============================================================================================


    @Test
    public void validateRequestSuccessful() {

        when(dto.getRestaurantId()).thenReturn(testId);

        when(dto.getUserId()).thenReturn(testId);

        when(restaurantRepository.existsById(testId)).thenReturn(true);

        when(userRepository.existsById(testId)).thenReturn(true);

        Date now = DateUtil.tomorrow();
        Calendar start = DateUtil.toCalendar(now);
        Calendar end = DateUtil.toCalendar(now);
        end.add(Calendar.HOUR_OF_DAY, numberOfMaxHoursForReservation - 1);

        when(dto.getDate()).thenReturn(DateUtil.toCalendar(now));

        when(dto.getStartTime()).thenReturn(start);

        when(dto.getEndTime()).thenReturn(end);

        when(timeHandler.hoursBetween(any(), any())).thenCallRealMethod();


        boolean thrown = false;

        try {
            userService.validateRequest(dto);
        } catch (InvalidRequestException e) {
            thrown = true;
        }

        assertThat(thrown, is(false));

        verify(restaurantRepository).existsById(testId);

        verify(userRepository).existsById(testId);

    }

    @Test
    public void validateRequestIncorrectTimes() {

        when(dto.getRestaurantId()).thenReturn(testId);

        when(dto.getUserId()).thenReturn(testId);

        when(restaurantRepository.existsById(testId)).thenReturn(true);

        when(userRepository.existsById(testId)).thenReturn(true);

        Date now = DateUtil.tomorrow();
        Calendar end = DateUtil.toCalendar(now);

        Calendar start = DateUtil.toCalendar(now);

        start.add(Calendar.HOUR_OF_DAY, numberOfMaxHoursForReservation - 1);

        when(dto.getStartTime()).thenReturn(start);

        when(dto.getEndTime()).thenReturn(end);

        boolean thrown = false;

        try {
            userService.validateRequest(dto);
        } catch (InvalidRequestException e) {
            thrown = true;
        }

        assertThat(thrown, is(true));

        verify(restaurantRepository).existsById(testId);

        verify(userRepository).existsById(testId);
    }

    @Test
    public void validateRequestStartTimeTooClose() {

        when(dto.getRestaurantId()).thenReturn(testId);

        when(dto.getUserId()).thenReturn(testId);

        when(restaurantRepository.existsById(testId)).thenReturn(true);

        when(userRepository.existsById(testId)).thenReturn(true);

        Date now = DateUtil.now();

        when(dto.getDate()).thenReturn(DateUtil.toCalendar(now));

        Calendar start = DateUtil.toCalendar(now);
        Calendar end = DateUtil.toCalendar(now);
        end.add(Calendar.HOUR_OF_DAY, numberOfMaxHoursForReservation - 1);


        when(dto.getStartTime()).thenReturn(start);

        when(dto.getEndTime()).thenReturn(end);


        boolean thrown = false;

        try {
            userService.validateRequest(dto);
        } catch (InvalidRequestException e) {
            thrown = true;
        }

        assertThat(thrown, is(true));


        verify(restaurantRepository).existsById(testId);

        verify(userRepository).existsById(testId);

    }

    @Test
    public void validateRequestReservationTooLong() {

        when(dto.getRestaurantId()).thenReturn(testId);

        when(dto.getUserId()).thenReturn(testId);

        when(restaurantRepository.existsById(testId)).thenReturn(true);

        when(userRepository.existsById(testId)).thenReturn(true);

        Date now = DateUtil.tomorrow();
        Calendar start = DateUtil.toCalendar(now);
        Calendar end = DateUtil.toCalendar(now);
        end.add(Calendar.HOUR_OF_DAY, numberOfMaxHoursForReservation);

        when(dto.getDate()).thenReturn(DateUtil.toCalendar(now));

        when(dto.getStartTime()).thenReturn(start);

        when(dto.getEndTime()).thenReturn(end);

        when(timeHandler.hoursBetween(any(), any())).thenCallRealMethod();

        boolean thrown = false;

        try {
            userService.validateRequest(dto);
        } catch (InvalidRequestException e) {
            thrown = true;
        }

        assertThat(thrown, is(true));

        verify(restaurantRepository).existsById(testId);

        verify(userRepository).existsById(testId);

    }

    @Test
    public void validateRequestRestaurantNotFound() {

        when(dto.getRestaurantId()).thenReturn(testId);

        when(restaurantRepository.existsById(testId)).thenReturn(false);

        boolean thrown = false;

        try {
            userService.validateRequest(dto);
        } catch (InvalidRequestException e) {
            thrown = true;
        }

        assertThat(thrown, is(true));

        verify(restaurantRepository).existsById(testId);

    }

    @Test
    public void validateRequestUserNotFound() {

        when(dto.getRestaurantId()).thenReturn(testId);

        when(dto.getUserId()).thenReturn(testId);

        when(restaurantRepository.existsById(testId)).thenReturn(true);

        when(userRepository.existsById(testId)).thenReturn(false);

        boolean thrown = false;

        try {
            userService.validateRequest(dto);
        } catch (InvalidRequestException e) {
            thrown = true;
        }

        assertThat(thrown, is(true));

        verify(restaurantRepository).existsById(testId);

        verify(userRepository).existsById(testId);

    }

    @Test
    public void validateRequestRequestOutOfDate() {

        when(dto.getRestaurantId()).thenReturn(testId);

        when(dto.getUserId()).thenReturn(testId);

        when(restaurantRepository.existsById(testId)).thenReturn(true);

        when(userRepository.existsById(testId)).thenReturn(true);

        Date now = DateUtil.yesterday();
        Calendar start = DateUtil.toCalendar(now);
        Calendar end = DateUtil.toCalendar(now);
        end.add(Calendar.HOUR_OF_DAY, numberOfMaxHoursForReservation);

        when(dto.getDate()).thenReturn(DateUtil.toCalendar(now));

        when(dto.getStartTime()).thenReturn(start);

        when(dto.getEndTime()).thenReturn(end);

        boolean thrown = false;

        try {
            userService.validateRequest(dto);
        } catch (InvalidRequestException e) {
            thrown = true;
        }

        assertThat(thrown, is(true));

        verify(restaurantRepository).existsById(testId);

        verify(userRepository).existsById(testId);

    }


    //~
    //==============================================================================================


    @Test
    public void getUserSuccessful() {

        when(dto.getUserId()).thenReturn(testId);
        when(userRepository.findById(testId)).thenReturn(Optional.of(user));

        boolean thrown = false;

        try {
            assertThat(userService.getUser(dto), is(user));
        } catch (InvalidDataException e) {
            thrown = true;
        }

        assertThat(thrown, is(false));

        verify(userRepository).findById(testId);
    }

    @Test
    public void getUserUserNotFound() {


        when(dto.getUserId()).thenReturn(testId);
        when(userRepository.findById(testId)).thenReturn(Optional.empty());

        boolean thrown = false;

        try {
            userService.getUser(dto);
        } catch (InvalidDataException e) {
            thrown = true;
        }

        assertThat(thrown, is(true));

        verify(userRepository).findById(testId);

    }


}