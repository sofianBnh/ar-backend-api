package com.abrahamandrusso.api.v4.service.table.impl;

import com.abrahamandrusso.api.v4.model.dto.ReservationDTO;
import com.abrahamandrusso.api.v4.model.resource.Reservation;
import com.abrahamandrusso.api.v4.model.resource.Restaurant;
import com.abrahamandrusso.api.v4.model.resource.RestaurantTable;
import com.abrahamandrusso.api.v4.repository.resource.RestaurantTableRepository;
import com.abrahamandrusso.api.v4.service.error.NotEnoughPlacesException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;

import static java.util.Arrays.asList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RestaurantTableServiceImplTest {

    @InjectMocks
    private RestaurantTableServiceImpl service;

    //==============================================================================================

    @Mock
    private RestaurantTableRepository restaurantTableRepository;

    @Mock
    private ReservationDTO dto;

    @Mock
    private Restaurant restaurant;

    @Mock
    private List<Reservation> sameTime;

    //==============================================================================================

    @Before
    public void setUp() {

        sameTime = Collections.emptyList();

        List<RestaurantTable> tables = asList(
                new RestaurantTable(2, 4),
                new RestaurantTable(1, 6),
                new RestaurantTable(1, 2),
                new RestaurantTable(4, 7),
                new RestaurantTable(1, 1)
        );

        doReturn(tables).when(restaurantTableRepository).findByRestaurant(any());
    }

    //==============================================================================================

    @Test
    public void getTablesForReservationSuccessful() throws Exception {

        for (int requested = 1; requested <= 20; requested++) {

            when(dto.getNumberOfPlaces()).thenReturn(requested);

            List<RestaurantTable> tablesForReservation = service.getTablesForReservation(dto, restaurant, sameTime);

            int max = tablesForReservation.stream().mapToInt(RestaurantTable::getMaxCapacity).sum();

            assertThat(max / requested <= 1.5, is(true));

        }
    }

    @Test(expected = NotEnoughPlacesException.class)
    public void getTablesForReservationNotEnoughPlaces() throws NotEnoughPlacesException {
        int requested = 21;

        when(dto.getNumberOfPlaces()).thenReturn(requested);

        service.getTablesForReservation(dto, restaurant, sameTime);

    }

}