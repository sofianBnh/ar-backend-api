package com.abrahamandrusso.api.v4.service.planning.impl;

import com.abrahamandrusso.api.v4.model.dto.DayDTO;
import com.abrahamandrusso.api.v4.model.dto.ReservationDTO;
import com.abrahamandrusso.api.v4.model.resource.planning.DefaultPlanning;
import com.abrahamandrusso.api.v4.model.resource.planning.SpecialDay;
import com.abrahamandrusso.api.v4.repository.resource.planning.DefaultPlanningRepository;
import com.abrahamandrusso.api.v4.repository.resource.planning.SpecialDayRepository;
import com.abrahamandrusso.api.v4.service.planning.error.PlanningException;
import com.abrahamandrusso.api.v4.utils.time.ITimeHandler;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Calendar;
import java.util.Collections;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PlanningServiceImplTest {


    @Spy
    @InjectMocks
    private PlanningServiceImpl service;

    //==============================================================================================

    @Mock
    private SpecialDayRepository specialDayRepository;

    @Mock
    private DefaultPlanningRepository defaultPlanningRepository;

    @Mock
    private ITimeHandler timeHandler;

    @Mock
    private SpecialDay specialDay;

    //==============================================================================================

    private Calendar date = Calendar.getInstance();

    private Calendar start = Calendar.getInstance();

    private Calendar end = Calendar.getInstance();

    private Calendar opening = Calendar.getInstance();

    private Calendar closing = Calendar.getInstance();

    private DefaultPlanning defaultPlanning = new DefaultPlanning(opening, closing);

    //==============================================================================================

    @Test
    public void findPlanningForDateSpecialSuccessful() throws Exception {

        when(timeHandler.getCurrentDate()).thenReturn(date);

        when(specialDayRepository.existsByDate(date)).thenReturn(true);

        when(specialDayRepository.findByDate(date)).thenReturn(Optional.of(specialDay));

        when(specialDay.isWorkingDay()).thenReturn(true);

        assertThat(service.findPlanningForDate(new DayDTO(date)), is(specialDay));

    }


    @Test(expected = PlanningException.class)
    public void findPlanningForDateSpecialDayNotWorking() throws Exception {

        when(timeHandler.getCurrentDate()).thenReturn(date);

        when(specialDayRepository.existsByDate(date)).thenReturn(true);

        when(specialDayRepository.findByDate(date)).thenReturn(Optional.of(specialDay));

        when(specialDay.isWorkingDay()).thenReturn(false);

        service.findPlanningForDate(new DayDTO(date));
    }

    @Test
    public void findPlanningForDateDefaultSuccessful() throws Exception {

        when(timeHandler.getCurrentDate()).thenReturn(date);

        when(specialDayRepository.existsByDate(date)).thenReturn(false);

        when(defaultPlanningRepository.findAll()).thenReturn(Collections.singletonList(defaultPlanning));

        assertThat(service.findPlanningForDate(new DayDTO(date)), is(defaultPlanning));

    }

    @Test(expected = PlanningException.class)
    public void findPlanningForDateDefaultNotFound() throws Exception {

        when(timeHandler.getCurrentDate()).thenReturn(date);

        when(specialDayRepository.existsByDate(date)).thenReturn(false);

        when(defaultPlanningRepository.findAll()).thenReturn(Collections.emptyList());

        service.findPlanningForDate(new DayDTO(date));

    }

    //==============================================================================================

    @Test
    public void checkCurrentDateTimesSuccessful() throws Exception {

        doReturn(defaultPlanning).when(service).findPlanningForDate(any());

        when(timeHandler.isBeforeIgnoreDate(start, opening)).thenReturn(false);

        when(timeHandler.isBeforeIgnoreDate(closing, end)).thenReturn(false);

        ReservationDTO dto = new ReservationDTO();
        dto.setDate(Calendar.getInstance());
        dto.setStartTime(start);
        dto.setEndTime(end);

        service.checkTimesWithCurrentPlanning(dto);

    }

    @Test(expected = PlanningException.class)
    public void checkCurrentDateTimesBadStart() throws Exception {

        doReturn(defaultPlanning).when(service).findPlanningForDate(any());

        when(timeHandler.isBeforeIgnoreDate(start, opening)).thenReturn(true);

        ReservationDTO dto = new ReservationDTO();
        dto.setDate(Calendar.getInstance());
        dto.setStartTime(start);
        dto.setEndTime(end);

        service.checkTimesWithCurrentPlanning(dto);

    }

    @Test(expected = PlanningException.class)
    public void checkCurrentDateTimesBadEnd() throws Exception {

        doReturn(defaultPlanning).when(service).findPlanningForDate(any());

//        when(timeHandler.isBeforeIgnoreDate(start, opening)).thenReturn(false);

        when(timeHandler.isBeforeIgnoreDate(closing, end)).thenReturn(true);

        ReservationDTO dto = new ReservationDTO();
        dto.setDate(Calendar.getInstance());
        dto.setStartTime(start);
        dto.setEndTime(end);

        service.checkTimesWithCurrentPlanning(dto);
    }

}