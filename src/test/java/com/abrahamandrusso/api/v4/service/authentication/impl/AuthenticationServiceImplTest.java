package com.abrahamandrusso.api.v4.service.authentication.impl;

import com.abrahamandrusso.api.v4.model.user.User;
import com.abrahamandrusso.api.v4.service.authentication.impl.user_details.UserDetailsImpl;
import com.abrahamandrusso.api.v4.utils.jwt.IJWTHolder;
import io.jsonwebtoken.JwtException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Benahmed Sofiane on 24/03/18 19:21
 */

@RunWith(MockitoJUnitRunner.class)
public class AuthenticationServiceImplTest {


    @InjectMocks
    private AuthenticationServiceImpl service;


    @Mock
    private UserDetailsService userDetailsService;

    @Mock
    private IJWTHolder token;

    @Mock
    private UserDetailsImpl userDetails;


    @Before
    public void setUp() {

        User user = new User(

                "John Doe",
                "jd@example.com",
                "55555555",
                "john",
                "password",
                Collections.emptyList()
        );


        userDetails = new UserDetailsImpl(user);

    }

    @Test
    public void checkIfAllowedSuccessful() {

        ((User) userDetails.getUser()).setEnabled(true);

        when(token.getSubject()).thenReturn(userDetails.getUsername());

        when(token.isValidToken(any(UserDetails.class))).thenReturn(true);

        when(userDetailsService.loadUserByUsername(userDetails.getUsername()))
                .thenReturn(userDetails);


        String fakeHeader = "token";

        UserDetailsImpl userDetailsResponse = (UserDetailsImpl) service.checkIfAllowed(fakeHeader);

        verify(token).getSubject();

        verify(userDetailsService).loadUserByUsername(anyString());

        assertEquals(userDetails, userDetailsResponse);

    }


    @Test
    public void checkIfAllowedUnknownUser() {

        when(token.getSubject()).thenReturn("will");

        String fakeHeader = "token";

        UserDetailsImpl userDetailsResponse = (UserDetailsImpl) service.checkIfAllowed(fakeHeader);

        verify(token).getSubject();

        verify(userDetailsService).loadUserByUsername(anyString());

        assertNull(userDetailsResponse);

    }

    @Test
    public void checkIfAllowedUnconfirmedUser() {

        when(token.getSubject()).thenReturn(userDetails.getUsername());

        when(token.isValidToken(any(UserDetails.class))).thenReturn(true);

        when(userDetailsService.loadUserByUsername(userDetails.getUsername()))
                .thenReturn(userDetails);


        String fakeHeader = "token";

        UserDetailsImpl userDetailsResponse = (UserDetailsImpl) service.checkIfAllowed(fakeHeader);

        verify(token).getSubject();

        verify(userDetailsService).loadUserByUsername(anyString());


        assertNull(userDetailsResponse);

    }

    @Test
    public void checkIfAllowedInvalidToken() {

        ((User) userDetails.getUser()).setEnabled(true);

        when(token.getSubject()).thenThrow(JwtException.class);

        String fakeHeader = "token";

        UserDetailsImpl userDetailsResponse = (UserDetailsImpl) service.checkIfAllowed(fakeHeader);

        assertNull(userDetailsResponse);

    }


}