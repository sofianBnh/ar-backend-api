package com.abrahamandrusso.api.v4.service.discount.impl;

import com.abrahamandrusso.api.v4.model.resource.Reservation;
import com.abrahamandrusso.api.v4.model.resource.discount.DefaultDiscount;
import com.abrahamandrusso.api.v4.model.resource.discount.FixedDiscount;
import com.abrahamandrusso.api.v4.model.resource.meal.Meal;
import com.abrahamandrusso.api.v4.model.user.SimpleUser;
import com.abrahamandrusso.api.v4.repository.resource.ReservationRepository;
import com.abrahamandrusso.api.v4.repository.resource.discount.DefaultDiscountRepository;
import com.abrahamandrusso.api.v4.repository.resource.discount.DynamicDiscountRepository;
import com.abrahamandrusso.api.v4.repository.resource.discount.FixedDiscountRepository;
import com.abrahamandrusso.api.v4.service.discount.error.DiscountException;
import com.abrahamandrusso.api.v4.utils.time.ITimeHandler;
import org.assertj.core.util.DateUtil;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.Calendar;
import java.util.Collections;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class DiscountServiceImplTest {

    private static double testMealPrice = 10d;

    private static int numberOfReservationsForDiscount = 3;

    private static double discountRatio = 0.3d;

    private static double reduction = 5d;

    //==============================================================================================

    @Spy
    @InjectMocks
    private DiscountServiceImpl service;

    //==============================================================================================

    @Mock
    private DynamicDiscountRepository dynamicDiscountRepository;

    @Mock
    private DefaultDiscountRepository defaultDiscountRepository;

    @Mock
    private FixedDiscountRepository fixedDiscountRepository;

    @Mock
    private ReservationRepository reservationRepository;

    @Mock
    private ITimeHandler timeHandler;

    @Mock
    private Reservation reservation;

    @Mock
    private SimpleUser user;

    //==============================================================================================

    private MockMeal meal = new MockMeal();

    private DefaultDiscount defaultDiscount = new DefaultDiscount();

    private MockDiscount discount = new MockDiscount();

    //==============================================================================================

    @Before
    public void setUp() {
        ReflectionTestUtils.setField(service,
                "numberOfReservationsForDiscount",
                numberOfReservationsForDiscount);

        defaultDiscount.setRatio(discountRatio);
    }

    //==============================================================================================

    @Test(expected = DiscountException.class)
    public void calculateInitialPriceInvalidReservationNotFound() throws Exception {
        service.calculateInitialPrice(null);

    }

    @Test(expected = DiscountException.class)
    public void calculateInitialPriceInvalidMealsAreNull() throws Exception {
        when(reservation.getMeals()).thenReturn(null);
        service.calculateInitialPrice(reservation);
    }

    @Test(expected = DiscountException.class)
    public void calculateInitialPriceInvalidOldReservation() throws Exception {

        when(reservation.getMeals()).thenReturn(Collections.singletonList(meal));
        Calendar date = Calendar.getInstance();
        date.setTime(DateUtil.yesterday());
        when(reservation.getDate()).thenReturn(date);

        when(timeHandler.getCurrentDate()).thenReturn(Calendar.getInstance());

        service.calculateInitialPrice(reservation);
    }

    @Test
    public void calculateInitialPriceSuccessful() throws Exception {

        when(reservation.getMeals()).thenReturn(Collections.singletonList(meal));
        Calendar date = Calendar.getInstance();
        date.setTime(DateUtil.tomorrow());
        when(reservation.getDate()).thenReturn(date);

        when(timeHandler.getCurrentDate()).thenReturn(Calendar.getInstance());

        assertThat(service.calculateInitialPrice(reservation), is(testMealPrice));
        verify(reservationRepository).save(reservation);
    }

    //==============================================================================================

    @Test(expected = DiscountException.class)
    public void getCurrentAutomaticDiscountDefaultDiscountNotFound() throws Exception {
        when(defaultDiscountRepository.findAll()).thenReturn(Collections.emptyList());
        service.getCurrentAutomaticDiscount();
    }

    @Test
    public void getCurrentAutomaticDiscountSuccessful() throws Exception {
        when(defaultDiscountRepository.findAll()).thenReturn(Collections.singletonList(defaultDiscount));
        assertThat(service.getCurrentAutomaticDiscount(), is(defaultDiscount));
    }

    //==============================================================================================

    @Test(expected = DiscountException.class)
    public void calculateWithAutomaticDiscountInitialPriceNotFound() throws Exception {
        when(reservation.getPrice()).thenReturn(null);
        service.calculateWithAutomaticDiscount(reservation);
    }

    @Test
    public void calculateWithAutomaticDiscountSuccessfulDiscounted() throws Exception {
        when(reservation.getPrice()).thenReturn(testMealPrice);
        when(reservation.getUser()).thenReturn(user);

        doReturn(defaultDiscount).when(service).getCurrentAutomaticDiscount();

        Calendar today = Calendar.getInstance();
        when(timeHandler.getCurrentDate()).thenReturn(today);

        Calendar begging = Calendar.getInstance();
        begging.setTime(today.getTime());
        begging.set(Calendar.MONTH, begging.get(Calendar.MONTH) - 1);
        when(reservationRepository
                .countByConfirmedAndUserAndDateGreaterThan(
                        true, user, begging
                )
        ).thenReturn(numberOfReservationsForDiscount);

        final double finalPrice = testMealPrice - testMealPrice * discountRatio;
        assertThat(service.calculateWithAutomaticDiscount(reservation), is(finalPrice));

        verify(reservationRepository).save(reservation);
    }

    @Test
    public void calculateWithAutomaticDiscountSuccessfulNotDiscounted() throws Exception {

        when(reservation.getPrice()).thenReturn(testMealPrice);
        when(reservation.getUser()).thenReturn(user);

        Calendar today = Calendar.getInstance();
        when(timeHandler.getCurrentDate()).thenReturn(today);

        Calendar begging = Calendar.getInstance();
        begging.setTime(today.getTime());
        begging.set(Calendar.MONTH, begging.get(Calendar.MONTH) - 1);
        when(reservationRepository
                .countByConfirmedAndUserAndDateGreaterThan(
                        true, user, begging
                )
        ).thenReturn(0);

        assertThat(service.calculateWithAutomaticDiscount(reservation), is(testMealPrice));


    }

    //==============================================================================================

    @Test(expected = DiscountException.class)
    public void calculateDiscountedPriceInitialPriceNotFound() throws Exception {
        when(reservation.getPrice()).thenReturn(null);
        service.calculateDiscountedPrice(reservation);
    }

    @Test
    public void calculateDiscountedPriceSuccessful() throws Exception {
        when(reservation.getPrice()).thenReturn(testMealPrice);

        Calendar date = Calendar.getInstance();
        when(reservation.getDate()).thenReturn(date);
        when(fixedDiscountRepository.findForDate(date)).thenReturn(Collections.singletonList(discount));
        when(dynamicDiscountRepository.findForDate(date)).thenReturn(Collections.emptyList());

        final double finalPrice = testMealPrice - reduction;

        assertThat(service.calculateDiscountedPrice(reservation), is(finalPrice));
        verify(reservationRepository).save(reservation);

    }

    //==============================================================================================

    class MockMeal extends Meal {
        @Override
        public Double getPrice() {
            return testMealPrice;
        }
    }

    class MockDiscount extends FixedDiscount {

        @Override
        public double calculateNewPrice(double oldPrice) {
            return oldPrice - reduction;
        }

        @Override
        public boolean isApplicable(double price) {
            return true;
        }
    }

}