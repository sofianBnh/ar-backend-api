package com.abrahamandrusso.api.v4.service.restaurant.impl;

import com.abrahamandrusso.api.v4.model.dto.RestaurantDTO;
import com.abrahamandrusso.api.v4.model.resource.Restaurant;
import com.abrahamandrusso.api.v4.repository.resource.RestaurantRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

/**
 * @author Benahmed Sofiane on 03/04/18 17:59
 */

@RunWith(MockitoJUnitRunner.class)
public class RestaurantDataServiceImplTest {

    @InjectMocks
    private RestaurantDataServiceImpl service;

    @Mock
    private RestaurantRepository repository;


    private Restaurant restaurant;

    private RestaurantDTO dto;


    @Before
    public void setUp() {

        restaurant = new Restaurant();

        restaurant.setAddress("some address");
        restaurant.setLatitude(1.0);
        restaurant.setLongitude(1.0);

        dto = new RestaurantDTO();

        dto.setAddress(restaurant.getAddress());
        dto.setId(null);
        dto.setLatitude(restaurant.getLatitude());
        dto.setLongitude(restaurant.getLongitude());

    }

    @Test
    public void findAll() {

        when(repository.findAll()).thenReturn(Arrays.asList(restaurant, restaurant));

        service.findAll().forEach(restaurantDTO -> {

            assertThat(restaurantDTO.getAddress(), is(dto.getAddress()));
            assertThat(restaurantDTO.getLatitude(), is(dto.getLatitude()));
            assertThat(restaurantDTO.getLongitude(), is(dto.getLongitude()));
            assertThat(restaurantDTO.getId(), is(dto.getId()));

        });

    }
}