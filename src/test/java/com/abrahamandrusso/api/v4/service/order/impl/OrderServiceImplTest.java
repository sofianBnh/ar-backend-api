package com.abrahamandrusso.api.v4.service.order.impl;

import com.abrahamandrusso.api.v4.model.dto.OrderDTO;
import com.abrahamandrusso.api.v4.model.resource.Reservation;
import com.abrahamandrusso.api.v4.model.resource.meal.Meal;
import com.abrahamandrusso.api.v4.model.resource.meal.MealType;
import com.abrahamandrusso.api.v4.model.user.User;
import com.abrahamandrusso.api.v4.repository.resource.MealRepository;
import com.abrahamandrusso.api.v4.repository.resource.ReservationRepository;
import com.abrahamandrusso.api.v4.service.order.error.OrderException;
import com.abrahamandrusso.api.v4.utils.jwt.IJWTHolder;
import com.abrahamandrusso.api.v4.utils.time.impl.TimeHandlerImpl;
import org.assertj.core.util.DateUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.validation.constraints.NotNull;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class OrderServiceImplTest {

    private static Long testId = 1L;

    private static String testToken = "";

    private static String testUsername = "username";

    //==============================================================================================

    @InjectMocks
    private OrderServiceImpl service;

    //==============================================================================================

    @Mock
    private ReservationRepository reservationRepository;

    @Mock
    private MealRepository mealRepository;

    @Mock
    private TimeHandlerImpl timeHandler;

    @Mock
    private IJWTHolder holder;


    @Mock
    private OrderDTO dto;

    @Mock
    private Reservation reservation;

    @Mock
    private User testOwner;

    //==============================================================================================

    private MockMeal meal = new MockMeal("food", 123d, "food", MealType.MAIN);

    private List<Long> mealList = Collections.singletonList(testId);

    //==============================================================================================

    @Test
    public void addOrderSuccessful() throws Exception {

        when(dto.getMeals()).thenReturn(mealList);

        when(dto.getReservationId()).thenReturn(testId);

        when(reservationRepository.existsById(testId)).thenReturn(true);

        when(reservationRepository.findById(testId)).thenReturn(Optional.of(reservation));

        when(reservation.getMeals()).thenReturn(null);

        Calendar tomorrow = Calendar.getInstance();
        tomorrow.setTime(DateUtil.tomorrow());

        when(reservation.getDate()).thenReturn(tomorrow);

        when(timeHandler.getCurrentDate()).thenReturn(Calendar.getInstance());

        when(reservation.getUser()).thenReturn(testOwner);

        when(testOwner.getUsername()).thenReturn(testUsername);

        when(holder.getSubject()).thenReturn(testUsername);

        when(mealRepository.findById(testId)).thenReturn(Optional.of(meal));

        service.addOrder(dto, testToken);

        verify(reservationRepository).save(reservation);

    }

    //==============================================================================================

    @Test(expected = OrderException.class)
    public void addOrderEmptyDTO() throws Exception {

        when(dto.getMeals()).thenReturn(null);

        service.addOrder(dto, testToken);
    }

    @Test(expected = OrderException.class)
    public void addOrderReservationNotSelected() throws Exception {

        when(dto.getMeals()).thenReturn(mealList);

        when(dto.getReservationId()).thenReturn(null);

        service.addOrder(dto, testToken);

    }

    @Test(expected = OrderException.class)
    public void addOrderReservationNotFound() throws Exception {

        when(dto.getMeals()).thenReturn(mealList);

        when(dto.getReservationId()).thenReturn(testId);

        when(reservationRepository.existsById(testId)).thenReturn(false);

        service.addOrder(dto, testToken);
    }

    //==============================================================================================

    @Test(expected = OrderException.class)
    public void addOrderReservationHasOrders() throws Exception {

        when(dto.getMeals()).thenReturn(mealList);

        when(dto.getReservationId()).thenReturn(testId);

        when(reservationRepository.existsById(testId)).thenReturn(true);

        when(reservationRepository.findById(testId)).thenReturn(Optional.of(reservation));

        when(reservation.getMeals()).thenReturn(Collections.singletonList(meal));

        service.addOrder(dto, testToken);

    }

    @Test(expected = OrderException.class)
    public void addOrderReservationBeforeToday() throws Exception {

        when(dto.getMeals()).thenReturn(mealList);

        when(dto.getReservationId()).thenReturn(testId);

        when(reservationRepository.existsById(testId)).thenReturn(true);

        when(reservationRepository.findById(testId)).thenReturn(Optional.of(reservation));

        when(reservation.getMeals()).thenReturn(null);

        Calendar yesterday = Calendar.getInstance();
        yesterday.setTime(DateUtil.yesterday());

        when(reservation.getDate()).thenReturn(yesterday);

        when(timeHandler.getCurrentDate()).thenReturn(Calendar.getInstance());
        service.addOrder(dto, testToken);

    }

    //==============================================================================================

    @Test(expected = OrderException.class)
    public void addOrderReservationWrongUser() throws Exception {

        when(dto.getMeals()).thenReturn(mealList);

        when(dto.getReservationId()).thenReturn(testId);

        when(reservationRepository.existsById(testId)).thenReturn(true);

        when(reservationRepository.findById(testId)).thenReturn(Optional.of(reservation));

        when(reservation.getMeals()).thenReturn(null);

        Calendar tomorrow = Calendar.getInstance();
        tomorrow.setTime(DateUtil.tomorrow());

        when(reservation.getDate()).thenReturn(tomorrow);
        when(timeHandler.getCurrentDate()).thenReturn(Calendar.getInstance());

        when(reservation.getUser()).thenReturn(testOwner);

        when(testOwner.getUsername()).thenReturn(testUsername);

        when(holder.getSubject()).thenReturn("not" + testUsername);

        service.addOrder(dto, testToken);
    }

    //==============================================================================================

    @Test(expected = OrderException.class)
    public void addOrderMealNotFound() throws Exception {

        when(dto.getMeals()).thenReturn(mealList);

        when(dto.getReservationId()).thenReturn(testId);

        when(reservationRepository.existsById(testId)).thenReturn(true);

        when(reservationRepository.findById(testId)).thenReturn(Optional.of(reservation));

        when(reservation.getMeals()).thenReturn(null);

        Calendar tomorrow = Calendar.getInstance();
        tomorrow.setTime(DateUtil.tomorrow());

        when(reservation.getDate()).thenReturn(tomorrow);
        when(timeHandler.getCurrentDate()).thenReturn(Calendar.getInstance());

        when(reservation.getUser()).thenReturn(testOwner);

        when(testOwner.getUsername()).thenReturn(testUsername);

        when(holder.getSubject()).thenReturn(testUsername);

        when(mealRepository.findById(testId)).thenReturn(Optional.empty());

        service.addOrder(dto, testToken);
    }

    class MockMeal extends Meal {
        MockMeal(@NotNull String name, @NotNull double price,
                 @NotNull String description, @NotNull MealType type) {
            super(name, price, description, type);
        }

        @Override
        public Long getId() {
            return testId;
        }
    }

}
