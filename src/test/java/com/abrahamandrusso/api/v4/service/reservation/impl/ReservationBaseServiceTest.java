package com.abrahamandrusso.api.v4.service.reservation.impl;

import com.abrahamandrusso.api.v4.model.dto.ReservationDTO;
import com.abrahamandrusso.api.v4.model.resource.Reservation;
import com.abrahamandrusso.api.v4.model.resource.Restaurant;
import com.abrahamandrusso.api.v4.model.user.SimpleUser;
import com.abrahamandrusso.api.v4.repository.resource.ReservationRepository;
import com.abrahamandrusso.api.v4.repository.resource.RestaurantRepository;
import com.abrahamandrusso.api.v4.service.error.NotEnoughPlacesException;
import com.abrahamandrusso.api.v4.service.planning.IPlanningService;
import com.abrahamandrusso.api.v4.service.table.IRestaurantTableService;
import com.abrahamandrusso.api.v4.utils.time.ITimeHandler;
import com.abrahamandrusso.api.v4.utils.time.impl.TimeHandlerImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

/**
 * @author Benahmed Sofiane on 30/03/18 23:00
 */

@RunWith(MockitoJUnitRunner.class)
public class ReservationBaseServiceTest {


    private static long testId = 1L;

    //==============================================================================================

    @InjectMocks
    private MockReservationBaseService service;

    //==============================================================================================

    @Mock
    private TimeHandlerImpl timeHandler;

    @Mock
    private IPlanningService planningService;

    @Mock
    private RestaurantRepository restaurantRepository;

    @Mock
    private IRestaurantTableService restaurantTableService;

    @Mock
    private ReservationRepository reservationRepository;


    //==============================================================================================

    @Mock
    private Restaurant restaurant;

    @Mock
    private ReservationDTO dto;

    @Mock
    private Reservation reservation;

    //==============================================================================================


    @Test(expected = NotEnoughPlacesException.class)
    public void makeNotEnoughPlaces() throws Exception {

        int takenPlaces = 2;
        int neededPlaces = 1;
        int availablePlaces = 2;


        when(reservation.isConfirmed()).thenReturn(true);

        when(reservation.getNumberOfPlaces()).thenReturn(takenPlaces);


        List<Reservation> reservations = Collections.singletonList(reservation);

        when(dto.getRestaurantId()).thenReturn(testId);

        when(restaurantRepository.findById(testId)).thenReturn(Optional.of(restaurant));


        when(reservationRepository.findByDateAndRestaurantId(any(), eq(testId)))
                .thenReturn(reservations);


        when(timeHandler.crossWith(any(), any(), any(), any())).thenReturn(true);


        when(dto.getNumberOfPlaces()).thenReturn(neededPlaces);


        when(restaurant.getNumberOfPlaces()).thenReturn(availablePlaces);


        service.make(dto);

    }

    @Test
    public void makeEnoughPlaces() throws Exception {

        int takenPlaces = 1;
        int neededPlaces = 1;
        int availablePlaces = 2;


        when(reservation.isConfirmed()).thenReturn(true);

        List<Reservation> reservations = Collections.singletonList(reservation);

        when(dto.getRestaurantId()).thenReturn(testId);

        when(restaurantRepository.findById(testId)).thenReturn(Optional.of(restaurant));


        when(reservationRepository.findByDateAndRestaurantId(any(), eq(testId)))
                .thenReturn(reservations);


        when(dto.getNumberOfPlaces()).thenReturn(neededPlaces);

        when(restaurant.getNumberOfPlaces()).thenReturn(availablePlaces);


        service.make(dto);

    }


    //==============================================================================================


    static class MockReservationBaseService extends ReservationBaseService {

        private SimpleUser user;

        protected MockReservationBaseService(
                IRestaurantTableService restaurantTableService,
                ReservationRepository reservationRepository,
                RestaurantRepository restaurantRepository,
                IPlanningService planningService,
                ITimeHandler timeHandler,
                SimpleUser user
        ) {
            super(
                    restaurantTableService,
                    reservationRepository,
                    restaurantRepository,
                    planningService,
                    timeHandler
            );
            this.user = user;
        }

        @Override
        void validateRequest(ReservationDTO reservation) {

        }

        @Override
        SimpleUser getUser(ReservationDTO dto) {
            return user;
        }

        @Override
        void validateTimes(ReservationDTO dto) {

        }
    }

}