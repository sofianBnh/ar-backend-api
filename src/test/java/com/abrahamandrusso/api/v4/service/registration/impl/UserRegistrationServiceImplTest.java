package com.abrahamandrusso.api.v4.service.registration.impl;

import com.abrahamandrusso.api.v4.model.dto.UserDTO;
import com.abrahamandrusso.api.v4.model.user.User;
import com.abrahamandrusso.api.v4.model.user.role.Role;
import com.abrahamandrusso.api.v4.model.user.role.Roles;
import com.abrahamandrusso.api.v4.repository.user.RoleRepository;
import com.abrahamandrusso.api.v4.repository.user.UserRepository;
import com.abrahamandrusso.api.v4.service.error.ConfirmationException;
import com.abrahamandrusso.api.v4.service.error.ServiceException;
import com.abrahamandrusso.api.v4.utils.jwt.IJWTHolder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

/**
 * @author Benahmed Sofiane on 28/03/18 00:24
 */

@RunWith(MockitoJUnitRunner.class)
public class UserRegistrationServiceImplTest {

    @InjectMocks
    private UserRegistrationServiceImpl service;

    //~
    //==============================================================================================

    @Mock
    private UserRepository userRepository;

    @Mock
    private RoleRepository roleRepository;

    @Mock
    private PasswordEncoder encoder;

    @Mock
    private IJWTHolder holder;

    @Mock
    private User user;


    //~
    //==============================================================================================

    private UserDTO dto = new UserDTO(
            "somthing@email.com",
            "user",
            "555555555",
            "user",
            "password"
    );

    //~
    //==============================================================================================


    @Before
    public void setUp() {

        ReflectionTestUtils.setField(service, "domain", "localhost");
        ReflectionTestUtils.setField(service, "confirmationPath", "path");

    }

    //~
    //==============================================================================================


    @Test
    public void signUpSuccessful() throws Exception {

        when(userRepository.existsByEmail(dto.getEmail())).thenReturn(false);
        when(userRepository.existsByUsername(dto.getUsername())).thenReturn(false);


        Roles roleUser = Roles.ROLE_USER;
        when(roleRepository.findByName(roleUser)).thenReturn(new Role(roleUser));

        doReturn(null).when(userRepository).save(any());

        when(userRepository.findByUsername(dto.getUsername())).thenReturn(user);

        when(user.getId()).thenReturn(1L);

        assertThat(service.signUp(dto).getObjectId(), is("1"));

    }

    @Test(expected = ServiceException.class)
    public void signUpEmailAlreadyUsed() throws Exception {
        when(userRepository.existsByEmail(dto.getEmail())).thenReturn(true);

        service.signUp(dto);
    }

    @Test(expected = ServiceException.class)
    public void signUpUsernameAlreadyUsed() throws Exception {

        when(userRepository.existsByEmail(dto.getEmail())).thenReturn(false);
        when(userRepository.existsByUsername(dto.getUsername())).thenReturn(true);

        service.signUp(dto);
    }

    @Test(expected = ServiceException.class)
    public void signUpBadRequest() throws Exception {

        when(userRepository.existsByEmail(dto.getEmail())).thenReturn(false);
        when(userRepository.existsByUsername(dto.getUsername())).thenReturn(false);


        Roles roleUser = Roles.ROLE_USER;
        when(roleRepository.findByName(roleUser)).thenReturn(new Role(roleUser));

        doReturn(null).when(userRepository).save(any());
        when(userRepository.findByUsername(dto.getUsername())).thenReturn(null);

        service.signUp(dto);

    }


    //~
    //==============================================================================================


    @Test
    public void confirmSuccessful() throws Exception {

        doNothing().when(holder).setToken(anyString());

        when(holder.getSubject()).thenReturn("1");

        when(userRepository.findById(1L)).thenReturn(Optional.of(user));

        doNothing().when(user).setEnabled(true);

        service.confirm(anyString());

        verify(user).setEnabled(true);

    }

    @Test(expected = ConfirmationException.class)
    public void confirmInvalidToken() throws Exception {

        doNothing().when(holder).setToken(anyString());

        when(holder.getSubject()).thenReturn(null);

        service.confirm(anyString());

    }

    @Test(expected = ConfirmationException.class)
    public void confirmUserNotFound() throws Exception {

        doNothing().when(holder).setToken(anyString());

        when(holder.getSubject()).thenReturn("1");

        when(userRepository.findById(1L)).thenReturn(Optional.empty());

        service.confirm(anyString());

    }


}