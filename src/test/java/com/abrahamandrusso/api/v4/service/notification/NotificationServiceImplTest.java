package com.abrahamandrusso.api.v4.service.notification;

import com.abrahamandrusso.api.v4.model.pojo.Notification;
import com.abrahamandrusso.api.v4.service.error.EmailNotSentException;
import com.abrahamandrusso.api.v4.service.notification.impl.NotificationServiceImpl;
import com.abrahamandrusso.api.v4.utils.email.IEmailService;
import com.abrahamandrusso.api.v4.utils.jwt.IJWTBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import static com.abrahamandrusso.api.v4.model.pojo.Notification.NotificationBuilder.aNotification;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

/**
 * @author Benahmed Sofiane on 01/04/18 20:53
 */

@RunWith(MockitoJUnitRunner.class)
public class NotificationServiceImplTest {


    @InjectMocks
    private NotificationServiceImpl service;


    //~
    //==============================================================================================

    @Mock
    private IEmailService emailService;

    @Mock
    private IJWTBuilder builder;


    //~
    //==============================================================================================

    private Notification notification;


    //~
    //==============================================================================================


    @Before
    public void setUp() {

        String confirmationPath = "confirm";
        ReflectionTestUtils.setField(service, "confirmationPath", confirmationPath);
        String domain = "/";
        ReflectionTestUtils.setField(service, "domain", domain);


        notification = aNotification()
                .withUserEmail("")
                .withOperation("")
                .withRoute("")
                .withSubject("")
                .withObjectId("")
                .withUserName("")
                .build();
    }


    //~
    //==============================================================================================


    @Test
    public void sendConfirmationEmailSuccessful() {

        when(emailService.sendEmail(anyString(), anyString(), anyString())).thenReturn(true);

        boolean thrown = false;

        try {
            service.sendConfirmationNotification(notification);
        } catch (EmailNotSentException e) {
            thrown = true;
        }

        verify(emailService).sendEmail(anyString(), anyString(), anyString());
        assertThat(thrown, is(false));

    }

    @Test
    public void sendConfirmationEmailNotSent() {

        when(emailService.sendEmail(anyString(), anyString(), anyString())).thenReturn(false);

        boolean thrown = false;

        try {
            service.sendConfirmationNotification(notification);
        } catch (EmailNotSentException e) {
            thrown = true;
        }

        verify(emailService).sendEmail(anyString(), anyString(), anyString());
        assertThat(thrown, is(true));


    }
}