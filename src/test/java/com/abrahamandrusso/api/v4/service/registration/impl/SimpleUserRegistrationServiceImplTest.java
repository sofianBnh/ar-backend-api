package com.abrahamandrusso.api.v4.service.registration.impl;

import com.abrahamandrusso.api.v4.model.dto.SimpleUserDTO;
import com.abrahamandrusso.api.v4.model.user.SimpleUser;
import com.abrahamandrusso.api.v4.model.user.role.Role;
import com.abrahamandrusso.api.v4.model.user.role.Roles;
import com.abrahamandrusso.api.v4.repository.user.RoleRepository;
import com.abrahamandrusso.api.v4.repository.user.SimpleUserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

/**
 * @author Benahmed Sofiane on 28/03/18 00:25
 */


@RunWith(MockitoJUnitRunner.class)
public class SimpleUserRegistrationServiceImplTest {


    @InjectMocks
    private SimpleUserRegistrationServiceImpl service;

    //~
    //==============================================================================================


    @Mock
    private SimpleUserRepository simpleUserRepository;

    @Mock
    private RoleRepository roleRepository;

    @Mock
    private SimpleUser simpleUser;


    //~
    //==============================================================================================


    private SimpleUserDTO dto = new SimpleUserDTO("somthing@email.com", "user", "55555555");


    //~
    //==============================================================================================


    @Test
    public void signUpSuccessful() {

        when(simpleUserRepository.existsByEmail(dto.getEmail())).thenReturn(false);

        doReturn(new Role(Roles.ROLE_USER)).when(roleRepository).findByName(any());

        when(simpleUserRepository.save(any())).thenReturn(null);

        when(simpleUserRepository.findByEmail(dto.getEmail())).thenReturn(simpleUser);

        when(simpleUser.getId()).thenReturn(1L);

        assertThat(service.create(dto).isOk(), is(true));

    }

    @Test
    public void signUpAlreadyUser() {

        when(simpleUserRepository.existsByEmail(dto.getEmail())).thenReturn(false);

        assertThat(service.create(dto).isOk(), is(false));

    }

    @Test
    public void signUpBadRequest() {

        when(simpleUserRepository.existsByEmail(dto.getEmail())).thenReturn(false);

        doReturn(new Role(Roles.ROLE_USER)).when(roleRepository).findByName(any());

        when(simpleUserRepository.save(any())).thenReturn(null);

        when(simpleUserRepository.findByEmail(dto.getEmail())).thenReturn(null);

        assertThat(service.create(dto).isOk(), is(false));

    }


}