package com.abrahamandrusso.api.v4.service.session.impl;

import com.abrahamandrusso.api.v4.model.dto.LoginDTO;
import com.abrahamandrusso.api.v4.service.authentication.error.AuthenticationException;
import com.abrahamandrusso.api.v4.service.authentication.impl.user_details.UserDetailsImpl;
import com.abrahamandrusso.api.v4.utils.jwt.IJWTHolder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetailsService;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

/**
 * @author Benahmed Sofiane on 24/03/18 19:21
 */

@RunWith(MockitoJUnitRunner.class)
public class SessionServiceImplTest {


    @InjectMocks
    private SessionServiceImpl service;


    //~
    //==============================================================================================


    @Mock
    private AuthenticationManager authenticationManager;

    @Mock
    private UserDetailsService userDetailsService;

    @Mock
    private IJWTHolder holder;

    @Mock
    private UserDetailsImpl user;


    //~
    //==============================================================================================


    private LoginDTO login = new LoginDTO("username", "password");

    private UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
            login.getUsername(),
            login.getPassword()
    );

    private String username = "user";
    private String testToken = "this is a test holder";


    //~
    //==============================================================================================


    @Test(expected = AuthenticationException.class)
    public void createAuthenticationTokenBadCredentials() {

        doThrow(BadCredentialsException.class).when(authenticationManager)
                .authenticate(new UsernamePasswordAuthenticationToken(
                        login.getUsername(),
                        login.getPassword()
                ));

        service.createAuthenticationToken(login);

    }

    @Test(expected = AuthenticationException.class)
    public void createAuthenticationTokenDisabled() {

        doThrow(DisabledException.class).when(authenticationManager)
                .authenticate(authentication);

        service.createAuthenticationToken(login);

    }


    //~
    //==============================================================================================


    @Test
    public void refreshAndGetAuthenticationTokenUserNotFound() {

        doNothing().when(holder).setToken(anyString());

        when(holder.getSubject()).thenReturn(username);
        when(userDetailsService.loadUserByUsername(username)).thenReturn(null);

        assertThat(service.refreshAndGetAuthenticationToken(testToken).isOk(), equalTo(false));

    }

    @Test
    public void refreshAndGetAuthenticationTokenUnRefreshableToken() {

        doNothing().when(holder).setToken(anyString());

        when(holder.getSubject()).thenReturn(username);

        when(userDetailsService.loadUserByUsername(username)).thenReturn(any());

        assertThat(service.refreshAndGetAuthenticationToken(testToken).isOk(), is(false));

    }

    @Test
    public void refreshAndGetAuthenticationTokenUnValidToken() {
        doNothing().when(holder).setToken(anyString());

        when(holder.getSubject()).thenReturn(null);

        assertThat(service.refreshAndGetAuthenticationToken(testToken).isOk(), is(false));

    }

    @Test
    public void getCurrentUserSuccessful() {

        doNothing().when(holder).setToken(anyString());

        when(holder.getSubject()).thenReturn(username);

        when(userDetailsService.loadUserByUsername(username)).thenReturn(user);

        when(user.getId()).thenReturn(1L);

        assertThat(service.getCurrentUserId(anyString()).isOk(), is(true));

    }

    @Test
    public void getCurrentUserUserNotFound() {

        doNothing().when(holder).setToken(anyString());

        when(holder.getSubject()).thenReturn(null);

        assertThat(service.getCurrentUserId(anyString()).isOk(), is(false));
    }
}