package com.abrahamandrusso.api.v4.service.reservation.impl;

import com.abrahamandrusso.api.v4.model.dto.ReservationDTO;
import com.abrahamandrusso.api.v4.model.resource.Reservation;
import com.abrahamandrusso.api.v4.model.user.User;
import com.abrahamandrusso.api.v4.repository.resource.ReservationRepository;
import com.abrahamandrusso.api.v4.repository.user.UserRepository;
import com.abrahamandrusso.api.v4.service.error.ConfirmationException;
import com.abrahamandrusso.api.v4.service.error.InvalidRequestException;
import com.abrahamandrusso.api.v4.service.error.ServiceException;
import com.abrahamandrusso.api.v4.service.notification.INotificationService;
import com.abrahamandrusso.api.v4.service.reservation.IReservationClientService;
import com.abrahamandrusso.api.v4.utils.jwt.IJWTHolder;
import com.abrahamandrusso.api.v4.utils.time.impl.TimeHandlerImpl;
import org.assertj.core.util.DateUtil;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.Calendar;
import java.util.Collections;
import java.util.Optional;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

/**
 * @author Benahmed Sofiane on 24/03/18 19:23
 */

@RunWith(MockitoJUnitRunner.class)
public class ReservationResourceServiceImplTest {


    private static long testId = 1L;

    private static String testUsername = "john";

    private static String testToken = "";

    //==============================================================================================

    @InjectMocks
    private ReservationResourceServiceImpl service;

    //==============================================================================================

    @Mock
    private ReservationRepository reservationRepository;

    @Mock
    private INotificationService notificationService;

    @Mock
    private UserRepository userRepository;

    @Mock
    private TimeHandlerImpl timeHandler;

    @Mock
    private IJWTHolder holder;

    //==============================================================================================

    @Mock
    private IReservationClientService clientService;

    @Mock
    private Reservation reservation;

    @Mock
    private User user;

    @Mock
    private ReservationDTO dto;

    //==============================================================================================

    @Before
    public void setUp() {

        int numberOfMaxHoursForReservation = 3;
        ReflectionTestUtils.setField(service, "numberOfMaxHoursForReservation",
                numberOfMaxHoursForReservation);

        int numberOfDaysBeforeDeadline = 1;
        ReflectionTestUtils.setField(service, "numberOfDaysBeforeDeadline",
                numberOfDaysBeforeDeadline);

    }

    //==============================================================================================

    @Test
    public void saveSuccessful() throws Exception {

        when(reservation.getId()).thenReturn(testId);

        when(clientService.make(dto)).thenReturn(reservation);

        when(reservation.getUser()).thenReturn(user);

        when(user.getName()).thenReturn("");

        when(reservationRepository.save(reservation)).thenReturn(reservation);

        assertThat(service.save(clientService, dto), is(reservation));
    }


    //==============================================================================================

    @Test
    public void updateSuccessful() throws Exception {

        when(reservation.getId()).thenReturn(testId);

        when(reservationRepository.existsById(testId)).thenReturn(true);

        Calendar cal = getCalTomorrowWithInterval(1);

        when(reservation.getDate()).thenReturn(cal);

        when(timeHandler.daysBetween(any(), any())).thenReturn(2.0);

        when(reservationRepository.save(reservation)).thenReturn(reservation);

        assertThat(service.update(reservation), is(reservation));

        verify(reservationRepository).existsById(testId);

        verify(reservationRepository).save(reservation);

    }

    @Test(expected = ServiceException.class)
    public void updateReservationNotFound() throws Exception {

        when(reservation.getId()).thenReturn(testId);

        when(reservationRepository.existsById(testId)).thenReturn(false);

        service.update(reservation);

        verify(reservationRepository).existsById(testId);

    }

    @Test(expected = ServiceException.class)
    public void updateDeadLineReached() throws Exception {


        when(reservation.getId()).thenReturn(testId);

        when(reservationRepository.existsById(testId)).thenReturn(true);

        Calendar cal = getCalTomorrowWithInterval(-1);

        when(reservation.getDate()).thenReturn(cal);

        when(timeHandler.daysBetween(any(), any())).thenReturn(0.0);

        service.update(reservation);

        verify(reservationRepository).existsById(testId);

        verify(reservationRepository, never()).save(reservation);

    }

    //==============================================================================================

    @Test
    public void deleteSuccessful() throws Exception {

        when(reservationRepository.findById(testId)).thenReturn(Optional.of(reservation));

        when(reservation.getUser()).thenReturn(user);

        when(user.getUsername()).thenReturn(testUsername);

        when(holder.getSubject()).thenReturn(testUsername);

        when(userRepository.findByUsername(testUsername)).thenReturn(user);

        when(user.getUsername()).thenReturn(testUsername);


        when(reservation.getDate()).thenReturn(getCalTomorrowWithInterval(1));

        when(timeHandler.daysBetween(any(), any())).thenReturn(2.0);

        assertThat(service.delete(testToken, testId).isOk(), is(true));

        verify(reservationRepository).findById(testId);

        verify(reservationRepository).deleteById(testId);

    }

    @Test(expected = ServiceException.class)
    public void deleteSWrongOwner() throws Exception {

        when(reservationRepository.findById(testId)).thenReturn(Optional.of(reservation));

        when(reservation.getUser()).thenReturn(user);

        when(user.getUsername()).thenReturn(testUsername);

        when(holder.getSubject()).thenReturn(testUsername);

        when(userRepository.findByUsername(testUsername)).thenReturn(user);

        when(user.getUsername()).thenReturn(testUsername, "not" + testUsername);

        service.delete(testToken, testId);

    }

    @Test(expected = ServiceException.class)
    public void deleteReservationNotFound() throws Exception {

        when(reservationRepository.findById(testId))
                .thenReturn(Optional.empty());

        service.delete(testToken, testId);

        verify(reservationRepository, never()).deleteById(testId);

    }

    @Test(expected = ServiceException.class)
    public void deleteDeadLineReached() throws Exception {

        when(reservationRepository.findById(testId))
                .thenReturn(Optional.of(reservation));

        when(reservation.getUser()).thenReturn(user);

        when(user.getUsername()).thenReturn(testUsername);

        when(holder.getSubject()).thenReturn(testUsername);

        when(userRepository.findByUsername(testUsername)).thenReturn(user);

        when(user.getUsername()).thenReturn(testUsername);


        when(reservation.getDate()).thenReturn(getCalTomorrowWithInterval(-1));

        when(timeHandler.daysBetween(any(), any())).thenReturn(0.0);

        service.delete(testToken, testId);

        verify(reservationRepository).findById(testId);

        verify(reservationRepository, never()).deleteById(testId);

    }

    //==============================================================================================

    private static Calendar extractDateFromCal(Calendar cal) {
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal;
    }

    @Test
    public void findReservationsForTokenSuccessful() throws Exception {

        when(holder.getSubject()).thenReturn(testUsername);

        when(userRepository.findByUsername(testUsername)).thenReturn(user);

        when(user.getId()).thenReturn(testId);

        when(reservationRepository.findByUserId(testId)).thenReturn(Collections.emptyList());

        assertNotNull(service.findReservationsForToken(testUsername));

        verify(userRepository).findByUsername(testUsername);

        verify(reservationRepository).findByUserId(testId);

    }

    @Test(expected = InvalidRequestException.class)
    public void findReservationsForTokenBadToken() throws Exception {

        when(holder.getSubject()).thenReturn(null);

        service.findReservationsForToken(testUsername);

        verify(userRepository).findByUsername(testUsername);

    }

    //==============================================================================================

    @Test
    public void confirmSuccessful() throws Exception {

        when(holder.getSubject()).thenReturn(String.valueOf(testId));

        when(reservationRepository.findById(testId)).thenReturn(Optional.of(reservation));

        when(reservation.getDate()).thenReturn(getCalTomorrowWithInterval(1));

        when(reservation.getUser()).thenReturn(user);

        when(user.getName()).thenReturn(testUsername);

        when(user.getEmail()).thenReturn("jd@email.com");

        service.confirm(testToken);

        verify(reservationRepository).findById(testId);

    }

    @Test(expected = ConfirmationException.class)
    public void confirmBadToken() throws Exception {

        doNothing().when(holder).setToken(testToken);

        when(holder.getSubject()).thenReturn(null);

        service.confirm(testToken);

    }

    @Test(expected = ConfirmationException.class)
    public void confirmReservationNotFound() throws Exception {

        doNothing().when(holder).setToken(testToken);

        when(holder.getSubject()).thenReturn(String.valueOf(testId));

        when(reservationRepository.findById(testId)).thenReturn(Optional.empty());

        service.confirm(testToken);

        verify(holder).setToken(testToken);

        verify(holder).getSubject();

    }

    @Test(expected = ConfirmationException.class)
    public void confirmConfirmationTimeOut() throws Exception {

        doNothing().when(holder).setToken(testToken);

        when(holder.getSubject()).thenReturn(String.valueOf(testId));

        when(reservationRepository.findById(testId)).thenReturn(Optional.of(reservation));

        Calendar cal = Calendar.getInstance();

        when(timeHandler.getCurrentDate()).thenReturn(cal);

        Calendar endcal = Calendar.getInstance();

        endcal = extractDateFromCal(endcal);

        cal.add(Calendar.MINUTE, -1);

        when(reservation.getDate()).thenReturn(endcal);

        service.confirm(testToken);

        verify(holder).setToken(testToken);

        verify(holder).getSubject();

        verify(reservationRepository).findById(testId);

    }

    //==============================================================================================

    @Test(expected = InvalidRequestException.class)
    public void findReservationsForTokenUserNotFound() throws Exception {


        when(holder.getSubject()).thenReturn(testUsername);

        when(userRepository.findByUsername(testUsername)).thenReturn(null);

        service.findReservationsForToken(testUsername);

        verify(userRepository).findByUsername(testUsername);

    }

    //==============================================================================================

    private Calendar getCalTomorrowWithInterval(int interval) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(DateUtil.tomorrow());
        cal.add(Calendar.MINUTE, interval);
        return cal;
    }


}